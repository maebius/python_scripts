import os
import sys

from utility import maebius

# store start time
start_time = maebius.utility.getCurrentTime()

# path to conversion program
fceu = "C:\\utils\\nes_conversion\\fceuigcv108x.exe "

# first argument is source directory for files
source_directory = sys.argv[1]

# second argument is target directory for files
target_directory = sys.argv[2]

# our current directory is where we change back eventually
original_directory = os.getcwd()

# do the uncompression on wanted files
#results = maebius.utility.uncompressUnsimilarFilesToDirectory(source_directory, target_directory, 0)
results = maebius.utility.uncompressAllFilesToDirectory(source_directory, target_directory, 0)

# change to source directory
os.chdir(target_directory)

# our list of entries
files_handled = []

# take list of all filenames
filenames = os.listdir(target_directory)

# loop over all entries
for name in filenames:
	
	# decide output name for file
	output = name.partition(".")
	# do the conversion
	maebius.utility.execute(fceu + "\"" + name + "\" \"" + output[0] + ".dol\"")
	# erase the source file
	maebius.utility.execute("erase \"" + name + "\"")

# change back to original directory
os.chdir(original_directory)

# preprocess the files by moving them to subdirectories
maebius.utility.moveFilesToAlphabeticalSubDirectories(target_directory)

# store stop time
stop_time = maebius.utility.getCurrentTime()

# print some statistics
print ""
print "*********************************************************"
print "*********************************************************"
print ""
print "Finished!"
print "---------"
print ""
print "Start time: " + repr(start_time.ctime())
print "Stop  time: " + repr(stop_time.ctime())
print ""
print "Processing took:", (stop_time - start_time)
print ""
print "Files went through : " + repr(results[1])
print "Files processed    : " + repr(results[0])
print ""
print "*********************************************************"
print "*********************************************************"