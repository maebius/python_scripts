import os
import sys
import datetime
import time

from utility import maebius

# store start time
start_time = maebius.utility.getCurrentTime()

# first argument is source directory for files
source_directory = sys.argv[1]

# second argument is target directory for files
target_directory = sys.argv[2]

# do the uncompression on wanted files
#results = maebius.utility.uncompressUnsimilarFilesToDirectory(source_directory, target_directory, 0)
results = maebius.utility.uncompressAllFilesToDirectory(source_directory, target_directory, 0)

# preprocess the files by moving them to subdirectories
maebius.utility.moveFilesToAlphabeticalSubDirectories(target_directory)

# store stop time
stop_time = maebius.utility.getCurrentTime()

# print some statistics
print ""
print "*********************************************************"
print "*********************************************************"
print ""
print "Finished!"
print "---------"
print ""
print "Start time: " + repr(start_time.ctime())
print "Stop  time: " + repr(stop_time.ctime())
print ""
print "Processing took:", (stop_time - start_time)
print ""
print "Files went through : " + repr(results[1])
print "Files processed    : " + repr(results[0])
print ""
print "*********************************************************"
print "*********************************************************"
