
If this directory is moved, the following file needs to be edited:

%PYTHON_INSTALL_DIR%\Lib\site-packages\paths.pth

NOTE! In order to get UTF-8 as a default encoding for python, also add / edit sitecustomize.py to:

%PYTHON_INSTALL_DIR%\Lib\site-packages

... with contents:

import sys
sys.setdefaultencoding('utf-8')
#sys.setdefaultencoding('iso-8859-1')
