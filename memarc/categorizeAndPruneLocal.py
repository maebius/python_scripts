import os
import sys

from utility import utility
from utility.fileUtility import fileUtility
from utility.categorize import categorize
from utility.log import log

from pythonutils import pathutils

###
### MAIN PROGRAM
###
### This script takes a root folder, interprets its as a media archive root, constructs categories out of it so
### that the folder structure implements a category hierarchy, and then checks if there are duplicate INSIDE this
### structure by checking the entries. If such are found, it deletes the ones we do not want.
###
### @param source
###	[required] The source directory under we operate.
### @param enableCategories 
###	[optional] If True, then we form categories and check inside those,
###	if False, then all entries in one huge category and checking everything against everything -> very slow if lots of entries.
### @param justPrint 
###	[optional] Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(sourceDirectory, enableCategories, mode, justPrint):

	# do the pruning
	log.log(LOG_NAME, 1, "Constructing categories ...")
	# here we put all our results	
	rawResults = dict()
	# do the stuff!!
	categorize.directoryCategorizerWithRoot(rawResults, "", sourceDirectory, "", enableCategories, mode)

	log.log(LOG_NAME, 1, "Pruning duplicates from categories ...")
	# here we put all our pruned results
	prunedResults = dict()
	ignoredEntries = dict()
	
	sameCheckedFields = []
	allowedDifferences = []
	
	sameCheckedFields.append("title")
	allowedDifferences.append(["-", "."])
	sameCheckedFields.append("issue")
	allowedDifferences.append([])
	sameCheckedFields.append("pub_year")
	allowedDifferences.append([])

	# just count the results first, for printing out progression
	totalCategoryCount = len(rawResults.items())
	totalItemCount = 0
	for categoryKey, categoryValue in rawResults.items():
		totalItemCount += len(categoryValue)	
	log.log(LOG_NAME, 1, "total results in \"" + sourceDirectory + "\"" +\
		": " + str(totalItemCount) + " [categories: " + str(totalCategoryCount) + "]")

	handledItemCount = 0
	progressPrintDelta = 500
	nextProgressPrintCount = progressPrintDelta

	foundDuplicatesCount = 0
	
	for category, categoryResults in rawResults.items():	
		
		prunedResults[category] = []
		uniqueEntriesInCategoryList = \
			categorize.getEntriesWithSameCategoryValues(categoryResults, sameCheckedFields, allowedDifferences)
		# go through all unique-lists
		for entryList in uniqueEntriesInCategoryList:
			# choose one from every "unique entries" list
			# NOTE that if there is only one entry in list, that will be automatically selected (as we initialize to it)
			selection = entryList[0]		
			noAddsFound = False
			for entry in entryList:
				# 1. if in field "full_path" string "noads" => we choose this (if multiple, the one with biggest size)
				if entry["filename"].find("noads") != -1:
					if noAddsFound == False or entry["size"] > selection["size"]:
						selection = entry
					noAddsFound = True			
				# 2. otherwise, take the one with biggest file size
				if noAddsFound == False:
					if entry["size"] > selection["size"]:
						selection = entry
			# append our selection to the pruned categories
			prunedResults[category].append(selection)
			# remember the ones we want to delete
			entryList.remove(selection)
			fullpath = os.path.join(selection["path"], selection["filename"])
			#print "ignored: " + fullpath
			#ignoredEntries[fullpath]  = entryList
			ignoredEntries[selection["filename"]]  = entryList
			foundDuplicatesCount += len(entryList)
			
			handledItemCount += 1			
			# do we want to print?
			if handledItemCount >= nextProgressPrintCount:			
				log.log(LOG_NAME, 1, "pruned"+\
					": " + str(handledItemCount) + " of " + str(totalItemCount))
				nextProgressPrintCount += progressPrintDelta
	
	dontPrint = []
	dontPrint.append("size")
	dontPrint.append("root_path")
	dontPrint.append("path")
	dontPrint.append("title")

	deletedCount = 0
	deletedCombinedFileSize = 0

	untouchedCount = 0
	untouchedCombinedFileSize = 0

	log.log(LOG_NAME, 1, "Deleting duplicates [" + str(foundDuplicatesCount) + "] from categories...")

	handledItemCount = 0
	progressPrintDelta = 10
	nextProgressPrintCount = progressPrintDelta
	totalItemCount = foundDuplicatesCount
	
	for key, value in prunedResults.items():
		
		log.log(LOG_NAME, 3, "------------------------------------------------------------------------------")
		log.log(LOG_NAME, 3, "CATEGORY: " + key + ", count: " + str(len(value)))
		log.log(LOG_NAME, 3, "------------------------------------------------------------------------------")

		untouchedCount += len(value)
		
		for entry in value:
			
			untouchedCombinedFileSize += entry["size"]
			ignoredKey = entry["filename"]
			
			ignoreCount = len(ignoredEntries[ignoredKey])
			
			if  ignoreCount > 0:
						
				log.log(LOG_NAME, 2, "IGNORE COUNT FOR ENTRY: " + str(ignoreCount) + "-----------------------------------------------")
				log.log(LOG_NAME, 2, "SEL: " + "[size: " + str(entry["size"] ) + "] - \"" + entry["filename"] + "\"")
				
				for entryKey, entryValue in entry.items():
					if not entryKey in dontPrint:
						log.log(LOG_NAME, 3, "\t" + entryKey + ": " + str(entryValue))
								
				for ignored in ignoredEntries[ignoredKey]:
					deletedCount += 1
					deletedCombinedFileSize += ignored["size"]

					log.log(LOG_NAME, 2, "DEL: " + "[size: " + str(ignored["size"]) + "] - \"" + ignored["filename"] + "\"")
					
					# TAA EI TOIMI OIKEIN fullpath-disc-id-muutosten takia tms.
					# -> korjaa!!!

					if not justPrint:
						os.remove(ignored["full_path"])						

					handledItemCount += 1			
					# do we want to print?
					if handledItemCount >= nextProgressPrintCount:			
						log.log(LOG_NAME, 1, "deleted"+\
							": " + str(handledItemCount) + " of " + str(totalItemCount))
						nextProgressPrintCount += progressPrintDelta
						
	deletedPreservedString = ""
	if not justPrint:
		# remove source, if empty
		deletedPreserved = fileUtility.deleteAllEmptyDirectories(sourceDirectory, False, justPrint)
		deletedPreservedString = "Directories deleted: " + str(len(deletedPreserved[0])) +\
			", preserved: " + str(len(deletedPreserved[1]))
			
	formatOptions = dict()
	formatOptions["largestonly"] = True
	formatOptions["nospace"] = False

	formattedDeletedBytes = pathutils.formatbytes(deletedCombinedFileSize, formatOptions)
	formattedUntouchedBytes = pathutils.formatbytes(untouchedCombinedFileSize, formatOptions)

	log.log(LOG_NAME, 0, "-------------------------------------------------------------------------")
	if justPrint:
		log.log(LOG_NAME, 0, "Should have (but in --justPrint, so did not):")	
	log.log(LOG_NAME, 0, "Constructed categories: " + str(len(rawResults)))
	log.log(LOG_NAME, 0, "Deleted files:\t" + str(deletedCount) + "\tcombined size:\t" + formattedDeletedBytes)
	log.log(LOG_NAME, 0, "Left files:\t" + str(untouchedCount) + "\tcombined size:\t" + formattedUntouchedBytes)
	if len(deletedPreservedString) > 0: 
		log.log(LOG_NAME, 0, deletedPreservedString)
	log.log(LOG_NAME, 0, "-------------------------------------------------------------------------")


def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "mode=", "justPrint", "enableCategories"]
	requiredOptions = [ "source", "mode"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# source directory for files
	sourceDirectory = optionsResult["source"]

	mode = optionsResult["mode"]
	
	# just print?
	justPrint = "justPrint" in optionsResult

	# enableCategories? If True, then we form categories and check inside those,
	# if False, then all entries in one huge category and checking everything against everything -> very slow if lots of entries
	enableCategories = "enableCategories" in optionsResult

	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	log.init(True, False, "")
	log.add(LOG_NAME, 3)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(fileUtility.LOG_NAME, log.LVL_ERROR)

	execute(sourceDirectory, enableCategories, mode, justPrint)

	log.finalize()

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)
	
if __name__ == '__main__':
    main()
