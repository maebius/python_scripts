
import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

def satisfiesDiscNotRight(db, tableName, entry, index):

	if not "disc_id" in index:
		return False
	
	disc = fileUtility.getDisc(entry[index["disc_id"]])
	if disc == None:
		return False
	
	fullPath = categorize.getFullPath(entry, index)
	exists = os.path.exists(fullPath)
	
	return not exists
		
def performDiscNotRight(db, tableName, entry, index, justPrint):

	fullPath = categorize.getFullPath(entry, index)
	fullPathWithoutDisc = fullPath[1 : ]

	retVal = 0
	found = False
	# check if file founds on some disk ...
	allDrives = fileUtility.getDiscInfo()
	for letter, drive in allDrives.items():
		newFullPath = letter[0] + fullPathWithoutDisc
	
		if os.path.exists(newFullPath):
			found = True
			# found from different drive! update...
			newKeysAndValues = dict()
			newKeysAndValues["disc_id"] = drive["serial"]
			whereKeysAndValues = dict()
			whereKeysAndValues["id"] = entry[index["id"]]
	
			if justPrint:
				log.log(LOG_NAME, log.LVL_PROGRESS,\
					"\"" + fullPathWithoutDisc + "\": " +\
					str(entry[index["disc_id"]]) + " -> \"" + newKeysAndValues["disc_id"] + "\"")
			else:	
				retVal = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False)
			break	
	if not Found:
		disc = fileUtility.getDisc(entry[index["disc_id"]])
		if disc != None:
			log.log(LOG_NAME, log.LVL_PROGRESS,\
				"\"" + fullPathWithoutDisc + "\": found associated disc" +\
				str(entry[index["disc_id"]]) + ", but path still NOT found! -> consider deleting from DB!")

	return retVal

def execute(tableName, justPrint):

	db = database()
	db.connect("memarc")

	resultSet = db.getAllFromTable(tableName)
	entryColumnNameToIndex = db.getColumnFields(tableName)
	
	modifiedEntries = 0
	shouldModifiedEntries = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 100 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = db.getCountFor(tableName)	
	preInfoLines = []
	preInfoLines.append("Entries in database: " + str(fullItemCount))
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	for entry in resultSet:

		if satisfiesDiscNotRight(db, tableName, entry, entryColumnNameToIndex):
			shouldModifiedEntries += 1
			modifiedEntries += performDiscNotRight(db, tableName, entry, entryColumnNameToIndex, justPrint)
			
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, log.LVL_PROGRESS, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	lines = []		
	lines.append("Modified (/should): " + str(modifiedEntries) + " (" + str(shouldModifiedEntries) + ")")
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "justPrint", "verbose="]
	requiredOptions = [ "table"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	# just print?
	justPrint = "justPrint" in optionsResult
	verbose = 0
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])

	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(table, justPrint)
	
	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()