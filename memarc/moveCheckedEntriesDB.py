
import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

def satisfiesMoveChecked(db, tableName, entry, index, notChecked):

	#print str(entry[index["title"]]) + " - " + str(entry[index["checked"]]) + " - " + str(entry[index["disc_id"]])
	if not "disc_id" in index or not entry[index["disc_id"]] == fileUtility.getCurrentDiscID():
		return False
	# it might be, that this entry still does not exist ... due to manual move etc.
	fullPath = categorize.getFullPath(entry, index)
	if not os.path.exists(fullPath):
		return False		
	if not "checked" in index:
		return False
	
	checked = entry[index["checked"]]
	if checked == None or checked == False or len(str(checked)) == 0:
		return notChecked
		
	return not notChecked
	
def performMoveChecked(db, tableName, entry, index, targetDisc, justPrint):

	log.log(LOG_NAME, log.LVL_PROGRESS, "Processing \"" + entry[index["title"]] + "\" ...")

	newDrive = fileUtility.getDiscInfo()[targetDisc]
	
	newKeysAndValues = dict()
	newKeysAndValues["disc_id"] = newDrive["serial"]
	whereKeysAndValues = dict()
	whereKeysAndValues["id"] = entry[index["id"]]

	oldFileMatch = categorize.getFullPath(entry, index)
	if stringUtilities.hasPostFix(oldFileMatch):
		oldFileMatch = stringUtilities.parseFileTypeAway(oldFileMatch) + "*.*"
	else:
		oldFileMatch += "*"
	
	retVal = 0
	movedBytes = 0
	ok, movedBytes = fileUtility.moveToOtherDisc(oldFileMatch, newDrive["letter"], justPrint)
	
	if justPrint:
		log.log(LOG_NAME, log.LVL_PROGRESS, "\tdb: \"" + str(entry[index["disc_id"]]) + "\" -> \"" + newKeysAndValues["disc_id"] + "\"")
		log.log(LOG_NAME, log.LVL_PROGRESS,  "\tfile: \"" + oldFileMatch + "\" -> \"" + newDrive["letter"] + "\"")		
	else:	
		if ok:
			retVal = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False)
	
	return retVal, movedBytes

def execute(tableName, targetDisc, notChecked, justPrint):

	discInfo = fileUtility.getDiscInfo()
	if not targetDisc in discInfo:
		log.log(LOG_NAME, log.LVL_ERROR, "NO given disc \"" + targetDisc + "\" present!")			
		return
	
	db = database()
	db.connect("memarc")

	resultSet = db.getAllFromTable(tableName)
	entryColumnNameToIndex = db.getColumnFields(tableName)
	
	modifiedEntries = 0
	shouldModifiedEntries = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 100 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = db.getCountFor(tableName)	
	preInfoLines = []
	preInfoLines.append("Entries in database: " + str(fullItemCount))
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	allMovedBytes = 0
	
	for entry in resultSet:

		if satisfiesMoveChecked(db, tableName, entry, entryColumnNameToIndex, notChecked):
			shouldModifiedEntries += 1
			modified, movedBytes = performMoveChecked(db, tableName, entry, entryColumnNameToIndex, targetDisc, justPrint)
			allMovedBytes += movedBytes
			modifiedEntries += modified
			
			if not justPrint and modified == 0:
				# propably run out of disc space, so exiting here ...
				log.log(LOG_NAME, log.LVL_PROGRESS, "exiting early due previous error ...")
				break
			
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, log.LVL_PROGRESS, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	lines = []		
	lines.append("Modified (/should): " + str(modifiedEntries) + " (" + str(shouldModifiedEntries) + ")")
	lines.append("Affected size: " + fileUtility.inGB(allMovedBytes))
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "targetDisc=", "notChecked", "justPrint", "verbose="]
	requiredOptions = [ "table", "targetDisc"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	targetDisc = optionsResult["targetDisc"]
	# just print?
	justPrint = "justPrint" in optionsResult
	verbose = 0
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])

	notChecked = "notChecked" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(fileUtility.LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(table, targetDisc, notChecked, justPrint)
	
	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    
	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
	
	raw_input("Press Enter to Exit ...")