import os
import sys

from utility import utility
from utility.fileUtility import fileUtility
from database.database import database
from utility.categorize import categorize

from utility.log import log
from pythonutils import pathutils

#from utility import *

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "target=", "justPrint"]
	requiredOptions = [ "source", "target"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	source = optionsResult["source"]
	target = optionsResult["target"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 1)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	log.add(fileUtility.LOG_NAME, log.LVL_PROGRESS)

	#fileUtility.moveAllFilesAndCollapseDirectories(source, target, justPrint)
	fileUtility.moveAllFilesAndPreserveDirectoryStructure(source, target, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
