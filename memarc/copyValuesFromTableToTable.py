import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###

LOG_NAME = "main"

def execute(source, target, justPrint):
				
	db = database()
	db.connect("memarc")

	searchMap = dict()
	searchMap["name"] = "title"
	#searchMap["title"] = "name"
	
	sourceResults = db.getAllFromTable(source)
	sourceColumnNameToIndex = db.getColumnFields(source)
	targetColumnNameToIndex = db.getColumnFields(target)
			
	handledItemCount = 0
	progressPrintDelta = 100
	nextProgressPrintCount = progressPrintDelta
	totalItemsSourceCount = db.getCountFor(source)	
	totalItemsTargetCount = db.getCountFor(target)	
	
	foundCount = 0
	foundResults = []
	notFoundResults = []
	
	updateValuesFromSourceToTarget = True
	updateColumns = dict()
	updateColumns["checked"] = "checked"
	updateColumns["rating"] = "rating"
	updateColumns["notes"] = "notes"
	updateColumns["checked_date"] = "checked_date"
	updateColumns["added"] = "added"
	
	notAcceptedValues = []
	notAcceptedValues.append("")
	notAcceptedValues.append("None")
	notAcceptedValues.append("0")
	notAcceptedValues.append("720p")
	notAcceptedValues.append("1080p")
	
	for sourceEntry in sourceResults:
	
		targetSearch = dict()
		for key, value in searchMap.items():
			sourceIndex = sourceColumnNameToIndex[key]
			targetSearch[value] = sourceEntry[sourceIndex]
			
		# try to find from target
		targetResults = db.getFromTableWhereIgnoreCase(target, targetSearch)
		if len(targetResults) > 0:
			for targetEntry in targetResults:
				foundCount += 1
				foundResults.append(targetEntry)
				
				if updateValuesFromSourceToTarget:
					newValues = dict()
					for srcCol, trgCol in updateColumns.items():
						newValue = str(sourceEntry[sourceColumnNameToIndex[srcCol]])
						doUpdate = True
						for notAccepted in notAcceptedValues:
							if newValue == notAccepted:
								doUpdate = False
								break
						if doUpdate:
							newValues[trgCol] = newValue
							
					db.updateValuesWhere(target, newValues, targetSearch, True)
				
		else:
			notFoundResults.append(sourceEntry)
		
		handledItemCount += 1
		# do we want to print?
		if handledItemCount >= nextProgressPrintCount:			
			log.log(LOG_NAME, 1, "processed against DB"+\
				": " + str(handledItemCount) + " of " + str(totalItemsSourceCount))
			nextProgressPrintCount += progressPrintDelta
	
	printFound = False
	printNotFound = False
	
	if printFound:
		log.log(LOG_NAME, 0,  "These source items FOUND also in target:")		
		for entry in foundResults:
			print entry
	if printNotFound:
		log.log(LOG_NAME, 0,  "These source items NOT found also in target:")		
		for entry in notFoundResults:
			print entry
	
	# this block of code to print all entries that are not found in target (from source), and have a valid disc-field ...
	if False:
		discIndex = sourceColumnNameToIndex["disc"]
		nameIndex = sourceColumnNameToIndex["name"]
		results = []
		for entry in notFoundResults:
			disc = entry[discIndex]
			if disc > 0:
				discString = str(disc)
				if len(discString) == 1:
					discString = "00" + discString
				elif len(discString) == 2:
					discString = "0" + discString
				results.append(discString + " - " + entry[nameIndex])
		results.sort()
		for line in results:
			print line
	
	lines = []		
	lines.append("Items in source \"" + source + "\": " + str(totalItemsSourceCount))
	lines.append("Items in target \"" + target + "\": " + str(totalItemsTargetCount))
	lines.append("Matches found: " + str(foundCount))
	limiter = utility.getMultipleOfStringOfLongest(lines, "-")
	log.log(LOG_NAME, 1,  limiter)
	for line in lines:
		log.log(LOG_NAME, 1,  line)		
	log.log(LOG_NAME, 1,  limiter)

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "sourceTable=", "targetTable=", "justPrint"]
	requiredOptions = [ "sourceTable", "targetTable"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# mode
	acceptedPostFixes = 0	
	
	# just print?
	justPrint = "justPrint" in optionsResult

	source = optionsResult["sourceTable"]
	target = optionsResult["targetTable"]	
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 2)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	execute(source, target, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
