import os
import sys

from utility import utility
from stringutilities import stringUtilities
from database import database



def printUsage():
	print "Wrong input! Usage:"
	print "python put_to_database.py isSourceDirFirstCategory disc sourceDirectory justPrint"
	print ""
		
###
### MAIN PROGRAM
###

if len(sys.argv) != 5:
	printUsage()
	exit(0)

# store start time
startTime = utility.getCurrentTime()

# disc for all entries
disc = int(sys.argv[2])

# source directory for files
sourceDirectory = sys.argv[3]

# this tells if we will get the source directory as first category
category = ""
if int(sys.argv[1]) == 1:
	category = sourceDirectory.rpartition("\\")[2].strip()
	
# just print?
justPrint = int(sys.argv[4])
	
# our current directory is where we change back eventually
originalDirectory = os.getcwd()

table = "memarc_comics"

database = database()
database.setParameters("localhost", 3306, "root", "fyget42mulle")
database.connect("kiilto")

directoryCategorizer(table, disc, category, sourceDirectory, "", justPrint)
#directoryCategorizer(disc, category, sourceDirectory, "", justPrint)
#directoryCategorizer(disc, "engineering", source_directory, "", justPrint)
#directoryCategorizer(table, disc, "comic / authors", sourceDirectory, "", justPrint)
#directoryCategorizer(table, disc, "comic / series / star wars", sourceDirectory, "", justPrint)
#authorCategorizer(table, disc, "comic / authors", sourceDirectory, "", "", justPrint)

#def authorCategorizer(disc, category, path, entry, author, justPrint):


# change back to original directory
os.chdir(originalDirectory)

# store stop time
stopTime = utility.getCurrentTime()

# print some statistics
utility.printProcessingTime(startTime, stopTime)