
import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

# ----------------------------------
def satisfiesChangeBackFilename(entry, index):
	if "filename" in index and "file_type" in index:
		filetype = entry[index["file_type"]]
		if filetype != None and len(filetype) > 0:
			if not filetype in entry[index["filename"]]:
				return True		
	return False

def performChangeBackFilename(entry, index, justPrint):	
	newKeysAndValues = dict()
	postFix = "." +  entry[index["file_type"]]
	if postFix == ".dvd":
		postFix = ""
			
	newKeysAndValues["filename"] = entry[index["filename"]] + postFix
	whereKeysAndValues = dict()
	whereKeysAndValues["id"] = entry[index["id"]]
		
	#print "\"" + str(newKeysAndValues["filename"]) + "\""
				
	if not justPrint:
		db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False)
		return 1
	return 0
# ----------------------------------
# ----------------------------------
def satisfiesAppendDisc(entry, index):
	if "disc_id" in index and "root_path" in index and (entry[index["disc_id"]] == None or len(entry[index["disc_id"]]) == 0):
		drives = fileUtility.getDiscInfo()
		root = entry[index["root_path"]]
		entryLetter = entry[index["root_path"]][0 : 2].upper()
		if entryLetter in drives:
			return True
	return False

def performAppendDisc(entry, index, justPrint):

	# take drive-info
	drives = fileUtility.getDiscInfo()

	entryLetter = entry[index["root_path"]][0 : 2].upper()
	drive = drives[entryLetter]
				
	newKeysAndValues = dict()
	newKeysAndValues["disc_id"] = drive["serial"]
	newKeysAndValues["root_path"] = entry[index["root_path"]][3 : ]
	
	whereKeysAndValues = dict()
	whereKeysAndValues["id"] = entry[index["id"]]
			
	retVal = 0
	if justPrint:
		print "\"" + newKeysAndValues["disc_id"] + "\" - \"" +  newKeysAndValues["root_path"]  + "\" - [" + str(entry[index["path"]]) + "\\" + str(entry[index["filename"]]) + "]"
	else:
		retVal = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False)
	
	return retVal
# ----------------------------------

# ----------------------------------
def satisfiesMoveChecked(db, tableName, entry, index):

	#print str(entry[index["title"]]) + " - " + str(entry[index["checked"]]) + " - " + str(entry[index["disc_id"]])
	if not "disc_id" in index or not entry[index["disc_id"]] == fileUtility.getCurrentDiscID():
		return False
	if not "checked" in index:
		return False
	
	checked = entry[index["checked"]]
	if checked == None or checked == False or len(str(checked)) == 0:
		return False
	return True
	
def performMoveChecked(db, tableName, entry, index, justPrint):

	print "***** SATISFY: " + entry[index["title"]]

	newDrive = fileUtility.getDiscInfo()["F:"]
	
	newKeysAndValues = dict()
	newKeysAndValues["disc_id"] = newDrive["serial"]
	whereKeysAndValues = dict()
	whereKeysAndValues["id"] = entry[index["id"]]

	oldFileMatch = categorize.getFullPath(entry, index)
	if stringUtilities.hasPostFix(oldFileMatch):
		oldFileMatch = stringUtilities.parseFileTypeAway(oldFileMatch) + ".*"
	
	retVal = 0
	if justPrint:
		print "db: " + str(entry[index["disc_id"]]) + " -> \"" + newKeysAndValues["disc_id"] + "\""
		print "file: \"" + oldFileMatch + "\" -> \"" + newDrive["letter"] + "\""
	else:	
		print "kuuchi"
		#fileUtility.moveToOtherDisc(oldFileMatch, newDrive["letter"])
		#retVal = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False)
	
	return retVal
# ----------------------------------

def execute(tableName, justPrint):

	db = database()
	db.connect("memarc")

	resultSet = db.getAllFromTable(tableName)
	entryColumnNameToIndex = db.getColumnFields(tableName)
	
	modifiedEntries = 0
	shouldModifiedEntries = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 100 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = db.getCountFor(tableName)	
	preInfoLines = []
	preInfoLines.append("Entries in database: " + str(fullItemCount))
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	for entry in resultSet:

		if satisfiesMoveChecked(db, tableName, entry, entryColumnNameToIndex):
			shouldModifiedEntries += 1
			modifiedEntries += performMoveChecked(db, tableName, entry, entryColumnNameToIndex, justPrint)
		#if satisfiesAppendDisc(entry, entryColumnNameToIndex):
		#	shouldModifiedEntries += 1
		#	modifiedEntries += performAppendDisc(entry, entryColumnNameToIndex, justPrint)
		#if satisfiesChangeBackFilename(entry):
		#	performChangeBackFilename(entry, justPrint)
			
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, log.LVL_PROGRESS, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	lines = []
	lines.append("Modified (/should): " + str(modifiedEntries) + " (" + str(shouldModifiedEntries) + ")")
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "justPrint", "verbose="]
	requiredOptions = [ "table"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	# just print?
	justPrint = "justPrint" in optionsResult
	verbose = 0
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])

	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(table, justPrint)
	
	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()