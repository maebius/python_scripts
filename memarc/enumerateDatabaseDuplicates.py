import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
###
### MAIN PROGRAM
###
### @param entryTable [required] the name of the entry DB table
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(entryTable, keepSmallest, justPrint):
	
	# here we put all our results
	results = dict()
	
	db = database()
	db.connect("memarc")

	allEntries = db.getAllFromTable(entryTable)
	fullItemCount = len(allEntries)
	entryColumnNameToIndex = db.getColumnFields(entryTable)
	
	# these are checked so that if the some entry in database has already identical values in these fields
	# with the one we are adding, we consired them duplicate, do some checking, and only preserve one of the candinates
	duplicateCheckColumns = list()
	duplicateCheckColumns.append("title")
	duplicateCheckColumns.append("pub_year")
	duplicateCheckColumns.append("issue")
	
	duplicateCount = 0
	shouldDeleted = 0
	deleted = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 500 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print
		
	deleteDict = dict()
	
	# go throug every result category
	for entry in allEntries:
		
		checkKeysAndValues = dict()			
		for column in duplicateCheckColumns:			
			checkKeysAndValues[column] = entry[entryColumnNameToIndex[column]]
						
		alreadyInCount = db.getCountForWhere(entryTable, checkKeysAndValues)
		
		if alreadyInCount > 1:
			duplicateCount += 1
			log.log(LOG_NAME, 0,\
				"Already " + str(alreadyInCount) + " entries in DB with criteria:")
			for checkKey, checkValue in checkKeysAndValues.items():
				log.log(LOG_NAME, 1, "\t" + str(checkKey) + ": " + str(checkValue))
		
			results = db.getFromTableWhereLike(entryTable, checkKeysAndValues)
			chosenSize = -1
			chosenFullpath = ""
			# check the entry we should keep
			for checkEntry in results:
				currentSize = checkEntry[entryColumnNameToIndex["size"]]					
				if (chosenSize == -1) or\
					(keepSmallest and currentSize < chosenSize) or\
					(not keepSmallest and currentSize > chosenSize):
					chosenSize = currentSize
					chosenFullpath = checkEntry[entryColumnNameToIndex["full_path"]]
			# if we found entry that we should keep, then delete all other
			if len(chosenFullpath) > 0 and chosenSize > 0:
				for checkEntry in results:
					deleteFullpath = checkEntry[entryColumnNameToIndex["full_path"]]															
					if not deleteFullpath == chosenFullpath:
						deleteDict[deleteFullpath] = checkEntry[entryColumnNameToIndex["id"]]
		
		handledItemCount += 1
		# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, 0, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	# do the actual deleting, if found any entries
	deleteKeyValue = dict()
	for fullPath, id in deleteDict.items():
		log.log(LOG_NAME, 1, "\tdelete: [" + str(id) + "] \"" + fullPath + "\"")			
		shouldDeleted += 1
		
		if not justPrint:
			deleteKeyValue["id"] = id
			db.deleteFromTableWhere(entryTable, deleteKeyValue)
			os.remove(fullPath)
			deleted += 1
			
	print "-------------------------------------------------------------------------"
	print "Duplicates: " + str(duplicateCount)
	print "Deleted (/should): " + str(deleted) + " (/" + str(shouldDeleted) + ")"
	print "-------------------------------------------------------------------------"

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "keepSmallest", "justPrint"]
	requiredOptions = [ "table", "keepSmallest"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	entryTable = optionsResult["table"]
	
	keepSmallest = "keepSmallest" in optionsResult
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 1)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	execute(entryTable, keepSmallest, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
