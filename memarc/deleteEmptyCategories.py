
import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

# ----------------------------------
def satisfiesDeleteEmptyCategories(db, sourceColumn, targetTable, targetColumn, entry, entryColumnNameToIndex):

	if not sourceColumn in entryColumnNameToIndex:
		return False
	
	wantedValue = entry[entryColumnNameToIndex[sourceColumn]]
	
	colsAndValues = dict()
	colsAndValues[targetColumn] = wantedValue
	number = db.getCountForWhere(targetTable, colsAndValues)
	if number > 0:
		return False
	
	return True
	
def performDeleteEmptyCategories(db, sourceTable, sourceColumn, entry, entryColumnNameToIndex, justPrint):

	wantedValue = entry[entryColumnNameToIndex[sourceColumn]]
	colsAndValues = dict()
	colsAndValues[sourceColumn] = wantedValue
	
	if justPrint:
		lines = []
		lines.append("*** delete entry from " + sourceTable + " ***")
		for c, i in entryColumnNameToIndex.items():
			lines.append(c + ": \"" + str(entry[i]) + "\"")
		log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
		return 0
	else:
		db.deleteFromTableWhere(sourceTable, colsAndValues, justPrint)
		
	return 1
# ----------------------------------

def execute(sourceTable, sourceColumn, targetTable, targetColumn, justPrint):

	db = database()
	db.connect("memarc")

	resultSet = db.getAllFromTable(sourceTable)
	entryColumnNameToIndex = db.getColumnFields(sourceTable)
	
	modifiedEntries = 0
	shouldModifiedEntries = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 100 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = db.getCountFor(sourceTable)	
	preInfoLines = []
	preInfoLines.append("Entries in database: " + str(fullItemCount))
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	for entry in resultSet:

		if satisfiesDeleteEmptyCategories(db, sourceColumn, targetTable, targetColumn, entry, entryColumnNameToIndex):
			shouldModifiedEntries += 1
			modifiedEntries += performDeleteEmptyCategories(db, sourceTable, sourceColumn, entry, entryColumnNameToIndex, justPrint)
			
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, log.LVL_PROGRESS, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	lines = []		
	lines.append("Modified (/should): " + str(modifiedEntries) + " (" + str(shouldModifiedEntries) + ")")
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "sourceTable=", "sourceColumn=", "targetTable=", "targetColumn=", "justPrint", "verbose="]
	requiredOptions = [ "sourceTable", "sourceColumn", "targetTable", "targetColumn" ]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	sourceTable = optionsResult["sourceTable"]
	targetTable = optionsResult["targetTable"]
	sourceColumn = optionsResult["sourceColumn"]
	targetColumn = optionsResult["targetColumn"]
	# just print?
	justPrint = "justPrint" in optionsResult
	verbose = 0
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])

	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(sourceTable, sourceColumn, targetTable, targetColumn, justPrint)
	
	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
	
	raw_input("Press Enter to Exit ...")
