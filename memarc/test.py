import os
import sys
import time

from utility import utility
from utility import fileUtility
from utility import stringUtilities
from utility.categorize import categorize
from utility.log import log
from database.database import database
		
###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

#def execute(paramStringOne, paramStringTwo, justPrint):
def execute(justPrint):
	
	# initialize database?
	#db = database()
	#db.setParameters("localhost", 3306, "root", "fyget42mulle")
	#db.connect("kiilto")

	#allEntries = db.getAllFromTable("table_name")
	#fullItemCount = len(allEntries)
	#entryColumnNameToIndex = db.getColumnFields("table_name")
	
	# TODO: do stuff!
	index = 0
	while index < 100:
		print "-------------------------------------------------------------------------"
		print "Some output! " +str(index)
		print "-------------------------------------------------------------------------"
		index += 1
		time.sleep(0.2)

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "requiredOptionWithValue=", "optionWithoutValue", "justPrint"]
	#requiredOptions = [ "requiredOptionWithValue"]
	requiredOptions = [ ]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	#requiredOptionWithValue = optionsResult["requiredOptionWithValue"]
	#optionWithoutValue = "optionWithoutValue" in optionsResult
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 1)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	#execute(requiredOptionWithValue, optionWithoutValue, justPrint)
	execute(justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
