
import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

def execute(tableName, justPrint):

	db = database()
	db.connect("memarc")
	
	categoryTable = "comics_series"
	
	resultSet = db.getAllFromTable(tableName)
	entryColumnNameToIndex = db.getColumnFields(tableName)
	catColumnNameToIndex = db.getColumnFields(categoryTable)
	
	titleIndex = entryColumnNameToIndex["title"]
	idIndex = entryColumnNameToIndex["id"]
	seriesIndex = entryColumnNameToIndex["series_id"]
	catNameIndex = catColumnNameToIndex["name"]
	catArticleIndex = catColumnNameToIndex["article"]
	
	shouldModifiedEntries = 0
	modifiedEntries = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 1000 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = len(resultSet)	
	log.log(LOG_NAME, 1, "Entries in database with criteria: " + str(fullItemCount))
	
	for entry in resultSet:
		title = entry[titleIndex]
		seriesId = entry[seriesIndex]
		seriesResultSet = db.getFromTableWhere(categoryTable, { "id" : seriesId } )
		
		seriesTitle = seriesResultSet[0][catNameIndex]
		#if seriesTitle != "Hellblazer":
			#continue
		
		if seriesResultSet[0][catArticleIndex] and len(seriesResultSet[0][catArticleIndex]) > 0:
			seriesTitle += ", " + seriesResultSet[0][catArticleIndex]
		category = seriesTitle.split(" | ")
		
		cleaned = categorize.stripCategoryOutOfTitle(title, category, "comic")
		if cleaned[0] > 0:		
			newTitle = cleaned[1]
			newArticle = cleaned[2]
			if newArticle != None:
				newTitle += ", " + newArticle
			
			log.log(LOG_NAME, 2, "found: \"" + title + "\" -> \"" + newTitle +"\"")
			shouldModifiedEntries += 1
			
			if not justPrint:
				newKeysAndValues = dict()
				newKeysAndValues["title"] = cleaned[1]
				if cleaned[2] != None:
					newKeysAndValues["article"] = cleaned[2]					
				whereKeysAndValues = dict()
				whereKeysAndValues["id"] = entry[idIndex]
				modifiedEntries += db.updateValues(tableName, newKeysAndValues, whereKeysAndValues)
		
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, 1, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	log.log(LOG_NAME, 1, "-------------------------------------------------------------------------")
	log.log(LOG_NAME, 1, "Handled: " + str(handledItemCount))
	log.log(LOG_NAME, 1, "Modified (/should): " + str(modifiedEntries) + " (/" + str(shouldModifiedEntries) + ")")
	log.log(LOG_NAME, 1, "-------------------------------------------------------------------------")
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "justPrint"]
	requiredOptions = [ "table"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 1)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	execute(table, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()