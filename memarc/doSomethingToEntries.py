import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###

LOG_NAME = "main"

def execute(sourceDirectory,\
					categoryTable, entryTable, enableCategories,\
					mode,\
					justPrint):
		
	# one entry (that is added to DB), consist of the following fields
	# (if some field is not generated/found, we try to add with empty string)
	# - additionally added:
	# -- "series_id" 
	# -- "id" (auto-increment style in DB)
	columns = list()
	# these should be always something else than empty ...
	columns.append("root_path")
	columns.append("path")	
	columns.append("filename")
	columns.append("full_path")
	columns.append("size")
	columns.append("file_type")
	columns.append("title")
	# these are optional (might not exist at all)
	columns.append("pub_year")
	columns.append("issue")
	columns.append("article")

	# these are checked so that if the some entry in database has already identical values in these fields
	# with the one we are adding, we consired them duplicate, do some checking, and only preserve one of the candinates
	duplicateCheckColumns = list()
	duplicateCheckColumns.append("series_id")
	duplicateCheckColumns.append("title")
	duplicateCheckColumns.append("path")
	duplicateCheckColumns.append("pub_year")
	duplicateCheckColumns.append("issue")

	# if we found duplicate (so that there is already in DB entry with the values of "duplicateCheckColumns",
	# and if we select new entry over the previous DB, then we use the old entrys other values but these,
	# as they may contain some already modified stuff that we want to keep (for example, rating and notes)
	modifyColumns = list()
	modifyColumns.append("filename")
	modifyColumns.append("full_path")
	modifyColumns.append("path")
	modifyColumns.append("size")
	modifyColumns.append("file_type")
	
	# *******************************************
	
	# here we put all our results
	results = dict()
	# do the stuff!!
	rootCategory = []
	#rootCategory.append("When We Left Earth (2008)")
	categorize.directoryCategorizerWithRoot(results, rootCategory, sourceDirectory, "", enableCategories, mode)

	db = database()
	db.connect("memarc")

	categoryColumnNameToIndex = db.getColumnFields(categoryTable)
	entryColumnNameToIndex = db.getColumnFields(entryTable)

	# just count the results first, for printing out progression
	totalCategoryCount = len(results.items())
	totalItemCount = 0
	for categoryKey, categoryValue in results.items():
		totalItemCount += len(categoryValue)	
	log.log(LOG_NAME, 1, "total results in \"" + sourceDirectory + "\"" +\
		": " + str(totalItemCount) + " [categories: " + str(totalCategoryCount) + "]")

	handledItemCount = 0
	progressPrintDelta = 100
	nextProgressPrintCount = progressPrintDelta
		
	# go throug every result category
	for categoryKey, categoryValue in results.items():		
		# now, go through all entries in category
		for entry in categoryValue:	
			if doesEntrySatisfyConditions(categoryKey, entry):
				doSomething(entry, justPrint)
			
			handledItemCount += 1
			# do we want to print?
			if handledItemCount >= nextProgressPrintCount:			
				log.log(LOG_NAME, 1, "processed against DB"+\
					": " + str(handledItemCount) + " of " + str(totalItemCount))
				nextProgressPrintCount += progressPrintDelta
			
def doesEntrySatisfyConditions(category, entry):
	if "issue" in entry and entry["issue"] > 99:
		return True
	return False

def doSomething(entry, justPrint):

	if justPrint:
		print "do stuff"
	else:
		issue = str(entry["issue"])
		newIssue = "S0" + issue[0 : 1] + "E" + issue[1 :]
		newFilename = entry["filename"].replace(issue, newIssue)
		newFullPath = os.path.join(entry["root_path"], os.path.join(entry["path"], newFilename))
		log.log(LOG_NAME, 1, "old: " + entry["full_path"])
		log.log(LOG_NAME, 1, "new: " + newFullPath)
		#shutil.move(entry["full_path"], newFullPath)	

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "mode=", "source=", "categoryTable=", "entryTable=", "justPrint"]
	requiredOptions = [ "mode", "source", "categoryTable", "entryTable"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# mode
	acceptedPostFixes = 0	
	mode = optionsResult["mode"]
	if not mode == "comic" and not mode == "tv":
		print "Error! Mode \"" + mode + "\" not supported!"
		sys.exit(2)
	
	# source directory for files
	sourceDirectory = os.path.normpath(optionsResult["source"])
	# just print?
	justPrint = "justPrint" in optionsResult

	categoryTable = optionsResult["categoryTable"]
	entryTable = optionsResult["entryTable"]	
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 2)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	if not os.path.exists(sourceDirectory):
		log.log(LOG_NAME, 0, "Source path \"" + sourceDirectory + "\" does not exist!")
	else:
		execute(sourceDirectory, categoryTable, entryTable, True, mode, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
