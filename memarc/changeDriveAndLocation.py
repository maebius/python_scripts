import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
LOG_NAME = "main"

def execute(tableName, sourceDisc, targetDisc, sourceDir, targetDir, justPrint):
		
	db = database()
	db.connect("memarc")
	
	whereKeysAndValues = dict()
	newKeysAndValues = dict()
	
	whereKeysAndValues["disc_id"] = sourceDisc
	if len(sourceDir) > 0:
		whereKeysAndValues["root_path"] = sourceDir
	
	if len(targetDisc) > 0:
		newKeysAndValues["disc_id"] = targetDisc
	if len(targetDir) > 0:
		newKeysAndValues["root_path"] = targetDir
	
	modifiedEntries = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, True, justPrint)

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "sourceDisc=", "targetDisc=", "sourceDir=", "targetDir=", "verbose=", "justPrint"]
	requiredOptions = [ "table", "sourceDisc", "targetDisc"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	sourceDisc = optionsResult["sourceDisc"]
	targetDisc = ""
	sourceDir = ""
	targetDir = ""
	
	if "targetDisc" in optionsResult:
		targetDisc = optionsResult["targetDisc"]
	if "sourceDir" in optionsResult:
		sourceDir = optionsResult["sourceDir"]
	if "targetDir" in optionsResult:
		targetDir = optionsResult["targetDir"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	# just print to file?
	printToFile = "printToFile" in optionsResult

	verbose = 2
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])
		
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	if printToFile:
		filenameToPrint = "file_list.txt"
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	categorize.afterHandledDelta = 10 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print
	log.add(categorize.LOG_NAME, 2)
	log.add(fileUtility.LOG_NAME, 0)
	log.add(database.LOG_NAME, 0)
	if justPrint:
		log.add(database.LOG_NAME, 3)
	
	execute(table, sourceDisc, targetDisc, sourceDir, targetDir, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
