
import os
import sys
import platform

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

def execute(tableName, justPrint):

	db = database()
	db.connect("memarc")

	resultSet = db.getAllFromTable(tableName)
	entryColumnNameToIndex = db.getColumnFields(tableName)
	
	filenameIndex = entryColumnNameToIndex["filename"]
	rootPathIndex = entryColumnNameToIndex["root_path"]
	pathIndex = entryColumnNameToIndex["path"]
	discIdIndex = entryColumnNameToIndex["disc_id"]
	idIndex = entryColumnNameToIndex["id"]
	
	foundMissing = 0
	deletedEntries = 0
	modifiedEntries = 0
	
	modifyColumns = list()
	modifyColumns.append("path")
	modifyColumns.append("root_path")
	modifyColumns.append("filename")
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 500 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = db.getCountFor(tableName)	
	preInfoLines = []
	preInfoLines.append("Entries in database: " + str(fullItemCount))
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	for entry in resultSet:
		# skip if the disc is not present, as we should not delete those ...
		disc = fileUtility.getDisc(entry[discIdIndex])
		if disc == None:
			continue

		databaseEntryPath = categorize.getFullPath(entry, entryColumnNameToIndex)
		id = entry[idIndex]
		
		# as a fail-safe, we check here that we still have the file that the DB says it has, and exit if not
		if not os.path.exists(databaseEntryPath):
			foundMissing += 1
			log.log(LOG_NAME, 2, "In DB, not in filesystem [" + str(id) + "]: \"" + str(databaseEntryPath) + "\"")
			
			keysAndValues = dict()
			keysAndValues["id"] = id
			
			db.deleteFromTableWhere(tableName, keysAndValues, justPrint)
			if not justPrint:
				deletedEntries += 1
		
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, log.LVL_PROGRESS, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	lines = []		
	lines.append("Found missing: " + str(foundMissing))
	lines.append("Deleted: " + str(deletedEntries))
	lines.append("Modified: " + str(modifiedEntries))
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "justPrint", "verbose="]
	requiredOptions = [ "table"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	# just print?
	justPrint = "justPrint" in optionsResult
	verbose = 0
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])

	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(table, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
	
	raw_input("Press Enter to Exit ...")
