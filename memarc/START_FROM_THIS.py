import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(paramStringOne, paramStringTwo, justPrint):
	
	# initialize database?
	#db = database()
	#db.setParameters("localhost", 3306, "root", "fyget42mulle")
	#db.connect("kiilto")

	#allEntries = db.getAllFromTable("table_name")
	#fullItemCount = len(allEntries)
	#entryColumnNameToIndex = db.getColumnFields("table_name")
	
	# TODO: do stuff!
			
	print "-------------------------------------------------------------------------"
	print "Some output!"
	print "-------------------------------------------------------------------------"

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "requiredOptionWithValue=", "optionWithoutValue", "justPrint"]
	requiredOptions = [ "requiredOptionWithValue"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	requiredOptionWithValue = optionsResult["requiredOptionWithValue"]
	optionWithoutValue = "optionWithoutValue" in optionsResult
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, log.LVL_ERROR)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(requiredOptionWithValue, optionWithoutValue, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':

	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
		
	raw_input("Press Enter to Exit ...")
