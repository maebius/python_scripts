import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
###
### MAIN PROGRAM
###
### @param sourceDirectory The source directory under we operate.
### @param targetDirectory [required] The location of the "master" archive, where we copy the entry if we decide to keep it.
### @param entryTable [required] the name of the entry DB table
### @param checkPartials [required] Do we process partial DB matches.
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(sourceDirectory, targetDirectory, entryTable, checkPartials, mode, justPrint):
	
	# here we put all our results
	results = dict()
	# do the stuff!!
	categorize.directoryCategorizerWithRoot(results, "", sourceDirectory, "", False, mode)
	
	printFields = []
	printFields.append("title")
	printFields.append("filename")
	printFields.append("path")
	printFields.append("root_path")
	printFields.append("category")
	printFields.append("issue")
	printFields.append("pub_year")
	printFields.append("size")

	db = database()
	db.connect("memarc")

	# THESE WE WILL WANT TO ADD TO DATABASE
	# if they exist (otherwise we try to add with empty string)
	# additionally added:
	# - "series_id" 
	# - "id" (auto-increment style)
	columns = list()
	# these should be always something else than empty ...
	columns.append("root_path")
	columns.append("path")	
	columns.append("filename")
	columns.append("full_path")
	columns.append("size")
	columns.append("file_type")
	columns.append("title")
	# these are optional (might not exist at all)
	columns.append("pub_year")
	columns.append("issue")
	columns.append("article")
	
	# these are checked so that if the some entry in database has already identical values in these fields
	# with the one we are adding, we consired them duplicate, do some checking, and only preserve one of the candinates
	duplicateCheckColumns = list()
	duplicateCheckColumns.append("title")
	duplicateCheckColumns.append("pub_year")
	duplicateCheckColumns.append("issue")

	# if we select new entry over some existing old, then we use the old entrys other values but these,
	# as they may contain some already modified stuff that we want to keep (for example, rating and notes)
	modifyColumns = list()
	modifyColumns.append("filename")
	modifyColumns.append("full_path")
	modifyColumns.append("path")
	modifyColumns.append("size")
	modifyColumns.append("file_type")

	entryColumnNameToIndex = db.getColumnFields(entryTable)

	modifiedEntries = 0
	shouldModifiedEntries = 0

	deletedFiles = 0
	shouldDeletedFiles = 0
	
	columnsDoNotMatch = 0
	columnsMatchButNotFoundInDB = 0
	columnsPartialMatchButNotFoundInDB = 0
	exactSingleMatch = 0
	
	selectedDatabaseEntry = 0
	selectedNewEntry = 0
	
	# just count the results first, for printing out progression
	totalCategoryCount = len(results.items())
	totalItemCount = 0
	for categoryKey, categoryValue in results.items():
		totalItemCount += len(categoryValue)
	
	log.log(LOG_NAME, 1, "total results in \"" + sourceDirectory + "\"" +\
		": " + str(totalItemCount) + " [categories: " + str(totalCategoryCount) + "]")

	handledItemCount = 0
	progressPrintDelta = 100
	nextProgressPrintCount = progressPrintDelta
	
	# found entries that partially match to a Database entry
	partialSingleHitsInDatabase = dict()
	partialMultipleHitsInDatabase = dict()
	
	# go throug every result category
	for categoryKey, categoryValue in results.items():
		
		# now, go through all entries in category
		for categoryEntry in categoryValue:			
			keysAndValues = dict()			
			checkKeysAndValues = dict()			

			newFilename = categoryEntry["filename"]
			
			# take entrys values that are of interest
			for col in columns:
				if col in categoryEntry:
					keysAndValues[col] = categoryEntry[col]

			# take all wanted values for duplicate check
			# NOTE! If entry does not have this, we make this a special case
			allColumnsInEntry = True
			for checkCol in duplicateCheckColumns:
				if checkCol in keysAndValues:
					checkKeysAndValues[checkCol] = keysAndValues[checkCol]
				else:
					allColumnsInEntry = False
			
			modifyExistingEntry = False
			
			alreadyInCount = db.getCountForWhere(entryTable, checkKeysAndValues)
			alreadyInEntry = 0
			databaseEntryFullPath = ""
			if alreadyInCount > 0:
				alreadyInEntry = db.getFirstFromTableWhere(entryTable, checkKeysAndValues) 				
				databaseEntryFullPath = alreadyInEntry[entryColumnNameToIndex["full_path"]]			

			# if was a partial hit, store information
			if not allColumnsInEntry:
				if alreadyInCount > 1:
					partialMultipleHitsInDatabase[newFilename] = databaseEntryFullPath
				elif alreadyInCount == 1:
					partialSingleHitsInDatabase[newFilename] = databaseEntryFullPath
			
			# do some info counting ...
			if allColumnsInEntry:
				if alreadyInCount == 0:
					columnsMatchButNotFoundInDB += 1
				elif alreadyInCount == 1:
					exactSingleMatch += 1
			else:			
				columnsDoNotMatch += 1
				if alreadyInCount == 0:
					columnsPartialMatchButNotFoundInDB += 1										
			
			if allColumnsInEntry or checkPartials:
				# it's an error if there are more than one in database, as it should have been pruned already!
				if not checkPartials and alreadyInCount > 1:
					log.log(LOG_NAME, 0,\
						"For: \"" + newFilename + "\" there are " + str(alreadyInCount) + " entries in DB with criteria:")
					for checkKey, checkValue in checkKeysAndValues.items():
						log.log(LOG_NAME, 0, str(checkKey) + ": " + str(checkValue))
					sys.exit(2)								
				# in this case we have one match in database for this entry ... make a selection between these two		
				elif alreadyInCount == 1:
					
					fileToRemove = ""
					
					# we're interested in file/path name and the filesize
					# 1. if there's "noads" in the entry, it gets selected
					# 2. if 1 is true for both, or  not true for both, then the bigger filesize wins
					
					databaseEntryPath = alreadyInEntry[entryColumnNameToIndex["path"]]
					newEntryPath = categoryEntry["full_path"]
					
					# if the filepaths are the same, this propably means we have just executed the script twice for same material
					# -> this means we should not delete or add anything
					if newEntryPath == databaseEntryFullPath:
						log.log(LOG_NAME, 1, "NOTE! duplicate (fullpath) in DB, skipping \"" + databaseEntryFullPath + "\"")
					else:
						sizeDatabaseEntry = alreadyInEntry[entryColumnNameToIndex["size"]]
						sizeNewEntry = categoryEntry["size"]
						
						# as a fail-safe, we check here that we still have the file that the DB says it has, and exit if not
						if not os.path.exists(databaseEntryFullPath):
							log.log(LOG_NAME, 0, "Don't have file (even if it is in Database): " + databaseEntryFullPath)
							sys.exit(2)
						
						noAdsInDatabaseEntry = databaseEntryFullPath.find("noads") != -1
						noAdsInNewEntry = newEntryPath.find("noads") != -1
						
						keepDatabaseEntry = True
						if noAdsInDatabaseEntry == True:
							if noAdsInNewEntry == True:
								keepDatabaseEntry =  sizeNewEntry > sizeDatabaseEntry
						else:
							if noAdsInNewEntry == True:
								keepDatabaseEntry = False
							else:
								keepDatabaseEntry = sizeDatabaseEntry >= sizeNewEntry

						if keepDatabaseEntry == True:
							selectedDatabaseEntry += 1
							# we preserve the entry found in database, and delete the new entry
							fileToRemove = newEntryPath
							log.log(LOG_NAME, 2, "selected EXISTING over new:")
						else:
							selectedNewEntry += 1
						
							# we want to keep the new entry, so we delete the old database entry (file),
							# and modify the database entry (we modify instead of delete/add as there might be some
							# already modified, interesting data such as ratings, notes, etc.)
							modifyExistingEntry = True
							
							# we delete the entry from database, and preserve/add the new one
							fileToRemove = databaseEntryFullPath
							log.log(LOG_NAME, 2, "selected NEW over existing:")
							
							# take the values we want to change from the new entry
							newKeysAndValues = dict()			
							for col in modifyColumns:
								if col in keysAndValues:
									newKeysAndValues[col] = keysAndValues[col]
							# swap
							keysAndValues = newKeysAndValues
								
						if len(fileToRemove) > 0:
							log.log(LOG_NAME, 2, "\tDB  [" + str(sizeDatabaseEntry) + "] - \"" + databaseEntryFullPath + "\"")
							log.log(LOG_NAME, 2, "\tNew [" + str(sizeNewEntry) + "] - \"" + newEntryPath + "\"")
							log.log(LOG_NAME, 2, "delete: \"" + fileToRemove + "\"")

							shouldDeletedFiles += 1

							if not justPrint:
								deletedFiles += 1
								os.remove(fileToRemove)
				
			if modifyExistingEntry:
				newCategoryDirectory = os.path.normpath(os.path.join(targetDirectory, databaseEntryPath))
				
				# manipulate filesystem
				if sourceDirectory != targetDirectory:
					# modify the entry as we move it 
					fullPathToFile = keysAndValues["full_path"]
					keysAndValues["root_path"] = targetDirectory
					keysAndValues["full_path"] = os.path.join(newCategoryDirectory, keysAndValues["filename"])
				
					log.log(LOG_NAME, 2, "move: \"" + fullPathToFile + "\" to: \"" + newCategoryDirectory + "\"")					
					if not justPrint:
						utility.move(fullPathToFile, newCategoryDirectory) 
				
				shouldModifiedEntries += 1
				
				log.log(LOG_NAME, 2, "update db-values for: \"" + fullPathToFile + "\"")
				if not justPrint:
					# update the existing database entry
					modifiedEntries += db.updateValues(entryTable, newKeysAndValues, checkKeysAndValues)
			
			handledItemCount += 1
			
			# do we want to print?
			if handledItemCount >= nextProgressPrintCount:			
				log.log(LOG_NAME, 1, "processed against DB"+\
					": " + str(handledItemCount) + " of " + str(totalItemCount))
				nextProgressPrintCount += progressPrintDelta
			
	deletedPreservedString = ""
	
	if not justPrint:
		# remove source, if empty
		deletedPreserved = fileUtility.deleteAllEmptyDirectories(sourceDirectory, False, justPrint)
		deletedPreservedString = "Directories deleted: " + str(len(deletedPreserved[0])) +\
			", preserved: " + str(len(deletedPreserved[1])) + "\n"
	
	if len(partialSingleHitsInDatabase) > 0:
		log.log(LOG_NAME, 0, "Partial SINGLE matches in DB:")	
		for entry, alreadyInPath in partialSingleHitsInDatabase.items():
			log.log(LOG_NAME, 0, "NEW \"" + entry + "\"\tDB \"" + alreadyInPath + "\"")

	if len(partialMultipleHitsInDatabase) > 0:
		log.log(LOG_NAME, 0, "Partial MULTIPLE matches in DB:")	
		for entry, alreadyInPath in partialMultipleHitsInDatabase.items():
			log.log(LOG_NAME, 0, "NEW \"" + entry + "\"\tDB \"" + alreadyInPath + "\"")
	
	lines = []		
	lines.append("Total item count: " + str(totalItemCount))
	lines.append("Cols don't match for: " + str(columnsDoNotMatch))
	lines.append("NOT in DB, exact col match: " + str(columnsMatchButNotFoundInDB))
	lines.append("NOT in DB, partial col match: " + str(columnsPartialMatchButNotFoundInDB))
	lines.append("Exact match in DB: " + str(exactSingleMatch))
	lines.append("Partial single/multiple DB matches: " + str(len(partialSingleHitsInDatabase)) + " / " + str(len(partialMultipleHitsInDatabase)))
	lines.append("Selected DB/NEW entry: " + str(selectedDatabaseEntry) + " / " + str(selectedNewEntry))
	lines.append("Deleted (/should) files: " + str(deletedFiles)  + " (" + str(shouldDeletedFiles) + ")")
	lines.append("Modified (/should) entries: " + str(modifiedEntries)  + " (" + str(shouldModifiedEntries) + ")")
	if len(deletedPreservedString) > 0: 
		lines.append(deletedPreservedString)
	log.printOutput(lines, 0, LOG_NAME)
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "target=", "mode=", "entryTable=", "partialMatches", "justPrint", "printToFile"]
	requiredOptions = [ "source", "target", "entryTable", "mode"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# source directory for files
	sourceDirectory = os.path.normpath(optionsResult["source"])
	# target directory for files
	targetDirectory = os.path.normpath(optionsResult["target"])
	# from what database table we check stuff?
	entryTable = optionsResult["entryTable"]
	# mode
	mode = optionsResult["mode"]
	# accept partial DB matches?
	partialMatches = "partialMatches" in optionsResult
	
	# just print?
	justPrint = "justPrint" in optionsResult
	# just print to file?
	printToFile = "printToFile" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	if printToFile:
		filenameToPrint = "file_list.txt"
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 2)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	if not os.path.exists(sourceDirectory):
		log.log(LOG_NAME, 0, "Source path \"" + sourceDirectory + "\" does not exist!")
	elif not os.path.exists(targetDirectory):
		log.log(LOG_NAME, 0, "Target path \"" + targetDirectory + "\" does not exist!")
	else:
		execute(sourceDirectory, targetDirectory, entryTable, partialMatches, mode, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
