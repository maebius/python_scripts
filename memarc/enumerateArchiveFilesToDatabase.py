import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

g_handledFilesCount = 0
g_fullFileCount = 0
g_afterHandledFilesPrintProgress = 0 # what is the next handled count after we print the progress
g_afterHandledDelta = 5000 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

def incrementHandledCount():
	global g_handledFilesCount
	global g_fullFileCount
	global g_afterHandledFilesPrintProgress
	global g_afterHandledDelta

	g_handledFilesCount += 1
	# print progress?
	if g_handledFilesCount  >= g_afterHandledFilesPrintProgress:
		# print!
		log.log(LOG_NAME, log.LVL_PROGRESS, "... handled files: "
			+ str(g_handledFilesCount)
			+ " of " + str(g_fullFileCount))
		# init next round
		g_afterHandledFilesPrintProgress += g_afterHandledDelta

def execute(folder, tableName, categoryTable, categoryField, mode, deleteWrongType, modifyIfPathDiffers, justPrint):

	global g_fullFileCount

	folder = os.path.normpath(os.path.abspath(folder))

	notInDB = []
	notInDBandWithoutCategory = []
	
	# these are checked so that if the some entry in database has already identical values in these fields
	# with the one we are adding, we consired them duplicate, do some checking, and only preserve one of the candinates
	duplicateCheckColumns = list()
	if not categoryField == None:
		duplicateCheckColumns.append(categoryField)
	
	duplicateCheckColumns.append("title")
	duplicateCheckColumns.append("file_type")
	duplicateCheckColumns.append("path")
	duplicateCheckColumns.append("pub_year")
	
	if mode == "tv":
		duplicateCheckColumns.append("episode")
	elif mode == "comic":
		duplicateCheckColumns.append("issue")

	# if we found duplicate (so that there is already in DB entry with the values of "duplicateCheckColumns",
	# and if we select new entry over the previous DB, then we use the old entrys other values but these,
	# as they may contain some already modified stuff that we want to keep (for example, rating and notes)
	modifyColumns = list()
	modifyColumns.append("disc_id")
	modifyColumns.append("filename")
	modifyColumns.append("path")
	modifyColumns.append("size")
	modifyColumns.append("file_type")
	
	notOfRightFileType = []

	db = database()
	db.connect("memarc")

	entryColumnNameToIndex = db.getColumnFields(tableName)

	isVideoFileMode = mode == "movie" or mode == "tv"
	
	categoryIndex = None
	categoryColumnNameToIndex = None
	if not categoryTable == None:
		categoryColumnNameToIndex = db.getColumnFields(categoryTable)
		categoryIndex = categoryColumnNameToIndex["id"]
	useYearInCategory = (categoryColumnNameToIndex) != None and ("pub_year" in categoryColumnNameToIndex)
		
	pathIndex = entryColumnNameToIndex["path"]
	filenameIndex = entryColumnNameToIndex["filename"]
	
	addCategoryIfMissing = not justPrint
	
	colsAndValues = dict()
	foundMissing = 0
	foundMissingWithoutCategory = 0	
	addedEntries = 0
	shouldAddedEntries = 0
	
	outOfSyncCount = 0
	outOfSyncCountHandled = 0
		
	fullCount = db.getCountFor(tableName)
	fileAndDirCount = fileUtility.getFileAndDirectoryCount(folder)
	g_fullFileCount = fileAndDirCount[0]

	interestingFiles = categorize.getFileTypeCount(folder, mode)

	preInfoLines = []
	
	preInfoLines.append("Entries in database: " + str(fullCount))
	preInfoLines.append("Interesting files (/all) to scan [in " + str(fileAndDirCount[1]) + " dirs]: " + str(interestingFiles) + " (/" + str(fileAndDirCount[0]) + ")")
	# form index ...
	indexFull = dict()
	indexFull[ "filename" ] = 0
	indexFull[ "disc_id" ] = 1
	indexFull[ "root_path" ] = 2
	indexFull[ "path" ] = 3
	indexFull["id"] = 4
	indexNoDisc = dict()
	indexNoDisc[ "filename" ] = 0
	indexNoDisc[ "root_path" ] = 2
	indexNoDisc[ "path" ] = 3
	
	# get all files in db
	allEntries = db.getFromTable(tableName, [ "filename", "disc_id", "root_path", "path", "id" ] )
	# construct a look-out map out of the entries
	entryLookUp = dict()
	# construct a look-out map out of the entries count
	entryCountLookUp = dict()
	dictionarizedCount = 0
	for entry in allEntries:
		dictionarizedCount += 1
		#compareName = entry[0].lower()
		compareName = categorize.getFullPath(entry, indexNoDisc).lower()
		
		if compareName in entryCountLookUp:
			entryCountLookUp[compareName] += 1
		else:
			entryCountLookUp[compareName] = 1
			
			entryLookUp[compareName] = dict()
			for key in indexFull.keys():
				entryLookUp[compareName][key] = entry[indexFull[key]]
			
	preInfoLines.append("Database look-up dict ready with " + str(dictionarizedCount) + " entries. Checking against files ...")
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	doNotIgnoreSubtitleFiles = not isVideoFileMode
	handledDirectoriesAsFiles = []
	
	#print sys.getdefaultencoding()
	for root, dirs, files in os.walk(folder, topdown=False):
		
		fileCountInFolder = len(files)
		
		for filename in files:
		
			logLines = []
			
			fullPath = os.path.normpath(os.path.abspath(os.path.join(root, filename)))
			isOfRightType = True

			wasMultiFile, fullPath, newRoot, directoryType = categorize.shouldHandleDirectoryAsEntry(fullPath, root)
			
			# .... if in video mode, and this is a dvd-format directory, some special stuff
			if wasMultiFile:
				# parse the root directory
				if fullPath in handledDirectoriesAsFiles:
					incrementHandledCount()
					continue
				handledDirectoriesAsFiles.append(fullPath)
				filename = os.path.split(fullPath)[1]
			else:
				isOfRightType = categorize.isFileTypeRight(fullPath, mode)
			
			# ... ensure that of right type
			if not isOfRightType:
					# some special handling for videos ...
				if doNotIgnoreSubtitleFiles or not fileUtility.isSubtitleFile(fullPath):
					notOfRightFileType.append(fullPath)
				
				incrementHandledCount()
				continue
			
			compareLower = stringUtilities.parseDriveLetter(fullPath)[1].lower()
			
			# ... the new and FAST way of checking if we have the entry in database
			count = 0
			if compareLower in entryCountLookUp:
				count = entryCountLookUp[compareLower]
			
			#print "checking: \"" + filenameLower + "\""
			
			tryToAdd = False
			tryToEdit = False
			
			if count > 1:
				log.log(LOG_NAME, log.LVL_ERROR, "Ambigous compare, " + str(count) + " matches in DB for: " + fullPath)
				
			elif count == 1:
				# compare to make sure that path is correct ...
				dbEntry = entryLookUp[compareLower]
				
				if dbEntry["disc_id"] != fileUtility.getDiscSerialForPath(fullPath):
					outOfSyncCount += 1
					dif = stringUtilities.getDifferencesAsString(categorize.getFullPathNoIndex(dbEntry), fullPath)
					logLines.append("Out-of-sync (1st=db, 2nd=file):\n" + dif)
					
					if modifyIfPathDiffers:
						tryToEdit = True
				
			else:
				tryToAdd = True
			
			if tryToAdd or tryToEdit:

				notInDB.append(fullPath)
				foundMissing += 1
				
				logLines.append("File not in DB: " + fullPath)

				wasMultiFile = fullPath in handledDirectoriesAsFiles
				
				categoryName = root
				
				if len(folder) > 0:
					parts = fullPath.partition(folder)
					categoryName = parts[2].replace(filename, "")
				
				categoryName = stringUtilities.parseAwayLeading(categoryName, "\\")
				categoryName = stringUtilities.parseAwayLeading(categoryName, "/")
				categoryName = stringUtilities.parseAwayTrailing(categoryName, "\\")
				categoryName = stringUtilities.parseAwayTrailing(categoryName, "/").strip()
				categoryName = categoryName.replace("\\", " | ").strip()

				category = categoryName.split(" | ")

				resultEntry = categorize.handleAsEntry(filename, fullPath, folder, mode, category, wasMultiFile, fileCountInFolder)
				
				if directoryType != None:
					resultEntry["file_type"] = directoryType
				
				if resultEntry != None and len(categoryName) > 0:
				
					# do we have separate table for the category?
					if not categoryIndex == None:
						# yes, check reference to that  (or create one)
						category, alreadyInCat, addedCat = categorize. getOrCreateCategory(\
							categoryTable, "name", categoryName, db, useYearInCategory, justPrint)
						categoryId = None
						if not category == None:
							categoryId = category[categoryIndex]
						if not categoryId == None:
							resultEntry[categoryField] = categoryId
					else:
						# no, just use the constructred string to the table itself, if field provided by input
						if not categoryField == None:
							resultEntry[categoryField] = categoryName
					
					if tryToEdit:
						newKeysAndValues = dict()
						whereKeysAndValues = dict()
						dbEntry = entryLookUp[compareLower]
						whereKeysAndValues["id"] = dbEntry["id"]
						
						fileEntryDisc = resultEntry["disc_id"]
						if fileEntryDisc != None and fileEntryDisc != dbEntry["disc_id"]:
							newKeysAndValues["disc_id"] = fileEntryDisc
						fileEntryRootPath = resultEntry["root_path"]
						if fileEntryRootPath != None and fileEntryRootPath != dbEntry["root_path"]:
							newKeysAndValues["root_path"] = fileEntryRootPath
						fileEntryPath = resultEntry["path"]
						if fileEntryPath != None and fileEntryPath != dbEntry["path"]:
							newKeysAndValues["path"] = fileEntryPath
						
						if len(newKeysAndValues) > 0:
							retVal = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False, justPrint)
							if retVal != None:
								outOfSyncCountHandled += 1
							
					elif tryToAdd:
						
						# logLines.append("Should add entry to DB: " + categorize.getAsString(resultEntry))
						shouldAddedEntries += 1
						
						alreadyInCount, filePathDuplicate, add, added, modify, modified, delete, deleted, keepDB =\
							categorize.addEntry(\
								resultEntry, db, tableName,\
								None, duplicateCheckColumns, modifyColumns,\
								folder, folder, False,\
								justPrint)
						
						addedEntries += added

				else:
					notInDBandWithoutCategory.append(fullPath)
					foundMissingWithoutCategory += 1
			
			log.printOutput(logLines, LOG_NAME, log.LVL_DETAIL)
		
			incrementHandledCount()

	notRightTypeFilesCount = len(notOfRightFileType)
	
	deleteThese = deleteWrongType and not justPrint
	
	if notRightTypeFilesCount > 0:
		log.log(LOG_NAME, log.LVL_PROGRESS, "")
		
		if deleteThese:
			log.log(LOG_NAME, log.LVL_PROGRESS, "Not right-type files [" + str(notRightTypeFilesCount) + "], deleting: ")
		else:
			log.log(LOG_NAME, log.LVL_PROGRESS, "Not right-type files [" + str(notRightTypeFilesCount) + "], consider deleting: ")
			
		log.log(LOG_NAME, log.LVL_PROGRESS, "")
		for entry in notOfRightFileType:
			log.log(LOG_NAME, log.LVL_PROGRESS, entry)
			if deleteThese:
				os.remove(entry)
			
		log.log(LOG_NAME, log.LVL_PROGRESS, "")
	
	if len(notInDBandWithoutCategory) > 0:
		log.log(LOG_NAME, log.LVL_PROGRESS, "")
		log.log(LOG_NAME, log.LVL_PROGRESS, "Not in DB and without pre-constructed category [" + str(len(notInDBandWithoutCategory)) + "]:")
		log.log(LOG_NAME, log.LVL_PROGRESS, "")
		for entry in notInDBandWithoutCategory:
			log.log(LOG_NAME, log.LVL_PROGRESS, entry)
		log.log(LOG_NAME, log.LVL_PROGRESS, "")
	
	lines = []		
	lines.append("Count of not in DB (/without valid category): " + str(foundMissing) + " (/" + str(foundMissingWithoutCategory) + ")")
	lines.append("Added entries (/should): " + str(addedEntries) + " (/" + str(shouldAddedEntries) + ")")
	lines.append("Out-of-sync repaired (/should): " + str(outOfSyncCountHandled) + " (/" + str(outOfSyncCount) + ")")
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [\
		"table=", "categoryTable=", "categoryField=", "path=",\
		"mode=", "justPrint", "printToFile=", "verbose=",\
		"deleteWrongType", "modifyIfPathDiffers", "directExit"\
		]
	requiredOptions = [ "table", "path", "mode"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	categoryTable = None
	categoryField = None
	table = optionsResult["table"]
	verbose = log.LVL_PROGRESS
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])
		
	if "categoryTable" in optionsResult:
		categoryTable = optionsResult["categoryTable"]
	if "categoryField" in optionsResult:
		categoryField = optionsResult["categoryField"]
	path = optionsResult["path"]
	mode = optionsResult["mode"]
	
	if categorize.g_acceptedModes.count(mode) == 0:
		utility.errorExit("Error! Mode \"" + mode + "\" not supported!")
	
	# just print?
	justPrint = "justPrint" in optionsResult

	global g_directExit
	g_directExit = "directExit" in optionsResult
	
	deleteWrongType = "deleteWrongType" in optionsResult
	modifyIfPathDiffers = "modifyIfPathDiffers" in optionsResult
	
	# just print to file?
	filenameToPrint = ""
	if "printToFile" in optionsResult:
		filenameToPrint = optionsResult["printToFile"]
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	logToStdOut = len(filenameToPrint) == 0
	
	log.init(logToStdOut, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(stringUtilities.LOG_NAME, log.LVL_ERROR)
	log.add(categorize.LOG_NAME, log.LVL_ERROR)
	
	#log.add(categorize.LOG_NAME, log.LVL_DETAIL)
	#log.add(stringUtilities.LOG_NAME, log.LVL_DETAIL)
	
	log.add(fileUtility.LOG_NAME, log.LVL_ERROR)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(path, table, categoryTable, categoryField, mode, deleteWrongType, modifyIfPathDiffers, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	


g_directExit = False

if __name__ == '__main__':

	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
	
	if not g_directExit:
		raw_input("Press Enter to Exit ...")