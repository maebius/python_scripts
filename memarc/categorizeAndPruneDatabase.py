import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
###
### MAIN PROGRAM
###
### This script takes a root folder, interprets its as a media archive root, constructs categories out of it so
### that the folder structure implements a category hierarchy. Then it checks if such entry founds already in SQL Database.
### If yes, we check if this one is better than the already found. If it is, the old one is deleted (both from SQL and filesystem). The new one
### is copied to the target directory (thought to be the master archive root), the entry modified to these changes and then added to the SQL table. 
###
### @param sourceDirectory The source directory under we operate.
### @param targetDirectory [required] The location of the "master" archive, where we copy the entry if we decide to keep it.
### @param categoryTable [required] the name of the category DB table
### @param entryTable [required] the name of the entry DB table
### @param enableCategories [required] do we enable categories, if not, then handling all "from one directory"
### @param deleteDuplicates Do we delete the duplicates we found from disk and database?
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(sourceDirectory, targetDirectory,\
					categoryTable, entryTable, enableCategories,\
					mode,\
					deleteDuplicates, justPrint):

	# these are checked so that if the some entry in database has already identical values in these fields
	# with the one we are adding, we consired them duplicate, do some checking, and only preserve one of the candinates
	duplicateCheckColumns = list()
	duplicateCheckColumns.append("series_id")
	duplicateCheckColumns.append("title")
	duplicateCheckColumns.append("path")
	duplicateCheckColumns.append("pub_year")
	if mode == "comic":
		duplicateCheckColumns.append("issue")
	elif mode == "tv":
		duplicateCheckColumns.append("episode")

	# if we found duplicate (so that there is already in DB entry with the values of "duplicateCheckColumns",
	# and if we select new entry over the previous DB, then we use the old entrys other values but these,
	# as they may contain some already modified stuff that we want to keep (for example, rating and notes)
	modifyColumns = list()
	modifyColumns.append("filename")
	modifyColumns.append("disc_id")
	modifyColumns.append("path")
	modifyColumns.append("size")
	modifyColumns.append("file_type")
	
	ok = categorize.categorizeAndPutToDatabase(\
					sourceDirectory, targetDirectory,\
					categoryTable, "series_id", entryTable, enableCategories,\
					mode, deleteDuplicates,\
					None, duplicateCheckColumns, modifyColumns,\
					justPrint)
	

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "mode=", "source=", "target=", "categoryTable=", "entryTable=", "verbose=", "justPrint", "printToFile", "deleteDuplicates"]
	requiredOptions = [ "mode", "source", "categoryTable", "entryTable"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# mode
	acceptedPostFixes = 0	
	mode = optionsResult["mode"]
	if not mode == "comic" and not mode == "tv":
		print "Error! Mode \"" + mode + "\" not supported!"
		sys.exit(2)
	
	# source directory for files
	sourceDirectory = os.path.normpath(optionsResult["source"])
	# target directory for files
	targetDirectory = sourceDirectory
	if "target" in optionsResult:
		targetDirectory = os.path.normpath(optionsResult["target"])
	# if duplicates found, do we delete them?
	deleteDuplicates = "deleteDuplicates" in optionsResult	
	# just print?
	justPrint = "justPrint" in optionsResult
	# just print to file?
	printToFile = "printToFile" in optionsResult

	verbose = 2
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])
		
	categoryTable = optionsResult["categoryTable"]
	entryTable = optionsResult["entryTable"]	
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	if printToFile:
		filenameToPrint = "file_list.txt"
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	#log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, 2)
	log.add(fileUtility.LOG_NAME, log.LVL_ERROR)
	log.add(database.LOG_NAME, log.LVL_ERROR)
		
	if not os.path.exists(sourceDirectory):
		log.log(LOG_NAME, 0, "Source path \"" + sourceDirectory + "\" does not exist!")
	else:
		execute(sourceDirectory, targetDirectory, categoryTable, entryTable, True, mode, deleteDuplicates, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
