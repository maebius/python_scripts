import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
LOG_NAME = "main"

def execute(sourceDirectory, targetDirectory,\
					enableCategories,\
					deleteDuplicates, justPrint):
		
	columns = list()
	# these should be always something else than empty ...
	columns.append("root_path")
	columns.append("path")	
	columns.append("filename")
	columns.append("disc_id")
	columns.append("size")
	columns.append("file_type")
	columns.append("title")
	columns.append("tags")
	columns.append("genre")
	columns.append("subtitle")
	# these are optional (might not exist at all)
	columns.append("pub_year")
	columns.append("video_width")
	columns.append("video_height")
	columns.append("video_fps")
	columns.append("duration_minutes")	
	columns.append("article")

	# these are checked so that if the some entry in database has already identical values in these fields
	# with the one we are adding, we consired them duplicate, do some checking, and only preserve one of the candinates
	duplicateCheckColumns = list()
	duplicateCheckColumns.append("title")
	duplicateCheckColumns.append("path")
	duplicateCheckColumns.append("pub_year")

	# if we found duplicate (so that there is already in DB entry with the values of "duplicateCheckColumns",
	# and if we select new entry over the previous DB, then we use the old entrys other values but these,
	# as they may contain some already modified stuff that we want to keep (for example, rating and notes)
	modifyColumns = list()
	modifyColumns.append("filename")
	modifyColumns.append("full_path")
	modifyColumns.append("path")
	modifyColumns.append("size")
	modifyColumns.append("file_type")
	
	ok = categorize.categorizeAndPutToDatabase(\
					sourceDirectory, targetDirectory,\
					"", "", "movies", enableCategories,\
					"movie", deleteDuplicates,\
					columns, duplicateCheckColumns, modifyColumns,\
					justPrint)

	if not ok:
		log.log(LOG_NAME, 0, "Exited premately due to previous error!")


def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "target=", "verbose=", "justPrint", "printToFile", "deleteDuplicates"]
	requiredOptions = [ "source"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# mode
	acceptedPostFixes = 0	
	
	# source directory for files
	sourceDirectory = os.path.normpath(optionsResult["source"])
	# target directory for files
	targetDirectory = sourceDirectory
	if "target" in optionsResult:
		targetDirectory = os.path.normpath(optionsResult["target"])
	# if duplicates found, do we delete them?
	deleteDuplicates = "deleteDuplicates" in optionsResult	
	# just print?
	justPrint = "justPrint" in optionsResult
	# just print to file?
	printToFile = "printToFile" in optionsResult

	verbose = 2
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])
		
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	if printToFile:
		filenameToPrint = "file_list.txt"
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	categorize.afterHandledDelta = 10 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	log.add(fileUtility.LOG_NAME, 0)
	
	if not os.path.exists(sourceDirectory):
		log.log(LOG_NAME, 0, "Source path \"" + sourceDirectory + "\" does not exist!")
	else:
		execute(sourceDirectory, targetDirectory, True, deleteDuplicates, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
