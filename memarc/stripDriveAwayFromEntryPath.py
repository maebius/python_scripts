
import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

LOG_NAME = "main"

def satisfies(db, tableName, entry, index):
	if not "root_path" in index:
		return False
	
	root = entry[index["root_path"]]
	
	drive, path = os.path.splitdrive(root)
	if drive != None and len(drive) > 0:
		return True
	
	if "disc_id" in index:
		disc_id = entry[index["disc_id"]]
		if disc_id == None or len(disc_id) == 0:
			return True
	
	return False

def perform(db, tableName, entry, index, justPrint):

	whereKeysAndValues = dict()
	whereKeysAndValues["id"] = entry[index["id"]]

	newKeysAndValues = dict()

	root = entry[index["root_path"]]
	drive, path = os.path.splitdrive(root)
	if drive != None and len(drive) > 0:
		newKeysAndValues["root_path"] = entry[index["root_path"]][3 : ]

	# take drive-info
	drives = fileUtility.getDiscInfo()
	# ... need to deduct from "old-style" path?
	entryLetter = root[0 : 2].upper()
	if entryLetter in drives:
		drive = drives[entryLetter]
		newKeysAndValues["disc_id"] = drive["serial"]

	if len(newKeysAndValues) > 0:
		retVal = db.updateValuesWhere(tableName, newKeysAndValues, whereKeysAndValues, False, justPrint)
		if retVal != None:
			return 1
	
	return 0

def execute(tableName, justPrint):

	db = database()
	db.connect("memarc")

	resultSet = db.getAllFromTable(tableName)
	entryColumnNameToIndex = db.getColumnFields(tableName)
	
	modifiedEntries = 0
	shouldModifiedEntries = 0
	
	handledItemCount = 0
	afterHandledItemsPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 100 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print

	fullItemCount = db.getCountFor(tableName)	
	preInfoLines = []
	preInfoLines.append("Entries in database: " + str(fullItemCount))
	log.printOutput(preInfoLines, LOG_NAME, log.LVL_PROGRESS)
	
	for entry in resultSet:

		if satisfies(db, tableName, entry, entryColumnNameToIndex):
			shouldModifiedEntries += 1
			modifiedEntries += perform(db, tableName, entry, entryColumnNameToIndex, justPrint)
			
		handledItemCount += 1
			# print progress?
		if handledItemCount  >= afterHandledItemsPrintProgress:
			# print!
			log.log(LOG_NAME, log.LVL_PROGRESS, "handled"+\
				" files: " + str(handledItemCount) + " of " + str(fullItemCount))
			# init next round
			afterHandledItemsPrintProgress += afterHandledDelta

	lines = []		
	lines.append("Modified (/should): " + str(modifiedEntries) + " (" + str(shouldModifiedEntries) + ")")
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "table=", "justPrint", "verbose="]
	requiredOptions = [ "table"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	table = optionsResult["table"]
	# just print?
	justPrint = "justPrint" in optionsResult
	verbose = log.LVL_PROGRESS
	
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])

	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, verbose)
	log.add(categorize.LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_PROGRESS)
	
	execute(table, justPrint)
	
	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()