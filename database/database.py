##  @package database
#   Class for accessing and manipulating MySQL database.

import os
import MySQLdb
from utility import utility
from utility.log import log

##  Class representing MySQL database.
class database:

	#LOG_NAME = database.__class__.__name__
	LOG_NAME = __name__

	# server address
	m_host = "84.248.93.124"
	# port
	m_port = 3306
	# user
	m_user = "maebius"
	# password
	m_password = "gigeli42quu"
	# database
	m_databaseName = ""
	# our database object
	m_database = 0
	# our database cursor
	m_cursor = 0
	# how many times we have tried reconnect
	m_reconnectCount = 0

	##  Set parameters for the database.
	#   @param self	
	#   @param host
	#   @param port	
	#   @param user	
	#   @param password
	def setParameters(self, host, port, user, password):
		self.m_host = host
		self.m_port = port
		self.m_user = user
		self.m_password = password

	##  Method to connect to mysql database.
	#   @param self
	#   @param databaseName
	def connect(self, databaseName):
		if self.m_cursor != 0:
			self.close()

		self.m_databaseName = databaseName

		try:
			self.m_database = MySQLdb.connect(
				host=self.m_host, port=self.m_port, user=self.m_user, 
				passwd=self.m_password, db=self.m_databaseName)
		except MySQLdb.Error:
			log.log(database.LOG_NAME, log.LVL_ERROR, "MySQLError: " + str(self.m_database.info()))
			
		self.m_cursor = self.m_database.cursor()

	##  Closes the connection.
	#   @param self
	def close(self):
		if self.m_cursor == 0:
			return -1
		self.m_cursor.close()
		
	##  Method to do a query and return all answers.
	#   @param self
	#   @param queryString
	def query(self, queryString, justPrint = False):
		
		if justPrint:
			log.log(database.LOG_NAME, log.LVL_PROGRESS, "\"" + queryString + "\"")
			return None
		
		tryToReconnect = False
		
		if self.m_cursor == 0:
			tryToReconnect = True
		else:
				
			log.log(database.LOG_NAME, log.LVL_DETAIL, "\"" + queryString + "\"")

			try:
				self.m_cursor.execute(queryString)
				self.m_database.commit()
				return self.m_cursor.fetchall()
			except Exception:
				log.log(database.LOG_NAME, log.LVL_ERROR, "MySQLError: " + str(self.m_database.info()))
				log.log(database.LOG_NAME, log.LVL_ERROR, "query: \"" + str(queryString) + "\"")
				tryToReconnect = True

		if tryToReconnect:
			# already tried? do not try again ...
			if self.m_reconnectCount > 0:
				log.log(database.LOG_NAME, log.LVL_ERROR, "Already tried reconnecting, aborting ...")
				return None
			
			# try to reconnect
			self.m_reconnectCount += 1
			self.connect(self.m_databaseName)
			if self.m_cursor != 0:
				self.m_reconnectCount = 0
			return self.query(queryString)
		
		return None
	
	##  Method to query password.
	#   @param self
	#   @param username
	#   @param password
	def isPasswordBad(self, username, password):
		
		query = "",
		"SELECT COUNT FROM memarc_users ",
		"WHERE userid = ", username,
		" AND passwd = ", password
		
		result = self.query(query)
				
		if len(result) > 0:
			return 1
		
		return 0
	
	## Method to get the number of entries/rows.
	#   @param self
	#   @param table
	def getCountFor(self, table):
		query = "SELECT COUNT(*) FROM " + table
		result = self.query(query)
		return result[0][0]
	
	
	## Get matching index for column name if any
	#   @param self
	#   @param table
	#   @param columnName
	def getMatchingIndexForColumnName(self, table, columnName):
			
		description = self.getDescription(table)
		currentIndex = 0
		for row in description:
			if row[0] == columnName:
				return currentIndex
			else:
				currentIndex += 1			
		return -1
		
	## Get description for table
	#   @param self
	#   @param table
	def getDescription(self, table):
		
		queryString = "DESCRIBE " + table			
		result = self.query(queryString)		
		return result
	
	## Method to get the number of entries/rows.
	#   @param self
	#   @param table
	#   @param colsAndValues
	def getCountForWhere(self, table, colsAndValues):
		
		queryString = "SELECT COUNT(*) FROM " + table + " WHERE "	
		entries = []
		for key, value in colsAndValues.items():
			entries.append(self.formatKeyValuePair(key, value))
		queryString += " AND ".join(entries)
		result = self.query(queryString)		
		count = result[0][0]
		
		return count
	
	##  Method to get all the column labels of a table.
	#   @param self
	#   @param table
	#   @param column
	def getColumnFields(self, table):
		columnNameToIndexMap = dict()
		description = self.getDescription(table)
		index = 0
		for row in description:
			columnNameToIndexMap[row[0]] = index
			index += 1
		return columnNameToIndexMap
	
	## Helper method to parse the pair for suitable SQL db form.
	def formatKeyValuePair(self, key, value):
		result = str(key) + " = \""
		if utility.isNumber(value):
			result += str(value)
		elif len(str(value)) > 0:
			# we have a string, replace backslashes so they do not disappear ...
			result += str(value).replace("\\", "\\\\")
		
		result += "\""
		return result
	
		## Helper method to parse the pair for suitable SQL db form.
	def formatKeyValuePairIgnoreCase(self, key, value):
		result = "LOWER(" + str(key) + ") = \""
		if utility.isNumber(value):
			result += str(value)
		elif len(str(value)) > 0:
			# we have a string, replace backslashes so they do not disappear ...
			result += str(value).replace("\\", "\\\\").lower()
		
		result += "\""
		return result
	
	##  Method to delete entries from table with given column/value pairs.
	#   @param self
	#   @param table
	#   @param colsAndValues
	def deleteFromTableWhere(self, table, colsAndValues, justPrint):
		queryString = "DELETE FROM " + table + " WHERE " 
		entries = []
		for key, value in colsAndValues.items():
			entries.append(self.formatKeyValuePair(key, value))
		queryString += " AND ".join(entries)
		#print queryString
		return self.query(queryString, justPrint)
	
	##  Method to get all columns from a table.
	#   @param self
	#   @param table
	def getAllFromTable(self, table):
		queryString = "SELECT * FROM " + table
		return self.query(queryString)

	##  Method to get selected columns from a table.
	#   @param self
	#   @param table
	#   @param columns
	def getFromTable(self, table, columns):
		cols = ", ".join(columns)
		queryString = "SELECT " + cols + " FROM " + table
		return self.query(queryString)
		
	##  Method to get wanted columns from a table matching column/value pairs.
	#   @param self
	#   @param table
	#   @param colsAndValues
	def getFromTableWhere(self, table, colsAndValues):
		queryString = "SELECT * FROM " + table + " WHERE " 
		entries = []
		for key, value in colsAndValues.items():
			entries.append(self.formatKeyValuePair(key, value))
		queryString += " AND ".join(entries)
		return self.query(queryString)

	##  Method to get wanted columns from a table matching column/value pairs.
	#   @param self
	#   @param table
	#   @param colsAndValues
	def getFromTableWhereIgnoreCase(self, table, colsAndValues):
		queryString = "SELECT * FROM " + table + " WHERE " 
		entries = []
		for key, value in colsAndValues.items():
			entries.append(self.formatKeyValuePairIgnoreCase(key, value))
		queryString += " AND ".join(entries)
		return self.query(queryString)

		
	##  Method to get wanted columns from a table including column/value pairs.
	#   @param self
	#   @param table
	#   @param colsAndValues
	def getFromTableWhereLike(self, table, colsAndValues):
		queryString = "SELECT * FROM " + table + " WHERE " 
		entries = []
		for key, value in colsAndValues.items():		
			pair = str(key) + " like \"%"
			pair += str(value)
			pair += "%\""
			entries.append(pair.replace("\\", "\\\\"))
			
		queryString += " AND ".join(entries)
		return self.query(queryString)
		
	##  Method to get first entry from table with matching column/value pairs.
	#   @param self
	#   @param table
	#   @param colsAndValues
	def getFirstFromTableWhere(self, table, colsAndValues):
		result = self.getFromTableWhere(table, colsAndValues)
		if len(result) > 0:		
			return self.getFromTableWhere(table, colsAndValues)[0]
		return None
	
	##  Method to add entries to table.
	#   @param self
	#   @param table
	#   @param colsAndValues
	#   @param addAdditionDate
	def addEntries(self, table, colsAndValues, addAdditionDate, justPrint):
		query = list()
		query.append("INSERT INTO " + table + " SET ")

		values = []
		for key, value in colsAndValues.items():
			values.append(self.formatKeyValuePair(key, value))
		if addAdditionDate:
			values.append("added = NOW()")

		query.append(", ".join(values))
		queryString = "".join(query)
		result = self.query(queryString, justPrint)

		return 1

	##  Method to update existing entrys values in table.
	#   @param self
	#   @param newColsAndValues
	#   @param whereColsAndValues
	def updateValuesWhere(self, table, newColsAndValues, whereColsAndValues, ignoreCase, justPrint):
		queryString = "UPDATE " + table + " SET " 
		newEntries = []
		for newKey, newValue in newColsAndValues.items():
			newEntries.append(self.formatKeyValuePair(newKey, newValue))
		queryString += ", ".join(newEntries)
		
		queryString += " WHERE "
		whereEntries = []
		for whereKey, whereValue in whereColsAndValues.items():
			if ignoreCase:
				whereEntries.append(self.formatKeyValuePairIgnoreCase(whereKey, whereValue))
			else:
				whereEntries.append(self.formatKeyValuePair(whereKey, whereValue))
		queryString += " AND ".join(whereEntries)
		return self.query(queryString, justPrint)
	
	##  Method to get biggest value of tables column,
	##  only makes sense for number valued columns.
	#   @param self
	#   @param table
	#   @param column
	def getBiggest(self, table, column):
		utility.errorExit("TODO: implement this ...")
	
	##  Method to check if column exist in a table with given column/value pairs.
	#   @param self
	#   @param table
	#   @param what
	#   @param columns
	#   @param values
	def isAvailable(self, table, what, columns, values):
		utility.errorExit("TODO: implement this ...")
	
	##  Method to get all leading alphabets of tables columns entries.
	#   @param self
	#   @param table
	#   @param what
	def getAlphabets(self, table, what):
		utility.errorExit("TODO: implement this ...")
	
	##  Method to get all leading alphabets of tables columns entries that match 
	##  the given column/value pairs.
	#   @param self
	#   @param table
	#   @param what
	#   @param columns
	#   @param values
	def getAlphabetsWhere(self, table, what, columns, values):
		utility.errorExit("TODO: implement this ...")

	##  Method to do some searching in database, given the "sessionParams" class.
	#   @param self
	#   @param sessionParams
	def doSearch(self, sessionParams):

		# do we want only entries with a search criteria?
		search_clause = ""
		
		if sessionParams.has("search_column") and\
			sessionParams.has("search_value"):
			if sessionParams.get("range") == "all":		
				search_clause = \
				" WHERE " + self.formatKeyValuePair(\
					sessionParams.get("search_column"), sessionParams.get("search_value"))
			else:	
				search_clause = \
				" AND " + self.formatKeyValuePair(\
					sessionParams.get("search_column"), sessionParams.get("search_value"))

		# build up query for fetching entries from table
		entry_query = ""

		selected_columns = sessionParams.get("table_entry_selection")
		if selected_columns != "*":
			selected_columns = "DISTINCTROW " + selected_columns

		if sessionParams.get("range") == "all":	
			entry_query = "SELECT " + selected_columns + " FROM "\
			+ sessionParams.get("table") + search_clause + " ORDER BY "\
			+ sessionParams.get("order_by") + " " + sessionParams.get("order")

		elif sessionParams.get("range") == "new":	
			
			stamp = time.strftime("%Y-%m-1")
			entry_query = "SELECT " + selected_columns + " FROM "\
			+ sessionParams.get("table") + " WHERE added >= \"" + stamp\
			+ "\" ORDER BY "\
			+ sessionParams.get("order_by") + " " + sessionParams.get("order")
		else:
			entry_query = "SELECT " + selected_columns + " FROM "\
			+ sessionParams.get("table") + " WHERE name LIKE \""\
			+ sessionParams.get("range") + "\" " + search_clause\
			+ sessionParams.get("order_by") + " " + sessionParams.get("order")

		return self.query(entry_query)	
			
###
### MAIN PROGREM
###

# doing some tests
#db = database()

#db.connect("kiilto")
#db.setDebug(1)

#cols = dict()
#cols["platform"] = "wii" 
#cols["genre"] = "puzzle" 

#result = db.getFirstFromTableWhere("memarc_games", "name", cols)
#print result[0]