import os
import sys

from utility import utility
from utility.fileUtility import fileUtility
from utility.categorize import categorize
from utility.log import log
from utility.stringutilities import stringUtilities

###
### MAIN PROGRAM
###
### This script takes a root folder, and tries to form categories out of all the files found under it.
### Then the script makes a folder structure out of the categories to the location given as argument,
### and moves all the files from source under this new folder structure. If the source folder becomes empty
### (as it should), it is deleted, otherwise a warning is issued about this.
###
### @param source [required] The source directory under we operate.
### @param target [required] Where we create the category folder structure.
### @param justPrint [optional]  Do we just print the "should-do" operations, without doing them (for debug)
### @param copy [optional] Do we copy the files instead of just moving them from source to target. 
###

LOG_NAME = "main"

# parse input args
acceptedArgs = ""
acceptedOptions = [ "source=", "target=", "mode=", "justPrint", "copy"]
requiredOptions = [ "source", "target", "mode"]
argsResult = []
optionsResult = dict()
utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

# source directory for files
sourceDirectory = optionsResult["source"]
# target directory for files
targetDirectory = optionsResult["target"]
# mode
mode = optionsResult["mode"]
# just print?
justPrint = "justPrint" in optionsResult

filenameToPrint = ""
log.init(True, False, filenameToPrint)
log.add(LOG_NAME, log.LVL_DETAIL)
#log.add(categorize.LOG_NAME, log.LVL_DETAIL)
#log.add(fileUtility.LOG_NAME, log.LVL_PROGRESS)

# copy instead of move?
copyInsteadOfMove = "copy" in optionsResult

# print some basic stuff
utility.printProgramInfo(optionsResult)
# store start time
startTime = utility.getCurrentTime()

# our current directory is where we change back eventually
originalDirectory = os.getcwd()
# here we put all our results
results = dict()
# do the stuff!!
categorize.directoryCategorizerWithRoot(results, "", sourceDirectory, "", True, mode)
categories = dict()

for key, value in results.items():
	for entry in value:
		# here, we treat every "title" as category and store it, disregarding entrys every other field ...
		category = entry["title"]
		fullpath = categorize.getFullPathNoIndex(entry)
		
		# check if category already exists, create if not
		if not category in categories:
			categories[category] = []

		categories[category].append(fullpath)

createdDirectories = 0
shouldCreatedDirectories = 0
processedFiles = 0

if not os.path.exists(targetDirectory):
	log.log(LOG_NAME, log.LVL_PROGRESS, "we should create directory: \"" + targetDirectory) 
	if not justPrint:
		fileUtility.makeDir(targetDirectory)

keys = categories.keys()
keys.sort()
for key in keys:
	dir = os.path.join(targetDirectory, key)
	value = categories[key]
	
	shouldCreatedDirectories += 1
	
	resultString = ""	
	fileUtility.makeDir(dir, justPrint)
	if not justPrint:
		createdDirectories += 1
		
	log.log(LOG_NAME, log.LVL_PROGRESS, "CATEGORY: " + key) 
	
	for entry in value:		
		#log.log(LOG_NAME, log.LVL_DETAIL, "this: " + entry + " to: " + dir)
		processedFiles += 1
		
		if copyInsteadOfMove:
			fileUtility.copy(entry, dir, justPrint)
		else:
			fileUtility.move(entry, dir, justPrint)
	
deletedPreservedString = ""
if not justPrint:

	# do some article processing for target
	fileUtility.renameArticleDirectories(targetDirectory)

	# remove source, if empty
	deletedPreserved = fileUtility.deleteAllEmptyDirectories(sourceDirectory, False, justPrint)
	deletedPreservedString = "Directories deleted: " + str(len(deletedPreserved[0])) +\
		", preserved: " + str(len(deletedPreserved[1])) + "\n"
	
# store stop time
stopTime = utility.getCurrentTime()

# print output
lines = []
lines.append("Created directories: " + str(createdDirectories) + ", moved files: " + str(processedFiles))
if len(deletedPreservedString) > 0: 
	lines.append(deletedPreservedString)
log.printOutput(lines, log.LVL_ERROR, LOG_NAME)

log.finalize()
	
# print some statistics
utility.printProcessingTime(startTime, stopTime)