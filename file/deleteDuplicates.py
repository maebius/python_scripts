import os
import sys

from utility import utility
from fileUtility import fileUtility
from log import log

###
### MAIN PROGRAM
###
### This script goes through the given directory and checks if it contains duplicate files.
### If the exact name of the file is same, or the filesize is exactly the same, the smaller (or if same size, just the other)
### is deleted. If "onlySameDir" option is used, then we do not operate cross directory boundaries.
###
### @param source [required] The source directory under we operate.
### @param onlySameDir [optional]  Do we operate only in same directory.
### @param justPrint [optional]  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

# parse input args
acceptedArgs = ""
acceptedOptions = [ "source=", "onlySameDir", "justPrint"]
requiredOptions = [ "source"]
argsResult = []
optionsResult = dict()
utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

# source directory for files
sourceDirectory = optionsResult["source"]
# only delete same sized from same directory? if no, then deleting cross directory boundaries
onlySameDir = "onlySameDir" in optionsResult
# do we just print the entries?
justPrint = "justPrint" in optionsResult
# just print to file?
printToFile = "printToFile" in optionsResult

filenameToPrint = ""
if printToFile:
	filenameToPrint = "comic_list.txt"

# print some basic stuff
utility.printProgramInfo(optionsResult)
# store start time
startTime = utility.getCurrentTime()

log.init(True, False, filenameToPrint)
log.add(LOG_NAME, 1)
log.add(fileUtility.LOG_NAME, 1)

# do the pruning

print "Searching same-named entries (preserve the one with biggest filesize)..."
deletedSameNamed = fileUtility.deleteSameNamedFiles(sourceDirectory, onlySameDir, justPrint)
print "Searching same-sized entries ..."
deletedSameSized = fileUtility.deleteSameSizedFiles(sourceDirectory, onlySameDir, justPrint)

print "-------------------------------------------------------------------------"
print "Deleted same-named entries: " + str(deletedSameNamed)
print "Deleted same-sized entries: " + str(deletedSameSized)
print "-------------------------------------------------------------------------"

log.finalize()

# store stop time
stopTime = utility.getCurrentTime()

# print some statistics
utility.printProcessingTime(startTime, stopTime)