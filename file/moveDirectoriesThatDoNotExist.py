import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(source, target, justPrint):

	files = []
	foundMatches = []
	entryDict = dict()

	separator = " - "
	# do the pruning
	files = os.listdir(source)

	moveAsNewCount = 0
	alreadyExistedCount = 0
	totalSize = 0
	for entry in files:
		dir = os.path.join(source, entry)
		
		if os.path.isdir(dir):
			
			destination = os.path.join(target, entry)
			
			if not os.path.exists(destination):
				thisSize = fileUtility.getSizeOfDir(dir)
				totalSize += thisSize
				log.log(LOG_NAME, log.LVL_PROGRESS, "from: \"" + dir + "\" to: \"" + destination + "\" (size: " + fileUtility.inGB(thisSize) + ")")
			
				fileUtility.move(dir, destination, justPrint)
				moveAsNewCount += 1
			
			else:
				alreadyExistedCount += 1
	
	print "-------------------------------------------------------------------------"
	print "Moved as new: " + str(moveAsNewCount) + " (totalSize: " + fileUtility.inGB(totalSize) + "... and not: " + str(alreadyExistedCount )+ ")"
	print "-------------------------------------------------------------------------"
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "target=", "justPrint"]
	requiredOptions = ["source"]
	argsResult = []
	optionsResult = dict()
	
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	source = optionsResult["source"]
	target = optionsResult["target"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, log.LVL_DETAIL)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	log.add(fileUtility.LOG_NAME, log.LVL_ERROR)
	
	execute(source, target, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
		
	raw_input("Press Enter to Exit ...")