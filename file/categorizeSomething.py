import os
import sys

from utility import utility

# our computer related categories
computerCategories = {
	'programs' :  
		['visual studio', 'atlas',
		'photoshop', 'dreamweaver',
		'adobe', 'powerpoint', 'office', 'access', 'word', 
		'excel', 'ipod', 'coldfusion', 'cvs',
		'eclipse', 'autocad', 'skype', '3ds', '3d max', 'apache',
		'outlook'],

	'database\\mysql' :  ['mysql'],
	'database\\oracle' :  ['oracle'],
	'database\\postgresql' :  ['postgresql'],
	'database' :  ['sql', 'database'],
	'photography' :  ['photography'],

	'category\\regexp' :  ['regexp', 'reg exp', 'regular exp'],
	'category\\graphics' :  
		['graphics', '3d', '2d', 'swing', 'render', 
		'lightning'],
	'category\\methodology' :  ['methodology', 'open source', 'fpga'],
	
	'category\\game' : 
		['game', 'xna', 'play', 'xbox', 'nintendo', 
		 'sega', 'playstation', 'play station']	,

	'technologies\\ajax' :  ['ajax'],
	'technologies\\file' :  ['ldap'],
	'technologies\\dot net' :  ['.net', 'dot net'],
	'technologies\\cgi' :  ['cgi'],
	'technologies\\asp' :  ['asp', 'active server pages'],
	'technologies\\cocoa' :  ['cocoa'],

	'languages\\lisp' :  ['lisp'],
	'languages\\ruby' :  ['ruby'],
	'languages\\visual basic' :  ['visual basic', 'vb'],	
	'languages\\c++' :  ['c++', 'cpp'],
	'languages\\perl' :  ['perl'],
	'languages\\python' :  ['python'],
	'languages\\assembly' :  ['assembly', 'asm'],
	'languages\\actionscript' :  ['actionscript', 'action script'],
	'languages\\flash' :  ['flash'],
	'languages\\delphi' :  ['delphi'],
	'languages\\javascript' :  ['javascript', 'java script'],
	'languages\\php' :  ['php'],
	'languages\\java' :  ['java', 'j2ee', 'j2me', 'j2se'],
	'languages\\uml' :  ['uml'],
	'languages\\xml' :  ['xml'],
	'languages\\html' :  ['html'],
	'languages\\c#' :  ['c#', 'c#', 'csharp', 'c sharp'],
	'languages\\css' :  ['css'],
	'languages\\xslt' :  ['xsl', 'xslt'],
	'languages\\c' :  [' c ', ' c.'],
	
	'category\\algorithm' :  ['algorithm', 'algo'],
	'category\\video' :  ['video'],
	'category\\audio' :  ['audio'],
	'category\\shell' :  ['shell', 'bash'],
	'category\\architecture' :  ['architecture', 'software'],

	'company\\microsoft' :  ['microsoft'],
	'company\\blackberry' :  ['blackberry'],
	'company\\nokia' :  ['nokia'],

	'os\\palm' :  ['palm'],
	'os\\windows' :  
		['windows server', 'vista', 'windows'],
	'os\\linux' :  
		['linux', 'unix', 'ubuntu', 'red hat', 'fedora', 'suse', 
		'kubuntu', 'bsd'],
	'os\\solaris' :  ['solaris'],
	'os\\apple' :  ['apple', 'mac ', 'maxos', 'osx'],
		
	'net' :  
		['ebay', 'internet', 'google',
		'web','server', 'network',
		'tcp', 'peer', 'irc', 'blog',
		'broadband', 'wifi', 'wi-fi', 'voip', 'isdn', 'wireless',
		'www', 'dns', 'gsm', 'p2p' ],

	'security' :  
		['security', 'crypto', 'penetration', 'virus', 'firewall',
		'pgp', 'intrusion', 'hacking', 'hacker', 'backup', 'crack',
		'attack', 'reverse eng', 'secure'],
	
	'category\\design' :  
		['design', 'object', 'uml', 'oop', 'patterns'],
	
	'category\\general' :  ['pc']

}

# our physics related categories
physicsCategories = {
	'particle' :  
		['particle', 'atom'],
	'quantum' :
		['quantum', 'qm'],
	'mechanics' :
		['mechanics'],
	'astronomy' :
		['astro', 'cosmo', 'space', 'universe', 'solar', 'holes',
		'planet', 'gravit', 'kepler'],
	'meteorology' :
		['meteorology'],
	'nanotechnology' :
		['nano'],
	'dynamics' :
		['dynamics', 'fluid', 'plasma']
}

# our engineering related categories
engineeringCategories = {
	'adaptive' : ['adaptive'],
	'cybernetics' : ['cyberne'],
	'video' : ['video'],
	'radar' : ['radar'],
	'radio' : ['radio', 'rfid'],
	'wireless' : ['wireles'],
	'microwave' : ['microwave'],
	'optical' : ['optical'],
	'mobile' : ['mobile', 'gsm'],
	'cmos' : ['cmos'],
	'bluetooth' : ['bluetooth'],
	'audio' : ['audio', 'sound'],
	'image' : ['image', 'imagin', 'pattern'],
	'802.11': ['802'],
	'space' : ['space'],
	'cdma' : ['cdma'],
	'dsp' : 
		['dsp', 'digital', 'signal', 'filter', 
		'wavele', 'finger', 'speech', 'neural' ],
	'antenna' : ['antenna'],
	'computer' : ['computer'],
	'electronics' :
		['electronic', 'circuit', 'electric', 'transis' ],
	'network' : ['network', 'broadband', 'fiber', 'position', 'carrier'],
	'finance' : ['financ'],
	'amplifier' : ['amplifier', 'amp'],
	'control' : ['control'],
	'satellite' : ['satellit'],
	'telecommunications' : ['telecom'],
	'design' : ['design']
}	

###
### Function to do the categorization for directory
###
def categorizeDirectory(categories, sourceDirectory, targetDirectory, categoryDivisions, justPrint):
		
	# take list of all filenames
	filenames = os.listdir(sourceDirectory)
	
	filesProcessed = 0
	
	# loop over all entries
	for name in filenames:
		# change to source directory
		os.chdir(sourceDirectory)

		# if directory, recursive call
		if os.path.isdir(name):			
			
			newSourceDirectory = sourceDirectory + "\\" + name
			filesProcessed = filesProcessed + categorizeDirectory(categories, newSourceDirectory, targetDirectory, categoryDivisions, justPrint)
		
		# if file, process
		elif os.path.isfile(name):
		
			chosenCategory = "misc"
			# check if file has some token and return that token
			found = 0
			for category in categories.keys():
				for entry in categories[category]:
					if (name.lower().find(entry) != -1):
						# we found the token, so we have a category
						chosenCategory = category
						found = 1
						break
				if (found):
					break;
			# move the file to the category
			categoryDivisions[chosenCategory].append(name)
			
			fileUtility.makeDir(targetDirectory + "\\" + chosenCategory, justPrint)
			utility.move(name, targetDirectory + "\\" + chosenCategory + "\\", justPrint)
			
			filesProcessed = filesProcessed + 1
	
	return filesProcessed
	
###
### Function to do the categorization for directory
###
def categorize(categories, sourceDirectory, targetDirectory, justPrint):
	
	categoryDivisions = dict()
	for category in categories.keys():
		categoryDivisions[category] = list()
	categoryDivisions['misc'] = list()
	
	filesProcessed = categorizeDirectory(categories, sourceDirectory, targetDirectory, categoryDivisions, justPrint)
	
	print "\n************************************"
	print "categorized ",filesProcessed," files in ",len(categoryDivisions)," categories"
	print "\n************************************"
	
	# print stuff
	if justPrint == 1:
		ordered = sorted(categoryDivisions.keys())
		for category in ordered:
			number = len(categoryDivisions[category])
			if number > 0:
				print "************************************"
				print category, " (",number,")"
				print "************************************"
				for entry in categoryDivisions[category]:
					print entry
				
###
### MAIN PROGRAM
###

# store start time
startTime = utility.getCurrentTime()

# first argument is source directory for files
sourceDirectory = sys.argv[1]

# second argument is target root directory for files
targetDirectory = sys.argv[2]

# just printing?
justPrint = int(sys.argv[3])

# our current directory is where we change back eventually
originalDirectory = os.getcwd()

# do the stuff
categorize(computerCategories, sourceDirectory, targetDirectory, justPrint)
#categorize(physicsCategories, sourceDirectory, targetDirectory, justPrint)
#categorize(engineeringCategories, sourceDirectory, targetDirectory, justPrint)

# store stop time
stopTime = utility.getCurrentTime()

# change back to original directory
os.chdir(originalDirectory)

# print some statistics
utility.printProcessingTime(startTime, stopTime)
