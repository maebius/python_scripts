#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
import sys
import traceback
from ODSReader import ODSReader

from utility.log import log
from database.database import database

LOG_NAME = "odtRead"

def processTable(table, requiredColumns, columnTranslations, valueTranslations):

	result = []

	if len(table) == 0:
		return result

	columnNames = table[0]
	requiredIndices = []

	for r in requiredColumns:
		try:
			index = columnNames.index(r)
			requiredIndices.append(index)
		except ValueError:
			print ("No required column '" + r + "' in table")

	for row in table[1:]:

		isValid = True

		for r in requiredIndices:
			if len(row) <= r or row[r] == None:
				isValid = False
				break

		if isValid:
			entry = dict()
			for i, value in enumerate(row):
				if columnNames[i] != None:

					key = columnNames[i]

					if value != None and key in valueTranslations:
						entry[valueTranslations[key][0]] = valueTranslations[key][1]
					else:
						if key in columnTranslations:
							key = columnTranslations[key]
						entry[key] = value

			if len(entry) > 0:
				#print(entry)
				result.append(entry)

	return result

def connectToDatabase():
	db = database()
	db.setParameters("localhost", 3306, "maebius", "gigeli42quu")
	db.connect("budget")

	return db

def printFromDatabase(db, tableName):
	values = db.getAllFromTable(tableName)
	count = 0
	for row in values:
		concat = " | ".join(str(e) for e in row)
		print ("[" + str(count) + "]: " + concat)

		count += 1


def addToDatabase(db, tableName, contents, autoAddColumns, justPrint):

	for entry in contents:
		for autoKey, autoValue in autoAddColumns.items():
			entry[autoKey] = autoValue
		
		db.addEntries(tableName, entry, True, justPrint)

def formatDate(input):

	# assume input format "dd.mm.yy wk" e.g. "05.12.17 su" 
	splitted = input.split()[0].split(".")
	return splitted[2] + "." + splitted[1] + "." + splitted[0]

def formatDecimal(input):
	return input.replace(",", ".")

def main():
	
	log.init(True, False, "")
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(database.LOG_NAME, log.LVL_PROGRESS)

	db = connectToDatabase()

	filename = "test.ods"

	sheetsToProcess = { '2017 menot' }

	requiredColumns = {'e', 'mita'}
	
	columnTranslations = dict()
	columnTranslations['pvm'] = "transaction_date"
	columnTranslations['e'] = "cost"
	columnTranslations['kategoria'] = "category"
	columnTranslations['turhat'] = "tags"
	columnTranslations['mita'] = "details"

	valueTranslations = dict()
	valueTranslations['turhat'] = [ 'tags', 'extra' ]
	
	autoAddColumns = dict()
	autoAddColumns["currency"] = "eur" 

	doc = ODSReader(filename, clonespannedcolumns=True)

	for sheetName in doc.SHEETS:

		if not sheetName in sheetsToProcess:
			continue

		table = doc.getSheet(sheetName)
		contents = processTable(table, requiredColumns, columnTranslations, valueTranslations)

		previousDate = None

		for e in contents:
			
			e['cost'] = formatDecimal(e['cost'])

			if e['transaction_date'] == None:
				e['transaction_date'] = previousDate
			else:
				e['transaction_date'] = formatDate(e['transaction_date'])
				previousDate = e['transaction_date']

		addToDatabase(db, "costs", contents, autoAddColumns, False)

	#printFromDatabase(db, "costs")


if __name__ == "__main__":
	try:
		main()
		
	except:
		print ("Unexpected exception: ", sys.exc_info()[0])
		print ('-'*60)
		traceback.print_exc(file=sys.stdout)
		print ('-'*60)
