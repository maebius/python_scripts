import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(path, justPrint):

	files = []
	foundMatches = []
	entryDict = dict()

	fileUtility.getFilePathsFromDirectoryRecursive(path, files, 2)

	foundCount = 0
	
	for file in files:
		path, filename = os.path.split(file)
		
		# we assume here that the filename consist of leading title name, and then "-" and then follows the author
		index = filename.rfind(" - ")
		
		if index != -1:
			# ... and the author is in form that the surname is first, then "_" and then first name
			author = filename[index + 3 : -5]
			author_parts = author.split("_")
			if len(author_parts) > 1:
				foundCount += 1
				first_name = author_parts[1].strip()
				sur_name = author_parts[0].strip()
				title = filename[0 : index].strip()
				
				# make a directory for the author
				newDir = os.path.join(path, sur_name + ", " + first_name)
				fileUtility.makeDir(newDir, justPrint)
				# ... and move the file there
				fileUtility.move(file, newDir, justPrint)
				
				#print "\"" + sur_name + "\", \"" + first_name + "\" - \"" + title + "\""
			else:
				print "DID NOT FIND AUTHOR: " + file
		else:
			print "DID NOT MATCH: " + file
	
	print "-------------------------------------------------------------------------"
	print "Found matches: " + str(foundCount)
	print "-------------------------------------------------------------------------"
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "path=", "justPrint"]
	requiredOptions = ["path"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	path = optionsResult["path"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, log.LVL_DETAIL)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	log.add(fileUtility.LOG_NAME, log.LVL_PROGRESS)
	
	execute(path, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
		
	raw_input("Press Enter to Exit ...")