import os
import sys
import shutil
import stat

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(path, endMatch, justPrint):
	
	found = 0
	moved = 0
	
	for root, dirs, files in os.walk(path, topdown=False):
		for dir in dirs:
			fullDir = os.path.join(root, dir)
			
			if fullDir.endswith(endMatch):
				found += 1
				oneDown = os.path.split(fullDir)[0]
				
				print fullDir
				#os.rename(fullDir, fullDir[0 : len(fullDir) - len(endMatch)])
				#continue
				#fileUtility.move(fullDir, oneDown, justPrint)
				
				if False:
					for subRoot, subDirs, subFiles in os.walk(fullDir, topdown=True):
						for subDir in subDirs:
							fullSubDir = os.path.join(subRoot, subDir)
							fileUtility.move(fullSubDir, oneDown, justPrint)
					#	for subFile in subFiles:
					#		fileUtility.move(subFile, oneDown, justPrint)
				
				if not justPrint:
					os.chmod(fullDir, stat.S_IWRITE)
					os.removedirs(fullDir)
				
				
	if not justPrint:
		moved = found

	lines = []
	lines.append("Found: " + str(found))
	lines.append("Moved: " + str(moved))
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "path=", "endMatch=", "justPrint"]
	requiredOptions = [ "path", "endMatch"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	path = optionsResult["path"]
	endMatch = optionsResult["endMatch"]

	if not os.path.exists(path):
		utility.errorExit("Path \"" + path + "\" does not exists!")
	
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(fileUtility.LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, log.LVL_ERROR)
	
	execute(path, endMatch, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)

if __name__ == '__main__':
    main()
