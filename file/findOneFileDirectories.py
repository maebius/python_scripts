import os
import sys

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from log import log
	
###
### MAIN PROGRAM
###

LOG_NAME = "main"

def execute(sourceDirectory, targetDirectory, fileCount, dirCount, mode, justPrint):

	foundFiles = []
	foundDirs = []

	if not os.path.exists(targetDirectory):
		log.log(LOG_NAME, 2, "mkDir: \"" + targetDirectory + "\"")
		fileUtility.makeDir(targetDirectory, justPrint)
	
	# do the pruning
	foundDirectories = fileUtility.goThroughDirectoriesWithFileCount(sourceDirectory, fileCount, dirCount, foundFiles, foundDirs)

	partOfSeriesCount = 0
	singleCount = 0

	shouldMovedFileCount = 0
	movedFileCount = 0
	
	for entry in foundFiles:

		pathAndFile = os.path.split(entry)
		categorizedEntry = categorize.handleFilenameEntry(pathAndFile[1], entry, pathAndFile[0], mode)

		# check if still is just part some serie ...
		if "issue" in categorizedEntry:	
			partOfSeriesCount += 1
		else:
			singleCount += 1
			log.log(LOG_NAME, 1, "single: \"" + entry + "\"")
			shouldMovedFileCount += 1
			if not justPrint:
				movedFileCount += 1
				utility.move(entry, targetDirectory)
	
	deletedPreservedString = ""
	if not justPrint:
		# remove source, if empty
		deletedPreserved = fileUtility.deleteAllEmptyDirectories(sourceDirectory, False, justPrint)
		deletedPreservedString = "Directories deleted: " + str(len(deletedPreserved[0])) +\
			", preserved: " + str(len(deletedPreserved[1]))
			
	log.log(LOG_NAME, 0, "Found 1-file dirs: " + str(foundDirectories))
	log.log(LOG_NAME, 0, "... the file is of series / singles: " + str(partOfSeriesCount) + " / " + str(singleCount))
	log.log(LOG_NAME, 0, "Moved (/should) files: " + str(movedFileCount) + " (/" + str(shouldMovedFileCount)  +")")
	if len(deletedPreservedString) > 0: 
		log.log(LOG_NAME, 0, deletedPreservedString)
			
			
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "target=", "mode=", "files=" ,"dirs=", "justPrint"]
	requiredOptions = [ "source", "target", "files", "dirs", "mode"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	# source directory for files
	sourceDirectory = os.path.normpath(optionsResult["source"])
	
	# target directory for files
	targetDirectory = os.path.normpath(optionsResult["target"])
	
	mode = os.path.normpath(optionsResult["mode"])
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	fileCount = int(optionsResult["files"])
	dirCount = int(optionsResult["dirs"])
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 1)
	log.add(categorize.LOG_NAME, 2)
	
	if not os.path.exists(sourceDirectory):
		log.log(LOG_NAME, 0, "Source path \"" + sourceDirectory + "\" does not exist!")
	else:
		execute(sourceDirectory, targetDirectory, fileCount, dirCount, mode, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
