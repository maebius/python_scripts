import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize

###
### MAIN PROGRAM
###

def changePostFix(path, old, new):
	fileType = stringUtilities.parseFilePostFix(path)
	if fileType.lower() == old:
		newFile = stringUtilities.parseFileTypeAway(file) + "." + new
		print newFile
		utility.move(path, newFile)

rules = dict()
#rules["[1080]"] = ""
#rules["[720]"] = ""
#rules["Harry Potter "] = ""
#rules[".idx"] = " [Eng].idx"
#rules[".sub"] = " [Eng].sub"
rules["- "] = " - "
#rules[" - See Guide[TC]"] = ""

		
def simpleStringMatch(filename):
	newFilename = filename
	for key,value in rules.items():		
		if newFilename.find(key) > -1:
			newFilename = newFilename.replace(key, rules[key])
			fileType = stringUtilities.parseFilePostFix(newFilename)
			newFilename = stringUtilities.parseFileTypeAway(newFilename).strip() + "." + fileType
			newFilename = newFilename.replace("  ", " ")
						
			while newFilename[0] == ' ' or\
				newFilename[0] == '-':
				newFilename = newFilename[1 :]
	return newFilename

def seriesSubtitleMatch(filename, fileCount):
	
	if not fileUtility.isSubtitleFile(filename):
		return ""
		
	episodeNumber = stringUtilities.parseEpisodeMaster(filename, fileCount)
	if episodeNumber[0] > -1:
		asString = str(episodeNumber[0])
		fill = ""
		if len(asString) == 1:
			fill = "0"
		newFilename = "Episode " + fill + asString
		return newFilename + "." + stringUtilities.parseFilePostFix(filename)
		
	return ""
		
def comicMatch(path, filename, fileCount):
	newFilename = filename
	if False:
		comp = ". "
		index = newFilename.find(comp) 	
		if index > -1:
			newFilename = newFilename[index + len(comp) :]
	if True:
		comp = "v2 "
		index = newFilename.find(comp) 	
		if index > -1:
			newFilename = newFilename[0 : index] + newFilename[index + len(comp) :]
	
	return newFilename

def seriesVideoMatch(path, filename, fileCount):
	
	fullPath = os.path.join(path, filename)	
	result = categorize.handleAsEntry(filename, fullPath, "", "tv", "", False, fileCount)
			
	if result != None and "episode" in result:
		asString = str(result["episode"])
		fill = ""
		if len(asString) == 1:
			fill = "0"
		#if "article" in result and len(result["article"]) > 0:
		#	newFilename = result["article"] + " "
		#newFilename += result["title"]
		newFilename = "Episode " + fill + asString
		#newFilename = fill + asString + " - " + newFilename
		
		#source = "Episode " + fill + asString + ".sub"
		#subtitleSource = os.path.join(path, source)
		#subtitleTarget = os.path.join(path, newFilename + os.path.splitext(subtitleSource)[1])

		#if result["episode"] > 0 and result["episode"] < 30:
		#	print "\"" + subtitleSource + "\"\t->\t\"" + subtitleTarget + "\""
		#	shutil.move(subtitleSource, subtitleTarget)
		
		return newFilename + "." + result["file_type"]
	
	return ""

def recursive(sourceDirectory, justPrint):
	files = []

	# do the pruning
	fileUtility.getFilePathsFromDirectoryRecursive(sourceDirectory, files)
	fileCount = len(files)
	for file in files:
		path, filename = os.path.split(file)
		
		#newFilename = seriesMatch(path, filename, fileCount)
		#newFilename = seriesSubtitleMatch(filename, fileCount)
		#newFilename = seriesVideoMatch(path, filename, fileCount)
		#newFilename = comicMatch(path, filename, fileCount)
		#newFilename = filename[10 : ]
		newFilename = ""
		previous = ""
		for letter in filename:
			if letter == letter.upper() and letter.isalpha():
				if previous != "":
					newFilename += " "
			newFilename += letter
			previous = letter
		
		#newFilename = simpleStringMatch(filename)
		#newFilename = filename[6 : ]
		#newFilename = filename[0 : 2] + " " 
		
		if len(newFilename) > 0 and newFilename != filename:
			
			if justPrint:
				print "path: \"" + path + "\" - \"" + filename + "\"\t ->\t\"" + newFilename + "\""
			else:
				filename = os.path.join(path, filename)
				newFilename = os.path.join(path, newFilename)
				if os.path.exists(newFilename):
					print "ERROR! already exists: \"" + newFilename + "\""
				else:
					shutil.move(filename, newFilename)
				
		#changePostFix(file, "zip", "cbz")

def oneDirectory(sourceDirectory, justPrint):
	entries = os.listdir(sourceDirectory)
	
	for entry in entries:
		newEntry = entry[6 : ]
		articleAndEntry = stringUtilities.parseArticle(newEntry)
		if articleAndEntry[0] != None:
			newEntry = articleAndEntry[1] + ", " + articleAndEntry[0]
		
		entry = os.path.join(sourceDirectory, entry)
		newEntry = os.path.join(sourceDirectory, newEntry)
		
		if justPrint:
			print "\"" + entry + "\" -> \"" + newEntry + "\""
		else:
			shutil.move(entry, newEntry)
			
### MAIN PROGRAM

# store start time
startTime = utility.getCurrentTime()

# source directory for files
sourceDirectory = sys.argv[1]

# do we just print the entries?
justPrint = int(sys.argv[2])

recursive(sourceDirectory, justPrint)
#oneDirectory(sourceDirectory, justPrint)

# store stop time
stopTime = utility.getCurrentTime()

# print some statistics
utility.printProcessingTime(startTime, stopTime)