import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def renameHelper(root, name, what, to, justPrint):
	newName = name
	if name.find(what) > 0:
		fullPath = os.path.join(root, name)
		newName = name.replace(what, to)
		newFullPath = os.path.join(root, newName)
		if os.path.exists(newFullPath):
			log.log(LOG_NAME, log.LVL_ERROR, "Can't rename \"" + fullPath + "\" to \"" + newFullPath + "\", already exists, handle manually!")
			return name
		if not justPrint:
			os.rename(fullPath, newFullPath)
		name = newName
	
	return newName

def execute(path, justPrint):

	mergeList = []
	movedFiles = []
	movedDirectories = []
	
	for root, dirs, files in os.walk(path, topdown=False):
		
		nameMap = dict()
		for name in dirs:
			name = renameHelper(root, name, " (The)", ", The", justPrint)
			name = renameHelper(root, name, " (A)", ", A", justPrint)
			name = renameHelper(root, name, " (An)", ", An", justPrint)
			name = renameHelper(root, name, " (Les)", ", Les", justPrint)
			name = renameHelper(root, name, " (Los)", ", Los", justPrint)
			name = renameHelper(root, name, " (La)", ", La", justPrint)
		
			fullPath = os.path.join(root, name)
			
			length = len(name)
			parIndex = name.find("(")
			commaIndex = name.find(",")
			parsedName = name
			if parIndex > -1:
				parsedName = name[ : parIndex - 1]
			afterDot = ""
			if commaIndex > -1:
				parsedName = name[ : commaIndex]
				afterDot = name[ commaIndex + 1: ]
		
			if parsedName in nameMap:
				
				alreadyPath = nameMap[parsedName][0]
				
				# check iif we need to preserve both as inviduals,
				# -> this might be the case if we have two directories that begin with "Surname," following different first names
				if len(afterDot) > 0 and nameMap[parsedName][2] > -1:
					alreadyAfterDot = alreadyPath[ nameMap[parsedName][2] + 1 :]
					if afterDot.find(alreadyAfterDot) == -1 and alreadyAfterDot.find(afterDot) == -1:
						nameMap[name]= [ fullPath, length, commaIndex]
						nameMap[alreadyPath]= [ alreadyPath, nameMap[parsedName][1], nameMap[parsedName][2] ]
						nameMap.pop(parsedName)
						
						continue
				
				source = ""
				target = ""
				
				if length > nameMap[parsedName][1]:
					source = alreadyPath
					target = fullPath
					nameMap[parsedName] = [ fullPath, length, commaIndex ]
				else:
					source = fullPath
					target = nameMap[parsedName][0]
				
				if len(source) > 0 and len(target) > 0:
				
					for moveRoot, moveDirs, moveFiles in os.walk(source, topdown=False):
						for moveFile in moveFiles:
							try:
								moveFile = unicode(moveFile)
							except:
								log.log(LOG_NAME, log.LVL_ERROR, "Malformed name [\"" + moveRoot + "\"]: \"" + moveFile + "\", handle manually!")
							
							movedFiles.append(moveFile)
							fileUtility.move(os.path.join(moveRoot, moveFile) , target, justPrint, True)
						for moveDir in moveDirs:
							moveDir = unicode(moveDir)
							movedDirectories.append(moveDir)
							fileUtility.move(os.path.join(moveRoot, moveDir) , target, justPrint)
					
					mergeList.append( [source, target] )
					log.log(LOG_NAME, log.LVL_PROGRESS, "\"" + source + "\"  -> \"" +  target + "\"")
				else:
					log.log(LOG_NAME, log.LVL_ERROR, "Invalid dir names! - src: \"" + source + "\" , trg: \"" +  target + "\"")

			else:
				nameMap[parsedName] = [ fullPath, length, commaIndex ]
	
	deletedDirs, preservedDirs = fileUtility.deleteAllEmptyDirectories(path, False, justPrint)
	
	print "-------------------------------------------------------------------------"
	print "Merged directories: " + str(len(mergeList))
	print "Moved files: " + str(len(movedFiles))
	print "Moved directories: " + str(len(movedDirectories))
	print "Deleted dirs: " + str(len(deletedDirs)) + " (preserved: " + str(len(preservedDirs)) + ")"
	print "-------------------------------------------------------------------------"

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "path=", "justPrint"]
	requiredOptions = [ "path"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	path = optionsResult["path"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(fileUtility.LOG_NAME, log.LVL_ERROR)
	log.add(categorize.LOG_NAME, log.LVL_ERROR)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	
	execute(path, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':

	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
		
	raw_input("Press Enter to Exit ...")
