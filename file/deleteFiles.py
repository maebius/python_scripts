import os
import sys

from utility import utility

###
### MAIN PROGRAM
###

# store start time
startTime = utility.getCurrentTime()

# first argument is source directory for files
sourceDirectory = sys.argv[1]

# just printing?
justPrint = int(sys.argv[2])

# our current directory is where we change back eventually
originalDirectory = os.getcwd()

# do the actual stuff

deleteEntries = [  'svn'  ]
	
deletedEntries = utility.deleteEntriesWithString(sourceDirectory, deleteEntries, justPrint)

# store stop time
stopTime = utility.getCurrentTime()

# change back to original directory
os.chdir(originalDirectory)

# print some statistics
utility.printProcessingTime(startTime, stopTime)
