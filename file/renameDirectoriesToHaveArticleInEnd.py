import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(path, justPrint):

	files = []
	foundMatches = []
	entryDict = dict()

	separator = " - "
	# do the pruning
	files = os.listdir(path)

	hasArticleCount = 0
	noArticleCount = 0
	
	for entry in files:
		dir = os.path.join(path, entry)
		
		if os.path.isdir(dir):
		
			article, without = stringUtilities.parseArticle(entry)
			if article != None:
				newDirectory = os.path.join(path, without + ", " + article)
				
				log.log(LOG_NAME, log.LVL_PROGRESS, "from: \"" + dir + "\" to: \"" + newDirectory + "\"")
			
				fileUtility.move(dir, newDirectory, justPrint)
				hasArticleCount += 1
			
			else:
				noArticleCount += 1
	
	print "-------------------------------------------------------------------------"
	print "Found article matches: " + str(hasArticleCount) + " (... and no: " + str(noArticleCount )+ ")"
	print "-------------------------------------------------------------------------"
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "justPrint"]
	requiredOptions = ["source"]
	argsResult = []
	optionsResult = dict()
	
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	path = optionsResult["source"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, log.LVL_DETAIL)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	log.add(fileUtility.LOG_NAME, log.LVL_ERROR)
	
	execute(path, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
		
	raw_input("Press Enter to Exit ...")