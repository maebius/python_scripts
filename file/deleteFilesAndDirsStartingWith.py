import os
import sys
import traceback
import locale

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log

###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(path, match, justPrint):

	print "-------------------------------------------------------------------------"
	print "Delete all entries starting with:"
	print "\"" + match + "\" (len: " + str(len(match)) + ")"

	numbers = fileUtility.deleteEntriesWithStart(path, [match], justPrint)
	
	print "-------------------------------------------------------------------------"
	print "Deleted files (/should): " + str(numbers[2]) + " (" + str(numbers[0]) + ")"
	print "Deleted dirs (/should): " + str(numbers[3]) + " (" + str(numbers[1]) + ")"
	print "-------------------------------------------------------------------------"
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "path=", "match=", "verbose=", "justPrint"]
	requiredOptions = ["path", "match"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	path = optionsResult["path"]
	match = optionsResult["match"]
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	verbose = 0
	if "verbose" in optionsResult:
		verbose = int(optionsResult["verbose"])
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	log.add(categorize.LOG_NAME, log.LVL_DETAIL)
	log.add(database.LOG_NAME, log.LVL_ERROR)
	log.add(fileUtility.LOG_NAME, verbose)
	
	execute(path, match, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
	locale.setlocale(locale.LC_ALL, '')

	try:
		main()
	except:
		print "Unexpected exception: ", sys.exc_info()[0]
		print '-'*60
		traceback.print_exc(file=sys.stdout)
		print '-'*60
		
	#raw_input("Press Enter to Exit ...")