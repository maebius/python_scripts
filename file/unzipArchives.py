import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(path, deleteSource, justPrint):
	
	found, executed, deleted = fileUtility.unzipAllFilesRecursively(path, deleteSource, justPrint)

	lines = []
	lines.append("Found: " + str(found))
	lines.append("Unzipped: " + str(executed))
	lines.append("Deleted: " + str(deleted))
	log.printOutput(lines, LOG_NAME, log.LVL_PROGRESS)
	
def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "path=", "deleteSource", "justPrint"]
	requiredOptions = [ "path"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	path = optionsResult["path"]

	if not os.path.exists(path):
		utility.errorExit("Path \"" + path + "\" does not exists!")
	
	deleteSource = "deleteSource" in optionsResult
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, log.LVL_PROGRESS)
	#log.add(fileUtility.LOG_NAME, log.LVL_PROGRESS)
	log.add(fileUtility.LOG_NAME, log.LVL_DETAIL)
	log.add(categorize.LOG_NAME, log.LVL_ERROR)
	
	execute(path, deleteSource, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)

if __name__ == '__main__':
    main()
