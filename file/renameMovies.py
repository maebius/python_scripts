import os
import sys
import shutil

from utility import utility
from fileUtility import fileUtility
from stringutilities import stringUtilities
from categorize import categorize
from database import database
from log import log
		
###
### MAIN PROGRAM
###
### @param paramStringOne [required] some parameter
### @param paramStringTwo [optional] some parameter
### @param justPrint  Do we just print the "should-do" operations, without doing them (for debug)
###

LOG_NAME = "main"

def execute(source, justPrint):
	
	# initialize database?
	#db = database()
	#db.setParameters("localhost", 3306, "root", "fyget42mulle")
	#db.connect("kiilto")

	#allEntries = db.getAllFromTable("table_name")
	#fullItemCount = len(allEntries)
	#entryColumnNameToIndex = db.getColumnFields("table_name")

	renamedCount = 0
	shouldRenamedCount = 0
	
	videoCount = 0
	subtitleCount = 0
	otherCount = 0
	otherFiles = []
	
	manipulationCount = 0
	
	filePaths = []
	fileUtility.getFilePathsFromDirectoryRecursive(source, filePaths)
	
	log.log(LOG_NAME, 0, "Total files: " + str(len(filePaths)))
	
	for path in filePaths:
		filepath, filename = os.path.split(path)
	
		manipulated = False
		year = None
		strippedName = filename
		
		if filename[0 : 4].isdigit() and filename[4] == " " and filename[5] == "-":
			year = int(filename[0 : 4])
			strippedName = filename[7 : ]
		else:
			# try to get pub year, give priority to parenthesis
			yearResult = stringUtilities.parsePublishYear(filename, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
			year = yearResult[0]
			strippedName = yearResult[1]
		
		if not year == None:
			manipulated = True
		
		fileType = stringUtilities.parseFilePostFix(strippedName)
		# strip file type away from entry name
		strippedName = stringUtilities.parseFileTypeAway(strippedName)
		
		# try to get article
		article = ""
		articleResult = stringUtilities.parseArticle(strippedName)		
		# if we have article, take it away from entry name
		if len(articleResult[0]) > 0:			
			manipulated = True
			article = articleResult[0]
			strippedName = articleResult[1]
		
		langString = ""
		hidefString = ""
		for inParenthesis in stringUtilities.parseParenthesis(strippedName):
			if len(inParenthesis) > 0 and inParenthesis[len(inParenthesis)-1] == "p":
				manipulated = True
				hidefString = inParenthesis[0 : len(inParenthesis) - 1]
			if inParenthesis.lower() == "fin" or inParenthesis.lower() == "eng":
				langString = inParenthesis
				
		strippedName = stringUtilities.parseAwayParenthesis(strippedName)
		
		curLen = len(strippedName)
		endString = strippedName[curLen - 5 : ]
		if endString == "- Fin" or endString == "- Eng":
			manipulated = True
			langString = strippedName[curLen - 4 : ].strip()
			strippedName = strippedName[0 : curLen - 5].strip()
		
		finalEntry = strippedName.strip()
		if len(article) > 0:
			manipulated = True
			finalEntry += ", " + article
		if not year == None:
			finalEntry += " (" + str(year) + ")"
		#if len(hidefString) > 0:
		#	finalEntry += " [" + hidefString + "p]"
		if len(langString) > 0 and not langString == "Fin":
			finalEntry += " [" + langString + "]"
			
		finalEntry += "." + fileType
		
		finalEntry = stringUtilities.parseMultiSpacesAway(finalEntry)
		 
		isRightFIleType = True

		if fileUtility.isVideoFile(finalEntry):
			videoCount += 1
		elif fileUtility.isSubtitleFile(finalEntry):
			subtitleCount += 1
		else:
			isRightFIleType = False
			otherCount += 1
			otherFiles.append(path)
		
		# still check ... if really changed
		if filename == finalEntry:
			manipulated = False
		
		if isRightFIleType and manipulated:
			manipulationCount += 1
			finalPath = os.path.join(filepath, finalEntry)
			log.log(LOG_NAME, 1, "from \"" + path + "\" to \"" + finalPath+ "\"")
			shouldRenamedCount += 1
			if not justPrint:
				renamedCount += 1
				shutil.move(path, finalPath)
		
	if otherCount > 0:
		print "\n"
		print "Not recognize these files:"
		print "\n"
		for otherFile in otherFiles:
			print otherFile
		
	print "-------------------------------------------------------------------------"
	print "Video file count: " + str(videoCount)
	print "Subtitle file count: " + str(subtitleCount)
	print "Other file count: " + str(otherCount)
	print "Renamed files (/should): " + str(renamedCount) + " (/" + str(shouldRenamedCount) + ")"
	print "-------------------------------------------------------------------------"

def main():
	# parse input args
	acceptedArgs = ""
	acceptedOptions = [ "source=", "justPrint"]
	requiredOptions = [ "source"]
	argsResult = []
	optionsResult = dict()
	utility.parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult)

	source = optionsResult["source"]
	optionWithoutValue = "optionWithoutValue" in optionsResult

	if not os.path.exists(source):
		print "ERROR! Source \"" + source + "\" does not exists!"
		exit(0)
	
	# just print?
	justPrint = "justPrint" in optionsResult
	
	# print some basic stuff
	utility.printProgramInfo(optionsResult)
	# store start time
	startTime = utility.getCurrentTime()

	# our current directory is where we change back eventually
	originalDirectory = os.getcwd()
	
	filenameToPrint = ""
	
	log.init(True, False, filenameToPrint)
	log.add(LOG_NAME, 1)
	log.add(categorize.LOG_NAME, 2)
	log.add(database.LOG_NAME, 0)
	
	execute(source, justPrint)

	log.finalize()

	# change back to original directory
	os.chdir(originalDirectory)

	# store stop time
	stopTime = utility.getCurrentTime()

	# print some statistics
	utility.printProcessingTime(startTime, stopTime)	

if __name__ == '__main__':
    main()
