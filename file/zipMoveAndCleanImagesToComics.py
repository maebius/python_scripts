import os
import sys

from utility import utility

###
### MAIN PROGRAM
###

# store start time
startTime = utility.getCurrentTime()

# source directory for files
sourceDirectory = sys.argv[1]

# do we just print the entries?
justPrint = int(sys.argv[2])

fileTypes = []
fileTypes.append("jpg")
fileTypes.append("gif")
fileTypes.append("png")
fileTypes.append("tga")
fileTypes.append("tiff")
fileTypes.append("bmp")

# do the pruning
utility.zipAllSpecifiedFilesRecursively(sourceDirectory, fileTypes, "cbz", 1, justPrint)

# store stop time
stopTime = utility.getCurrentTime()

# print some statistics
utility.printProcessingTime(startTime, stopTime)