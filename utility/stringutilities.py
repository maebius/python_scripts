##  @package stringUtilities
#   Collection of static string manipulation utility functions.

import os
import sys
import re

##  Class containing the utility functions.
class stringUtilities:

	LOG_NAME = __name__

	romanNumerals =\
	{
		'I' : 1, 'i' : 1,
		'V' : 5, 'v' : 5,
		'X' : 10, 'x' : 10,
		'L' : 50, 'l' : 50,
		'C' : 100, 'c' : 100,
		'D' : 500, 'd' : 500, 
		'M' : 1000, 'm' : 1000
	}
	
	##  Try to parse entry as a roman numeral. Don't accept mixed cases. Returns as 10-digit.
	#   @param entry
	def parseRomanNumeral(entry):
		
		# remove whitespaces from start/end
		entry = entry.strip()
		if len(entry) == 0:
			return -1
			
		# first chek if this is a valid roman numeral
		lowerCount = 0
		upperCount = 0
		for c in entry:
			if not c in stringUtilities.romanNumerals:
				return -1
			if c.islower():
				lowerCount += 1
			elif c.isupper():
				upperCount += 1
			if lowerCount > 0 and upperCount > 0:
				return -1
		
		intValue = 0
		for i in range(len(entry)):
			if i < len(entry) - 1\
				and stringUtilities.romanNumerals[entry[i]] < stringUtilities.romanNumerals[entry[i+1]]:
				intValue +=  stringUtilities.romanNumerals[entry[i+1]] - stringUtilities.romanNumerals[entry[i]]
			elif i > 0 and stringUtilities.romanNumerals[entry[i-1]] < stringUtilities.romanNumerals[entry[i]]:
				pass
			else:
				intValue += stringUtilities.romanNumerals[entry[i]]
		
		return intValue
	parseRomanNumeral = staticmethod(parseRomanNumeral)

	##  Checks if this is a number with postfix
	#   @param entry
	#   @param postfix
	#   @param ignoreCase
	def isNumberWithPostfix(entry, postfix, ignoreCase):
		if ignoreCase:
			postfix = postfix.lower()
			entry = entry.lower()
		
		if entry.endswith(postfix):
			stripped = entry[0 : len(entry) - len(postfix)]
			if len(stripped) > 0:
				for c in stripped:
					if not c.isdigit():
						return False
				return True
		return False		
	isNumberWithPostfix = staticmethod(isNumberWithPostfix)
	
	##  Find strings enclosed by given string pairs.
	#   @param entry
	#   @param search
	def parseAwayAlphabetsAfter(entry, search):
		
		index = entry.find(search)
		if index == -1:
			return entry
		
		index += len(search)
		result = entry[0 : index]
		digits = 0
		alphas = 0
		startIndex = index
		while index < len(entry):
			if entry[index].isdigit():
				digits += 1
			elif entry[index].isalpha():
				alphas += 1
			else:
				if alphas == 0:
					result += entry[startIndex : index]
				startIndex = index
				digits = 0
				alphas = 0

			index += 1
		return result
		
	parseAwayAlphabetsAfter = staticmethod(parseAwayAlphabetsAfter)

	##  Parse away number string, if it only includes, addition to numbers, given token.
	#   @param entry
	#   @param postFix
	def parseAwayNumbersWith(entry, separator, token):
		result = []
		words = entry.split(separator)
		for word in words:
			if word.find(token) > -1:
				test = word.replace(token, "")
				if not test.isdigit():
					result.append(word)
			else:
				result.append(word)
		
		return separator.join(result)
	parseAwayNumbersWith = staticmethod(parseAwayNumbersWith)	
	
	##  Parse away number, if it starts the entry and ends with given post fix.
	#   @param entry
	#   @param postFix
	def parseAwayLeadingNumberWithPostfix(entry, postFix):
		
		if len(entry) == 0 or not entry[0].isdigit():
			return entry
		index = 1
		while index < len(entry) and entry[index].isdigit():
			index += 1
		
		endIndex = index + len(postFix)
		if endIndex >= len(entry):
			return entry
		
		if entry[index : endIndex] == postFix:
			return entry[endIndex : len(entry)]
			
		return entry		
	parseAwayLeadingNumberWithPostfix = staticmethod(parseAwayLeadingNumberWithPostfix)
	
	##  Find strings enclosed by given string pairs.
	#   @param entry
	#   @param startRange
	#   @param endRange
	def parseAwayLonelyNumbers(entry, startRange, endRange):
		splitted_name = entry.split(" ")
		new_name = ""
		
		for i in range(len(splitted_name)):
			formatted = splitted_name[i].strip()
			isDigit = formatted.isdigit()
			asDigit = 0
			if isDigit:
				try:
					asDigit = int(formatted)
				except:
					isDigit = False
					from log import log
					log.log(stringUtilities.LOG_NAME, log.LVL_ERROR, "int(\"" + formatted + "\") FAILED when isdigit()==True [\"" + entry + "\"]")
			
			if not isDigit\
				or asDigit < startRange\
				or asDigit > endRange:
				new_name = new_name + formatted + " "
		return new_name.strip()
	parseAwayLonelyNumbers = staticmethod(parseAwayLonelyNumbers)
	
	##  Replaces the given replacer limiters with something if the enclosing string is of given length.
	#   @param entry
	#   @param limiter
	#   @param replacement
	#   @param length
	def replaceLimiterIfEnclosedLength(entry, limiter, replacement, length):
		
		result = ""
		index = 0
		prevIndex = -1
		limiterLength = len(limiter)
		count = 0
		while index != -1:
			index = entry.find(limiter, index)

			# if no limiter found at all, just break away instantly
			if index == -1 and prevIndex == -1 and count == 0:
				result = entry
				break
			# ... was the previous limiter last one? concatenate the trailing stuff and break away
			elif index == -1 and prevIndex != -1:
				startIndex = prevIndex
				if count > 1:
					startIndex += limiterLength					
				result += entry[startIndex : len(entry)]
				break
			# ... or was this the first limiter? concatenate the stuff before that
			elif index != -1 and prevIndex == -1:
				result += entry[0 : index]
			# ... the meat of the function: check the stuff between the limiters
			elif index != -1 and prevIndex != -1:
				# ... was the stuff inside of the right lenght? if so, take the limiters away
				if index - prevIndex - limiterLength == length:
					startIndex = prevIndex + limiterLength
					endIndex = startIndex + length
					contents = entry[startIndex : endIndex]					
					result += replacement + contents + replacement
				# ... otherwise don't touch anything, just put as it was to result
				else:
					result += entry[prevIndex : index]
			
			# initialize for next round
			prevIndex = index
			index += limiterLength
			if index != -1:
				count += 1
		
		return result
	replaceLimiterIfEnclosedLength = staticmethod(replaceLimiterIfEnclosedLength)
	
	##  Find strings enclosed by given string pairs.
	#   @param entry
	#   @param startLimiter
	#   @param endLimiter
	def findStringsEnclosedBy(entry, startLimiter, endLimiter):
		result = list()
		
		# find all "startString" entries
		startIndex = 0
		while startIndex != -1:
			startIndex = entry.find(startLimiter, startIndex)
			if startIndex != -1:
				# try to find closing limiter
				startIndex = startIndex + len(startLimiter)
				endIndex = entry.find(endLimiter, startIndex)
				if endIndex != -1:
					# found!
					result.append(entry[startIndex : endIndex])
					startIndex = endIndex
		return result		
	findStringsEnclosedBy = staticmethod(findStringsEnclosedBy)
		
	##  Parse away all string that are inside limiters, if they're at maximum of given length, including the limiters.
	#   @param entry
	#   @param startLimiter
	#   @param endLimiter
	#   @param lengthLimit
	def parseAwayEnclosedBy(entry, startLimiter, endLimiter, lengthLimit):

		# take all tokens that  are of form, for example "- -" or "--" or "-    -" away
		insiders = stringUtilities.findStringsEnclosedBy(entry, startLimiter, endLimiter)
		for insider in insiders:
			if len(insider.strip()) <= lengthLimit:
				# parse limiter in full form ..
				insider = startLimiter + insider + endLimiter
				entry = entry.replace(insider, "")
		return entry
	parseAwayEnclosedBy = staticmethod(parseAwayEnclosedBy)
	
	##  Parse away as long as the given string is leading.
	#   @param entry
	#   @param leading
	def parseAwayLeading(entry, leading):
		while entry.startswith(leading):
			entry = entry[len(leading) : ]
		return entry
	parseAwayLeading = staticmethod(parseAwayLeading)
	
	##  Parse away as long as the given string is trailing.
	#   @param entry
	#   @param trailing
	def parseAwayTrailing(entry, trailing):
		while entry.endswith(trailing):
			entry = entry[0 : len(entry) - len(trailing)]
		return entry
	parseAwayTrailing = staticmethod(parseAwayTrailing)
	
	##  Parse publish year out of string
	#   @param entry
	def parsePublishYear(entry, minAcceptedYear, maxAcceptedYear):
		# try to get pub year, give priority to parenthesis
		pubYear = -1
		for yearString in stringUtilities.parseParenthesis(entry):
			yearResult = stringUtilities.parseYear(yearString, 0, minAcceptedYear, maxAcceptedYear)
			if yearResult[0] > -1:
				pubYear = yearResult[0]
				#print entry + " : replace " + yearString + " with " + yearResult[1]
				entry = entry.replace(yearString, yearResult[1])
		# if we did not found from parenthesis, check the whole name
		if pubYear == -1:
			yearResult = stringUtilities.parseYear(entry, 0, minAcceptedYear, maxAcceptedYear)
			if yearResult[0] > -1:
				pubYear = yearResult[0]
				entry = yearResult[1]
		if pubYear > 0:
			return pubYear, entry
		return None, entry
	parsePublishYear = staticmethod(parsePublishYear)
	
	def parseReversedArticle(entry):
		lowerCase = entry.lower()
		if lowerCase.endswith(", a"):
			return "A", entry[0 : len(entry) - 3]
		if lowerCase.endswith(", an"):
			return "An", entry[0 : len(entry) - 4]
		if lowerCase.endswith(", the"):
			return "The", entry[0 : len(entry) - 5]
		return "", entry
	parseReversedArticle = staticmethod(parseReversedArticle)
		
	##  Try to get article out of entry.
	#   @param entry
	def parseArticle(entry):
		lowerCase = entry.lower()
		
		if lowerCase.startswith("a "):
			return "A", entry[2 : ]
		if lowerCase.startswith("an "):
			return "An", entry[3 : ]
		if lowerCase.startswith("the "):
			return "The", entry[4 : ]
		
		l = len(lowerCase)
		
		if lowerCase.endswith(", a"):
			return "A", entry[0 : l - 3]
		if lowerCase.endswith(", an"):
			return "An", entry[0 : l - 4]
		if lowerCase.endswith(", the"):
			return "The", entry[0 : l - 5]
		
		# no article
		return None, entry
	parseArticle = staticmethod(parseArticle)

	##  Take all multispaces from string and convert them to single spaces.
	#   @param filename
	def parseMultiSpacesAway(entry):		
		while entry.find("  ") > 0:
			entry = entry.replace("  ", " ")
		return entry
	parseMultiSpacesAway = staticmethod(parseMultiSpacesAway)

	##  Try to get the type (=postfix) of a filepath.
	#   @param filepath
	def parseFilePostFix(filepath):
		
		baseAndExt = os.path.splitext(filepath) 
		if len(baseAndExt[1]) == 0:
			return None
		return baseAndExt[1][1 : ].lower()
	parseFilePostFix = staticmethod(parseFilePostFix)

	##  Get the postfix of a filepath away and return the result.
	#   @param filepath
	def parseFileTypeAway(filepath):
		
		baseAndExt = os.path.splitext(filepath) 
		return baseAndExt[0]
	parseFileTypeAway = staticmethod(parseFileTypeAway)

	##  Does path has file extension?
	#   @param filepath
	def hasPostFix(filepath):
		
		baseAndExt = os.path.splitext(filepath) 
		return len(baseAndExt[1]) > 0
	hasPostFix = staticmethod(hasPostFix)
	
	g_acceptedResolutionsWxH =\
	[
		# Digital television
		[ 352, 240], # video cd ntsc
		[ 352, 288], # video cd pal
		[ 480, 272], # umd
		[ 352, 480], # china video disc pal
		[ 352, 576], # china video disc pal
		[ 480, 480], # svcd ntsc
		[ 480, 576 ], # svcd pal
		[ 640, 480], # sdtv, edtv
		[ 704, 480], # sdtv, edtv
		[ 852, 480], # sdtv, edtv
		[ 720, 480], # dvd ntsc
		[ 720, 576], # dvd pal
		[ 1280,  720], # hdtv, blueray
		[ 1920, 1080 ], # hdtv, blueray
		# Analog television
		[ 520, 576], # pal, secam
		[ 440, 486], # ntsc
		[ 320, 480], # vhs ntsc
		[ 310, 576], # vhs pal/secam
		[ 530, 480], # s-vhs ntsc
		[ 520, 576], # s-vhs pal/secam
		# Computer
		[ 120, 160], #
		[ 160, 120], # 
		[ 240, 160], # 
		[ 320, 240], # 
		[ 400, 240], # 
		[ 480, 320], # 
		[ 640, 480], # 
		[ 720, 364], # 
		[ 800, 600], # 
		[ 1024, 768], # 
		[ 1024, 800], # 
		[ 1280, 720], # 
		[ 1280, 800], # 
		[ 1280, 960], # 
		[ 1280, 1024], # 
		[ 1366, 768], # 
		[ 1440, 900], # 
		[ 1600, 900], # 
		[ 1600, 1200], # 
		[ 1920, 1080], # 
		[ 1920, 1200], # 
		[ 2048, 1152] # 
	]
	
	##  Try to find a resolution definition from a string.
	#   @param entry
	def parseResolution(entry):		
		
		pair = stringUtilities.parseNumberSXEY(entry, "", "Xx", 3, 4, 3, 4)		
		if pair[0] > -1 and pair[1] > -1:
			for accepted in stringUtilities.g_acceptedResolutionsWxH:
				if pair[0] == accepted[0] and pair[1] == accepted[1]:
					return str(accepted[0]) + "x" + str(accepted[1]), pair[2]
		
		for accepted in stringUtilities.g_acceptedResolutionsWxH:
			height= str(accepted[1])
			index = entry.find(height)
			if  index > -1:
				# check if previous letter was alpha, if yes, this is not acceptable
				if index > 0:
					prevLetter = entry[index - 1]
					if prevLetter.isdigit():
						continue
				# also check if next alpha, in which case not acceptable
				nextIndex = index + len(height)
				if nextIndex < len(entry):
					nextLetter = entry[nextIndex]
					if nextLetter.isdigit():
						continue
				# this is ok, return it
				return height, entry.replace(height, "")
			
		return "", entry
		
	parseResolution = staticmethod(parseResolution)

	##  Try to find an issue number from a string.
	#   @param entry
	#   @param maxAcceptedIssueNumber
	def parseEpisodeMaster(entry, maxAcceptedNumber):		
		episodeSeasonEpisode = stringUtilities.parseNumberSXEY(entry, "Ss", "Ee", 1, 2, 1, 2, [" ", "."])		
		if episodeSeasonEpisode[0] > -1 and episodeSeasonEpisode[1] <= maxAcceptedNumber:
			return episodeSeasonEpisode[1], episodeSeasonEpisode[2], episodeSeasonEpisode[0]

		episodeSeasonEpisode = stringUtilities.parseNumberSXEY(entry, "", "Xx", 1, 2, 1, 3, [" ", "."])		
		if episodeSeasonEpisode[0] > -1 and episodeSeasonEpisode[1] <= maxAcceptedNumber:
			return episodeSeasonEpisode[1], episodeSeasonEpisode[2], episodeSeasonEpisode[0]

		result = stringUtilities.parseIssueNumberWithPrefix(entry, "Ep ", maxAcceptedNumber)
		if result[0] > -1:
			return result
		result = stringUtilities.parseIssueNumberWithPrefix(entry, "Ep_", maxAcceptedNumber)
		if result[0] > -1:
			return result
		result = stringUtilities.parseIssueNumberWithPrefix(entry, "Part ", maxAcceptedNumber)
		if result[0] > -1:
			return result
		result = stringUtilities.parseIssueNumberWithPrefix(entry, "Part.", maxAcceptedNumber)
		if result[0] > -1:
			return result
		result = stringUtilities.parseIssueNumberWithPrefix(entry, " E", maxAcceptedNumber)
		if result[0] > -1:
			return result
		result = stringUtilities.parseIssueNumberWithPrefix(entry, ".E", maxAcceptedNumber)
		if result[0] > -1:
			return result
		
		if entry[0].lower() == "e":
			result = stringUtilities.parseIssueNumberWithPrefix(entry, "E.", maxAcceptedNumber)
			if result[0] > -1:
				return result
			result = stringUtilities.parseIssueNumberWithPrefix(entry, "E", maxAcceptedNumber)
			if result[0] > -1:
				return result
		
		# the rest of the stuff we do to entries without parenthesis
		entry = stringUtilities.parseAwayParenthesis(entry)

		# NOTE! This script can't handle if there is "no season" series with episodes going over 100
		# -> californication 101 is now handled correctly
		# -> legend of galactic heroes 101 -> should be episode 101, but will be treated e01s01
		
		# ... first try here 4-digit episodes, like SouthPark 1101 -> season 11, episode 1
		episodeSeasonEpisode = stringUtilities.parseNumberSXEY(entry, "", "", 2, 2, 2, 2, None)		
		if episodeSeasonEpisode[0] > -1 and episodeSeasonEpisode[1] <= maxAcceptedNumber:
			return episodeSeasonEpisode[1], episodeSeasonEpisode[2]
		# ,,. if no luck, then  like californication 101 -> season 1 episode 1
		# -> this only if max episode number is less than 100, otherwise this will mess that constraint
		if maxAcceptedNumber < 100:
			episodeSeasonEpisode = stringUtilities.parseNumberSXEY(entry, "", "", 1, 1, 2, 3, None)		
			if episodeSeasonEpisode[0] > -1 and episodeSeasonEpisode[1] <= maxAcceptedNumber:
				return episodeSeasonEpisode[1], episodeSeasonEpisode[2]
		
		result = stringUtilities.parseIssueNumberWithoutParenthesis(entry, maxAcceptedNumber)
		if result[0] > -1:
			return result
		
		return stringUtilities.parseIssueNumber(entry, maxAcceptedNumber)
		
	parseEpisodeMaster = staticmethod(parseEpisodeMaster)

	##  Try to find number of form "x of y" from string.
	#   @param string
	def parseNumberSXEY(string, seasonPrefix, episodePrefix, minDigitsX, maxDigitsX, minDigitsY, maxDigitsY, betweenStrings):
		regExp = ""
		seasonPreLen = len(seasonPrefix)
		if seasonPreLen > 0:
			regExp += "[" + seasonPrefix + "]"			
		regExp += "(?P<season>[0-9]{" + str(minDigitsX) + "," + str(maxDigitsX) + "})"
		
		if betweenStrings != None and len(betweenStrings) > 0:
			regExp += "['" + "', '".join(betweenStrings) + "']*" 
				
		episodePreLen = len(episodePrefix)
		if episodePreLen > 0:
			regExp += "[" + episodePrefix + "]"					
		regExp += "(?P<episode>[0-9]{" + str(minDigitsY) + "," + str(maxDigitsY) + "})"
		
		season = -1	
		episode = -1

		#print regExp
		
		m = re.search(regExp, string)
		if m and m.group:
			entry = m.group(0)
			seasonString = m.group("season")
			season = int(seasonString)
			episodeString = m.group("episode")
			episode = int(episodeString)
			string = string.replace(entry,"")
			#print "\"" + seasonString + "-" + episodeString + "\" from: \"" + string + "\""
			#print "entry \"" + string + "\" - seasonString \"" + seasonString + "\", episodeString \"" + episodeString + "\""
			
		return season, episode, string		
	parseNumberSXEY = staticmethod(parseNumberSXEY)
	
	##  Try to find number of form "x of y" from string.
	#   @param entry
	def parseNumberXofY(entry):
		findStart = 0
		# we satisfy from starting index 1 only, as if there's a leading "if" it obviously won't do
		index = 1

		# we loop the whole string, all "if"s, until we find the first one satisfying our conditions
		while index > 0:
			index = entry.lower().find("of", findStart)
			if index > 0:
				findStart = index + 1

				xStartIndex = -1
				xEndIndex = -1
				
				numberOfParenthesis = 0
				numberOfCurly = 0
				numberOfSquare = 0

				xIndex = index - 1
				while xIndex > 0\
					and (entry[xIndex] == " " or entry[xIndex] == "_"\
						or entry[xIndex] == "(" or entry[xIndex] == "[" or entry[xIndex] == "{"):

					if entry[xIndex] == "(":
						numberOfParenthesis += 1
					elif entry[xIndex] == "[":
						numberOfSquare += 1
					elif entry[xIndex] == "{":
						numberOfCurly += 1

					xIndex -= 1
				
				if entry[xIndex].isdigit():
					xEndIndex = xIndex + 1
					while xIndex > -1 and entry[xIndex].isdigit():
						xIndex -= 1
					xStartIndex = xIndex + 1
				
				if xStartIndex > -1 and xEndIndex > -1 and xEndIndex >= xStartIndex:
					xNumberString = entry[xStartIndex : xEndIndex]

					yStartIndex = -1
					yEndIndex = -1
							
					yIndex = index + 2
					while yIndex < len(entry) and (entry[yIndex] == " " or entry[yIndex] == "_"):
						yIndex += 1
					if yIndex < len(entry) and entry[yIndex].isdigit():
						yStartIndex = yIndex
						while yIndex < len(entry) and entry[yIndex].isdigit():
							yIndex += 1
						yEndIndex = yIndex
										
					if yStartIndex > -1 and yEndIndex > -1 and yEndIndex >= yStartIndex:
						yNumberString = entry[yStartIndex : yEndIndex]
						
						stripEnd = len(entry)
						if numberOfParenthesis > 0 or numberOfCurly > 0 or numberOfSquare > 0:
							
							# if found some parenthesis, we need to go pas them to strip away right amount of letters							
							stripEnd = yEndIndex
							while stripEnd < len(entry) and\
								(numberOfParenthesis > 0 or numberOfCurly > 0 or numberOfSquare > 0):
								if entry[stripEnd] == ")":
									numberOfParenthesis -= 1
								elif entry[stripEnd] == "]":
									numberOfSquare -= 1
								elif entry[stripEnd] == "}":
									numberOfCurly -= 1
								stripEnd += 1
						newEntry = entry[0 : xStartIndex] + entry[stripEnd : len(entry)]
						return int(xNumberString), int(yNumberString), newEntry.replace("#","")
		# we did not find anything ...
		return -1, -1, entry
	parseNumberXofY = staticmethod(parseNumberXofY)
	
	##  Try to find any number from string.
	#   @param entry
	#   @param reversed
	#   @param acceptPrePostAlphabet
	def parseAnyNumber(entry, reversed, acceptPrePostAlphabet):

		#print "parseAnyNumber entry: \"" + entry + "\", reversed: " + str(reversed) + ", acceptPrePostAlphabet: " + str(acceptPrePostAlphabet)

		# from string start to end
		limit = len(entry)
		increment = 1
		index = 0
		# ... or from end to beginning?
		if reversed:
			limit = -1
			increment = -1
			index = len(entry) - 1
		
		leadingWhiteSpaceCount = 0
		trailingWhiteSpaceCount = 0
		
		while index != limit:
		
			startIndex = -1
			endIndex = -1
			
			while index != limit and not entry[index].isdigit():
				if entry[index] == " ":
					leadingWhiteSpaceCount += 1
				index += increment

			if reversed:
				# in python, the end index in syntax string[x : y]  must point to the index we don't include anymore
				endIndex = index + 1
			else:
				startIndex = index
				
			while index != limit and entry[index].isdigit():
				if entry[index] == " ":
					trailingWhiteSpaceCount += 1
				index += increment

			# go back one, while loop got us over
			index -= increment
				
			if reversed:
				startIndex = index
			else:
				# in python, the end index in syntax string[x : y]  must point to the index we don't include anymore
				endIndex = index + 1

			# did we find suitable?
			# 1. we must have start and end indices, so that start comes before end
			# 2. if acceptPrePostAlphabet==False, we dont accept entries where there would be leading or trailing alphabet
			if startIndex > -1 and endIndex > -1 and endIndex > startIndex\
				and (acceptPrePostAlphabet\
					or (leadingWhiteSpaceCount == startIndex)\
					or ((startIndex == 0 or not entry[startIndex-1].isalpha())\
						and (endIndex == len(entry) or not entry[endIndex].isalpha()))):
			
				numberString = entry[startIndex : endIndex]
				
				strippedEntry = ""
				if startIndex > 0:			
					strippedEntry += entry[0 : startIndex]
				if endIndex < len(entry):
					strippedEntry += entry[endIndex : len(entry)]
				return int(numberString), strippedEntry
			
			else:
				index += increment
			
		return -1, entry
	parseAnyNumber = staticmethod(parseAnyNumber)
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param maxAcceptedIssueNumber
	def isSurroundedBy(entry, startIndex, endIndex, pre, post):
		index = startIndex
		while index > -1 and not entry[index : index + len(pre)] == pre:
			index -= len(pre)
		if index < 0:
			return False
		index = endIndex
		while index < len(entry) and not entry[index : index + len(post)] == post:
			index += len(post)
		if index >= len(entry):
			return False
		return True
		
	isSurroundedBy = staticmethod(isSurroundedBy)
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param prefix
	#   @param addToFront 
	#   @param replacement
	def convertNumberWithPrefixTo(entry, prefix, addToFront, replacement):
		result = ""
		prevIndex = 0
		prevLength = 0
		index = entry.find(prefix)
		if index == -1:
			return entry

		startIndex = 0
		endIndex = 0
			
		while index > -1:
			result += entry[prevIndex : index + 1]
			
			startIndex = index + len(prefix)
			endIndex = startIndex + 1
			while endIndex < len(entry) and entry[endIndex].isdigit():
				endIndex += 1
			if endIndex > startIndex:
				number = entry[startIndex : endIndex]
				if addToFront:
					result +=  number + replacement
				else:
					result +=  replacement + number
			
			prevIndex = index
			index = entry.find(prefix, index + 1)
		
		result += entry[endIndex : len(entry)]
		return result		
	convertNumberWithPrefixTo = staticmethod(convertNumberWithPrefixTo)
	
	##  Tries to find number from entry in that fashion that it is first splitted with separator,
	#   and if those we find a pure number, that is the one we want.
	#   @param entry
	#   @param separator
	#   @param maxAcceptedIssueNumber
	def parseIssueNumberClean(entry, separator, maxAcceptedIssueNumber):
		
		stopIndex = len(entry)
		index = 0
		while index > -1:
			index = entry.rfind(separator, 0, stopIndex)
			if index > -1:
				numberStart = index + len(separator)
			
				ok = True
				for letter in entry[numberStart : stopIndex]:
					if not letter.isdigit():
						ok = False
						break		
				if ok and stopIndex > numberStart:
					number = int(entry[numberStart : stopIndex])
					if number < maxAcceptedIssueNumber:
						return number, entry[0 : numberStart] + entry[stopIndex : len(entry)]
					else:
						print ("stringutilities.parseIssueNumberClean - NOTE: \"" + entry + "\", max reached: " + str(number) + ", max accepted: " + str(maxAcceptedIssueNumber))
						
				stopIndex = index
				
		return -1, entry
	parseIssueNumberClean = staticmethod(parseIssueNumberClean)
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param index
	#   @param postfix 
	#   @param maxAcceptedIssueNumber
	def parseIssueNumberFromWithPostfix(entry, index, postfix, maxAcceptedIssueNumber):
		if index < 0 or index >= len(entry) or not entry[index].isdigit():
			return -1, entry
		end = index + 1
		while end < len(entry) and entry[end].isdigit():
			end += 1
		if end + len(postfix) > len(entry):
			return -1, entry
		
		asInt = int(entry[index : end])
		toCompare = entry[end : end + len(postfix)]
		
		if toCompare == postfix:
			if asInt < maxAcceptedIssueNumber:
				return asInt, entry[0 : index] + entry[end + len(postfix) : len(entry)]
			else:
				print ("stringutilities.parseIssueNumberFromWithPostfix - NOTE: \"" + entry + "\", max reached: " + str(asInt) + ", max accepted: " + str(maxAcceptedIssueNumber))

		return -1, entry
	parseIssueNumberFromWithPostfix = staticmethod(parseIssueNumberFromWithPostfix)
			
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param prefix 
	#   @param maxAcceptedIssueNumber
	def parseIssueNumberWithPrefix(entry, prefix, maxAcceptedIssueNumber):
		
		index = 0
		while index != -1:
			index = entry.find(prefix, index)
			if index != -1:
				# we require that next character is a number
				nextIndex = index + len(prefix)
				if nextIndex >= len(entry) or not entry[nextIndex].isdigit():
					index = nextIndex
					continue
				
				result = stringUtilities.parseAnyNumber(entry[nextIndex: ], False,False)
				if result[0] > -1:
					if result[0] <= maxAcceptedIssueNumber:
						#print "index of prefix: " + str(index)
						#print "parsed issue: " + str(result[0])
						#print "entry parsed away of issue: \"" + result[1] + "\""
						preParsed = entry[0 : max( [0, index - 1] ) ]
						#postParsed = result[1].replace(prefix, "")
						postParsed = result[1]
						#print "preParsed part: \"" + preParsed + "\""
						#print "postParsed part: \"" + postParsed + "\""
						#sys.exit(0)
						return result[0],  preParsed + postParsed
					else:
						print ("stringutilities.parseIssueNumberWithPrefix - NOTE: \"" + entry + "\", max reached: " + str(result[0]) + ", max accepted: " + str(maxAcceptedIssueNumber))

				# above conditions did not meet, move to next
				index = nextIndex
				
		return -1, entry
	parseIssueNumberWithPrefix = staticmethod(parseIssueNumberWithPrefix)
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param maxAcceptedIssueNumber
	def parseIssueNumberWithoutParenthesis(entry, maxAcceptedIssueNumber):
					
		withoutParenthesis = stringUtilities.parseAwayParenthesis(entry)
		anyNumber = stringUtilities.parseAnyNumber(withoutParenthesis, True, False)
		
		if anyNumber[0] <= maxAcceptedIssueNumber:
			
			nmbAsStr = str(anyNumber[0])
			nmbLength = len(nmbAsStr)

			stopIndex = len(entry)
			while stopIndex > -1:
				numberIndex = entry.rfind(nmbAsStr, 0, stopIndex)
				length = nmbLength

				# we need to check if the number actually contained leading zeros ...
				while numberIndex > 0 and entry[numberIndex - 1].isdigit()\
					and int(entry[numberIndex - 1]) == 0:					
					numberIndex -= 1
					length += 1
				
				# now, we know that the found entry was found from place where it was not inside parenthesis
				# -> we still need to strip the original entry (that has parenthesis) to such form that we get the issue
				# number out 
				# -> this means finding the issue-string from the original entry and taking it out
				if not stringUtilities.isSurroundedBy(entry, numberIndex, numberIndex + length, "(", ")")\
					and not stringUtilities.isSurroundedBy(entry, numberIndex, numberIndex + length, "[", "]")\
					and not stringUtilities.isSurroundedBy(entry, numberIndex, numberIndex + length, "{", "}"):

					newEntry = entry[0 : numberIndex] + entry[numberIndex + length : len(entry)]			
					return anyNumber[0], newEntry
				
				stopIndex = numberIndex
				
		print ("stringutilities.parseIssueNumberWithoutParenthesis - NOTE: \"" + entry + "\", max reached: " + str(anyNumber[0]) + ", max accepted: " + str(maxAcceptedIssueNumber))

		return -1, entry
	parseIssueNumberWithoutParenthesis = staticmethod(parseIssueNumberWithoutParenthesis)
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param maxAcceptedIssueNumber
	def parseIssueNumberMaster(entry, maxAcceptedIssueNumber):		
		# NOTE! Here the number in "of 5" is random, it gets pruned away and it's not used in anything, so it does not matter
		entry = stringUtilities.convertNumberWithPrefixTo(entry, " Pt.", True, " of 5")
		
		issueNumber = []
		# try to take issue number, and strip it away		
		issueNumberXofY = stringUtilities.parseNumberXofY(entry)
		
		if issueNumberXofY[0] > -1:
			# we still give priority to numbers that found without parenthesis ... if there is something else also in the name
			# -> usually XofY formed entries that include other numbers are that kind that XofY is a subnumbering "issue 53 part 1 of 4" for example
			withoutPar = stringUtilities.parseIssueNumberWithoutParenthesis(issueNumberXofY[2], maxAcceptedIssueNumber)
			if withoutPar[0] > -1 and not issueNumberXofY[2].strip().isdigit():
				stripped = stringUtilities.parseAwayTrailing(withoutPar[1].strip() , "-")
				issueNumber = withoutPar[0], stripped\
					+ " - " + str(issueNumberXofY[0]) + " of " + str(issueNumberXofY[1])
			else:
				issueNumber = issueNumberXofY[0], issueNumberXofY[2]
		else:		
			issueNumber = stringUtilities.parseIssueNumberFromWithPostfix(entry, 0, "-", maxAcceptedIssueNumber)
			if issueNumber[0] == -1:
				issueNumber = stringUtilities.parseIssueNumberWithPrefix(entry, "#", maxAcceptedIssueNumber)
			if issueNumber[0] == -1:
				issueNumber = stringUtilities.parseIssueNumberClean(entry, " ", maxAcceptedIssueNumber)
			if issueNumber[0] == -1:
				issueNumber = stringUtilities.parseIssueNumberWithoutParenthesis(entry, maxAcceptedIssueNumber)
			if issueNumber[0] == -1:
				issueNumber = stringUtilities.parseIssueNumber(entry, maxAcceptedIssueNumber)
		return issueNumber
	parseIssueNumberMaster = staticmethod(parseIssueNumberMaster)
	
	##  Try to find an issue number from a string.
	#   @param entry
	#   @param maxAcceptedIssueNumber
	def parseIssueNumber(entry, maxAcceptedIssueNumber):
		
		anyNumber = stringUtilities.parseAnyNumber(entry, True, False)
		if anyNumber[0] <= maxAcceptedIssueNumber:
			return anyNumber
	
		print ("stringutilities.parseIssueNumber - NOTE: \"" + entry + "\", max reached: " + str(anyNumber[0]) + ", max accepted: " + str(maxAcceptedIssueNumber))
			
		# no acceptable result
		return -1, entry
	parseIssueNumber = staticmethod(parseIssueNumber)
	
	##  Try to find N-digit string from entry
	#   @param entry
	#   @param digitsN
	#   @param separator
	def parseDigitStringN(entry, digitsN, separator):
		result = []
		if len(separator) > 0:
			words = entry.split(separator)
			for word in words:     
				if word.isdigit() and  len(word) == digitsN:
					result.append(word)
		else:
			count = 0
			startIndex = 0
			for i in range(len(entry)):
				if entry[i].isdigit():
					count += 1
					if count == digitsN:
						result.append(entry[startIndex : i + 1])
						startIndex = i
						count = 0
				else:
					startIndex = i
					count = 0
							
		return result
	parseDigitStringN = staticmethod(parseDigitStringN)
			
	##  Try to find a year from a string.
	#   @param entry
	#   @param parseTwoDigit
	def parseYear(entry, parseTwoDigit, minYear, maxYear):
		# early exit
		if entry.isalpha():
			return -1, entry
		
		# do we have four number year format (18/19/20xx)?
		
		# try to find 19, 20
		index = entry.find("19")
		if index == -1\
			or index + 3 >= len(entry)\
			or not entry[index+2].isdigit()\
			or not entry[index+3].isdigit():
			index = entry.find("20")
		# did we find it?
		if index != -1:
			# check the two following numbers
			if len(entry) >= index + 4 \
			and entry[index+2].isdigit() \
			and entry[index+3].isdigit():
				# yes! we have a proper 19/20xx year number!
				year = int(entry[index : index + 4])
				if year >= minYear and year <= maxYear:
					parsedString = ""
					if index > 0:
						parsedString += entry[0 : index]
					if index + 4 < len(entry):
						parsedString += entry[index + 4 : len(entry)]
					return year, parsedString 
	
		# try to parse 6-digit strings that validate to a date format: " YYMMDD"
		sixDigitResult = stringUtilities.parseDigitStringN(entry, 6, "")
		for sixDigit in sixDigitResult:
			yy = int(sixDigit[1 : 3])
			mm = int(sixDigit[3: 5])
			dd = int(sixDigit[5: 7])
			if mm < 13 and dd < 32:
				year = 0
				if yy > 10:
					year = 1900 + yy
				else:
					year = 2000 + yy
				# in here, we do not replace the string just to empty as it tells us nice time info in the title
				# -> probably the entry is a "strip" so its good to have the timestamp in the title				
				return year, entry.replace(sixDigit, "")
	
		if parseTwoDigit == 1:
			# no 4-digit format, so do we have maybe a two number (xx) year  format? 
			for i in range(len(entry)):
				if entry[i].isdigit() \
				and i + 1 < len(entry) \
				and entry[i+1].isdigit():
					# we have a two-digit number here
					parsedString = ""
					if i > 0:
						parsedString += entry[0 : i]
					if i + 2 < len(entry):
						parsedString += entry[i + 1 : len(entry)]

					return int(entry[i : i + 2]), parsedString
			
		# no proper year in this string
		return -1, entry
	# make method static
	parseYear = staticmethod(parseYear)		

	##  Divide string to parts enclosed by parenthesis.
	#   @param entry
	def parseParenthesis(entry):
		result = list()
		# find all "(", and try to get matching ")"
		first = stringUtilities.findStringsEnclosedBy(entry, "(", ")")
		for i in first:
			result.append(i)
		
		# find all "[", and try to get matching "]"
		second = stringUtilities.findStringsEnclosedBy(entry, "[", "]")
		for i in second:
			result.append(i)
		
		# find all "{", and try to get matching "}"
		third = stringUtilities.findStringsEnclosedBy(entry, "{", "}")
		for i in third:
			result.append(i)
		return result			
	parseParenthesis = staticmethod(parseParenthesis)

	##  Parse parenthesis that have nothing inside away
	##  including the parenthesis themselves.
	#   @param entry
	def parseAwayEmptyParenthesis(entry):

		entry = stringUtilities.parseAwayEnclosedBy(entry, "(", ")", 0)
		entry = stringUtilities.parseAwayEnclosedBy(entry, "[", "]", 0)
		entry = stringUtilities.parseAwayEnclosedBy(entry, "{", "}", 0)
	
		return entry
	parseAwayEmptyParenthesis = staticmethod(parseAwayEmptyParenthesis)

	##  Parse everything away that is inside parenthesis,
	##  including the parenthesis themselves.
	#   @param entry
	def parseAwayParenthesis(entry):

		entry = stringUtilities.parseAwayEnclosedBy(entry, "(", ")", 10000)
		entry = stringUtilities.parseAwayEnclosedBy(entry, "[", "]", 10000)
		entry = stringUtilities.parseAwayEnclosedBy(entry, "{", "}", 10000)
	
		entry = entry.replace("  ", " ")
		return entry.strip()
	parseAwayParenthesis = staticmethod(parseAwayParenthesis)

	##  Parse everything away from entry before "before" if there is no "condition" first
	#   @param entry
	def parseAwayBeforeIfBefore(entry, before, condition):
		beforeIndex = entry.find(before)
		conditionIndex = entry.find(condition)		
		if beforeIndex > -1 and beforeIndex < conditionIndex:
			entry = entry[beforeIndex + 1 : len(entry)]
	parseAwayBeforeIfBefore = staticmethod(parseAwayBeforeIfBefore)
	
	##  Parse everything away from entry after "after" if there is no "needsToHave" after that
	#   @param entry
	#   @param after
	#   @param needsToHave
	def parseAwayAfterIfHasNo(entry, after, needsToHave):
		
		index = entry.rfind(after)
		conditionIndex = entry.rfind(needsToHave)		
		
		while index > -1 and conditionIndex < index:
			entry = entry[0 : index]
			index = entry.rfind(after)
			conditionIndex = entry.rfind(needsToHave)
		return entry
	parseAwayAfterIfHasNo = staticmethod(parseAwayAfterIfHasNo)
	
	##  Parse everything away that are "inside" non-closing parenthesis
	#   @param entry
	def parseAwayNonClosingParenthesis(entry):		
		entry = stringUtilities.parseAwayAfterIfHasNo(entry, "(", ")")
		entry = stringUtilities.parseAwayAfterIfHasNo(entry, "[", "]")
		entry = stringUtilities.parseAwayAfterIfHasNo(entry, "{", "}")
			
		return entry
	parseAwayNonClosingParenthesis = staticmethod(parseAwayNonClosingParenthesis)
	
	##  Make all string lower case except capitalize the first found alphabet.
	##  including the parenthesis themselves.
	#   @param entry
	def capitalizeFirstAlphabet(entry):

		formatted = entry.lower()
		index = 0
		while index < len(formatted) and not formatted[index].isalpha():
			index += 1
		formatted = formatted[0 : index] + formatted[index : len(formatted)].capitalize()
	
		return formatted
	capitalizeFirstAlphabet = staticmethod(capitalizeFirstAlphabet)

	
	##  Make every word of string start with caps, except roman numerals.
	#   @param entry
	def makeStartingLettersUppercase(entry, minimumLength):

		titleWords = entry.split(" ")
		newTitleWords = []
		for word in titleWords:
			# check if we have a roman number
			asRoman = stringUtilities.parseRomanNumeral(word)
			# we require to be greater than 1 because "I" can be a pronomin
			if asRoman > 1:
				newTitleWords.append(word.upper())
			else:
				if len(word) >= minimumLength:
					# make sure starts with a capital letter
					word = stringUtilities.capitalizeFirstAlphabet(word)
				newTitleWords.append(word)
		newTitle = " ".join(newTitleWords)
		return newTitle
	makeStartingLettersUppercase = staticmethod(makeStartingLettersUppercase)

	
	regularExpressionEscapes = [ "[", "]", "?", "*", ".", "^", "+", "$" ]
	
	def escapeRegExpReserved(entry):
		# ensure all reg-exp reserved characters are escaped ...
		regExpFixed = entry
		for regExp in stringUtilities.regularExpressionEscapes:
			regExpFixed = regExpFixed.replace(regExp, "\\" + regExp)
		return regExpFixed
	escapeRegExpReserved = staticmethod(escapeRegExpReserved)
	
	##  Replace from string every found item with something, ignoring or preserving case.
	#   @param entry
	#   @param what
	#   @param ignoreCase
	#   @param to
	def replaceWith(entry, what, ignoreCase, to):
		result = entry
		for word in what:
			if ignoreCase:
				fixed = stringUtilities.escapeRegExpReserved(word)
				result = re.sub("(?i)" + fixed, to, result)				
			else:				
				result = result.replace(word, to)
		return result
	replaceWith = staticmethod(replaceWith)
	
	## REALLY SIMPLE conversion from "hour and minute" format to plain minutes ...
	def parseToMinutes(entry):
		result = 0
		
		items = entry.split(" ")
		for item in items:
			if item[len(item)-1] == "h":
				result += 60 * int(item[ : len(item) - 1])				
			
			minIndex = item.find("mn")
			if  minIndex > -1:			
				result += int(item[ : minIndex])
	
		return result
	parseToMinutes = staticmethod(parseToMinutes)
	
	def getDifferencesAsString(first, second):
		
		differences = []
		firstLimit = len(first)
		secondLimit = len(second)
		shortLimit = min(firstLimit, secondLimit)

		indicatorLine = " "
		
		detail = False
		
		for index in range(shortLimit):
			if first[index] != second[index]:
				differences.append("[" + str(index) + "]: \"" + first[index] + "\" vs \"" + second[index] + "\"")
				indicatorLine += "^"
			else:
				indicatorLine += " "
		result = "\"" + first + "\"\n\"" + second + "\"\n" + indicatorLine
		
		if detail and firstLimit != secondLimit:
			longer = first
			printString = "1st"
			if secondLimit > firstLimit:
				longer = second
				printString = "2nd"
			differences.append(printString + " longer from [" + str(shortLimit) + "]: " + longer[shortLimit + 1 :])
			result += "\nDifferences: " + str(len(differences))
			if len(differences) > 0:
				for dif in differences:
					result += "\n\t" + dif
		
		return result
	getDifferencesAsString = staticmethod(getDifferencesAsString)
	
	
	def parseDriveLetter(fullPath):
		drive, path = os.path.splitdrive(os.path.abspath(fullPath))
		path = stringUtilities.parseAwayLeading(path, "\\")
		path = stringUtilities.parseAwayLeading(path, "/")
		
		return drive, path
	parseDriveLetter = staticmethod(parseDriveLetter)
	
	