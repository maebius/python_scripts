##  @package utility
#    Collection of logging functions.

import os
import sys
import getopt
import re
import datetime
import time
import shutil
import string
import inspect

##  Static class containing the logging functions.
class log(object):

	import utility

	LVL_ERROR = 0
	LVL_PROGRESS = 1
	LVL_DETAIL = 2
	LVL_DEBUG = 3

	m_levelNames =\
	{
		LVL_ERROR : "ERROR",
		LVL_PROGRESS : "progress",
		LVL_DETAIL : "-detail-",
		LVL_DEBUG : "* debug *"
	}
	
	## Adds module to be logged
	m_registeredModules = []
	m_modulePrintLevel = dict()
	def add(moduleId, printedLevel):
		if not moduleId in log.m_registeredModules:
			log.m_registeredModules.append(moduleId)
		log.m_modulePrintLevel[moduleId] = printedLevel
	# make method static
	add = staticmethod(add)	
	
	# this is for use of progress/debug printing, we first print in string, then later make to stdout / file
	m_wholeResultString = ""
	m_progress = False
	m_signature = False
	m_filename = ""
	## Logs a string to wanted destination.
	def log(moduleId, level, string):
		# is this module registered?
		if not moduleId in log.m_registeredModules:
			return
	
		# only do if configured to the right level
		if level <= log.m_modulePrintLevel[moduleId]:
			levelString = str(level)
			if level in log.m_levelNames:
				levelString = log.m_levelNames[level]
				
			logged = "[" + moduleId + "][" + levelString + "] - "
			
			# append file/method frame
			if log.m_signature:
				logged += utility.getFrameSignature(2) + " - "
			
			logged += str(string)
			# instantly/straight to stdout? if not, cache.
			if log.m_progress: 
				print (logged)
			else:
				log.m_wholeResultString += logged + "\n"
				if level == log.LVL_ERROR or level == log.LVL_PROGRESS:
					print (logged)
	# make method static
	log = staticmethod(log)	
	
	## Initializes our logging stuff.
	def init(enabled, signature, filename):
		log.m_progress = enabled
		log.m_signature = signature
		log.m_filename = filename
	# make method static
	init = staticmethod(init)	
			
	## Finalizes our logging calls, by either printing stuff to stdout or file, if there's anything to print.
	def finalize():
		if len(log.m_wholeResultString) > 0:
			if len(log.m_filename) > 0:
				f = open(log.m_filename, 'w')
				if f != 0:
					f.write(log.m_wholeResultString)
					f.close()
				print ("Wrote program logging info to: \"" + log.m_filename + "\"")
			else:
				print (log.m_wholeResultString)
	# make method static
	finalize = staticmethod(finalize)	

	## Format the output a program should print a bit.
	def printOutput(lines, logName, level):
	
		# is this module registered?
		if len(lines) == 0 or not logName in log.m_registeredModules:
			return
		# only do if configured to the right level
		if level > log.m_modulePrintLevel[logName]:
			return
		
		limiter = log.utility.getMultipleOfStringOfLongest(lines, "-")
		log.log(logName, level,  limiter)
		for line in lines:
			log.log(logName, level,  line)
		log.log(logName, level,  limiter)
	# make method static
	printOutput = staticmethod(printOutput)

	