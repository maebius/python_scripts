##  @package fileUtility
#   Collection of static utilities for file handling.  

import os
import sys
import re
import datetime
import time
import shutil
import string
import xml.dom.minidom
import glob
import threading
#import thread
import stat

#from threading import Thread

from stringUtilities import stringUtilities
import utility
from log import log

g_threadBreak = True
g_multiplierMB = 1 / (1024.0 * 1024.0)

def fileCopyLogger(fileToCheck, totalSizeBytes):
	global g_threadBreak
	
	log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS , "Started moving to \"" + fileToCheck + "\" at " + utility.getSimpleTime())
	startTime = utility.getCurrentTime()
	totalSizeMB = totalSizeBytes * g_multiplierMB
	sizeString = " / %0.2f MB" % (totalSizeMB)
	totalSizeBytesInverted = 1.0 / totalSizeBytes
	
	sleepAmountSeconds = 1.0
	if totalSizeMB < 300:
		sleepAmountSeconds = 0.5
	elif totalSizeMB > 2000:
		sleepAmountSeconds = 7.0
	elif totalSizeMB > 1000:
		sleepAmountSeconds = 5.0
	
	while not g_threadBreak:
		if os.path.exists(fileToCheck):
			doneSizeBytes = os.path.getsize(fileToCheck)
			doneSizeMB = doneSizeBytes * g_multiplierMB
			percentage = doneSizeBytes * totalSizeBytesInverted
			sinceStartSeconds = utility.getSecondsSince(startTime)
			
			leftMB = totalSizeMB - doneSizeMB
			avgSpeedMBPerSecond = doneSizeMB / sinceStartSeconds
			approxLeftToGoSeconds = leftMB / avgSpeedMBPerSecond
			
			log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS,\
				"\t[" + str(sinceStartSeconds) + " - " + str(approxLeftToGoSeconds) + " s]" +\
				(" - avg. %0.2f MB/s" % (avgSpeedMBPerSecond)) + (" (%0.2f" % doneSizeMB) + sizeString + (" - %0.2f \%" % percentage) + ")")
		
		# sleep some
		time.sleep(sleepAmountSeconds)

##  Class containing all the functions.
class fileUtility:

	LOG_NAME = __name__
	
	##  Go through all files, and make categories out of them if more than one similar filename is found
	#    Also move all the files to the corresponding directories, or to "misc" directory if no similar files found
	#   @param directory
	#   @param targetDirectory
	#   @param justPrint
	filenameCategories = dict()
	def makeCategoriesOutOfFiles(directory, targetDirectory, justPrint):
		
		processedEntries = 0
		
		# take all files in directory
		filenames = os.listdir(directory)			

		# go through every entry
		for entry in filenames:
			path = os.path.join(directory, entry)
			
			if os.path.isdir(path):						
				# recursive call
				processedEntries = \
				processedEntries + utility.makeCategoriesOutOfFiles(path, \
					targetDirectory, justPrint)
		
			elif os.path.isfile(path):
				# form a category out of the filename				
				# . take only filename
				afterDir = os.path.basename(path)
				# . take away postfix
				afterPostfix = stringUtilities.parseFileTypeAway(afterDir)
				# . strip all inside parenthesis away (including parenthesis)				
				afterParenthesis = stringUtilities.parseAwayParenthesis(afterPostfix)
				# . strip all numbers (that are not part of word) away, if that does not leave string empty
				afterNumbers = stringUtilities.parseAwayLonelyNumbers(afterParenthesis, 0, len(afterParenthesis))
				
				final = afterNumbers
				
				if len(final) == 0:
					final = afterParenthesis

				# create directory if it does not exist
				directoryName = os.path.join(targetDirectory, final)
					
				# check if category already exists, create if not
				if not final in fileUtility.filenameCategories:
					# create category
					fileUtility.filenameCategories[final] = 0
					
					fileUtility.makeDir(directoryName, justPrint)
				
				if not justPrint:
					# move file
					utility.move(path, directoryName)
					processedEntries = processedEntries + 1
				
				# increment count (not actually needed for anything, though)
				fileUtility.filenameCategories[final] = fileUtility.filenameCategories[final] + 1

				log.log(fileUtility.LOG_NAME, 2, afterDir + "->" + final + " (" + fileUtility.filenameCategories[final] + ")")
				
		return processedEntries
	# make method static
	makeCategoriesOutOfFiles = staticmethod(makeCategoriesOutOfFiles)	

	##  Delete all same sized files (expect the first occurence)
	#   away from a directory (and its subdirectories)
	#   @param directory
	#   @param sameDirectory
	#   @param justPrint
	staticSizeMap = dict()
	def deleteSameSizedFiles(directory, sameDirectory, justPrint):
		
		deletedEntries = 0
		sizeMap = dict()
		
		# take all files in directory
		filenames = os.listdir(directory)			

		# go through every entry
		for entry in filenames:
			path = os.path.join(directory, entry)
		
			if os.path.isdir(path):
				# recursive call
				deletedEntries = \
				deletedEntries + fileUtility.deleteSameSizedFiles(path, \
					sameDirectory, justPrint)
		
			elif os.path.isfile(path):
			
				# take file info			
				statinfo = os.stat(path)
				
				map = sizeMap
				if sameDirectory == False:
					map = fileUtility.staticSizeMap
				
				# do we have this sized entry already?
				if statinfo.st_size in map:			
					log.log(fileUtility.LOG_NAME, 2, "DEL:\t" + path + "\nPRE: \t" + map[statinfo.st_size])

					# yes! delete
					if not justPrint:
						os.remove(path)
					
					deletedEntries = deletedEntries + 1
				else:
					# no, but no we have
					map[statinfo.st_size] = path
		
			else:
				log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "Not a file nor directory: " + path)
		
		return deletedEntries
	# make method static
	deleteSameSizedFiles = staticmethod(deleteSameSizedFiles)	

	##  Delete all same named files that are of the same size than previously found.
	def deleteSameNamedAndSizedFiles(directory, justPrint):
		
		fileCount = 0
		foundDuplicates = []
		nameMap = dict()
		
		for root, dirs, files in os.walk(unicode(directory), topdown=False):
			root = unicode(root)
			for name in files:
				fileCount += 1
				fullPath = os.path.join(root, unicode(name))
				size = os.stat(fullPath).st_size
				
				if name in nameMap:
					# the entry is a list
					listOfSizes = nameMap[name]
					if size in listOfSizes:
						# delete the new one
						foundDuplicates.append(fullPath)
						if not justPrint:
							os.remove(fullPath)
					else:
						nameMap[name].append(size)
				else:
					nameMap[name] = [ size ]
		
		return foundDuplicates, fileCount
	# make method static
	deleteSameNamedAndSizedFiles = staticmethod(deleteSameNamedAndSizedFiles)
	
	##  Delete all same named files (expect the occurence with greatest file size)
	#   away from a directory (and its subdirectories)
	#   @param directory
	#   @param sameDirectory
	#   @param justPrint
	staticNameMap = dict()
	def deleteSameNamedFiles(directory, sameDirectory, justPrint):
		
		deletedEntries = 0
		nameMap = dict()
		
		# take all files in directory
		# NOTE! It might be that the directory is read protected so allow fallback return ...
		try:
			filenames = os.listdir(directory)
		except:
			log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Unexpected exception: " + str(sys.exc_info()[1]))
			return deletedEntries

		# go through every entry
		for entry in filenames:
			path = os.path.join(directory, entry)
		
			if os.path.isdir(path):
				# recursive call
				deletedEntries = \
				deletedEntries + fileUtility.deleteSameNamedFiles(path, \
					sameDirectory, justPrint)
		
			elif os.path.isfile(path):
			
				# take file info			
				statinfo = os.stat(path)
				
				map = nameMap
				if sameDirectory == False:
					map = fileUtility.staticNameMap

				basename = os.path.basename(path)
				
				# do we have this sized entry already?
				if entry in map:			
					# yes! delete, but which one?
					oldSize = map[entry][1]
					deletePath = ""
					preservePath = ""
					if oldSize < statinfo.st_size:
						# delete old, preserve new
						deletePath = map[entry][0]
						preservePath = path
						map.pop(entry)
						map[entry] = []
						map[entry].append(path)
						map[entry].append(statinfo.st_size)
					else:
						# delete new, preserve old
						deletePath = path
						preservePath = map[entry][0]
						
					log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "DEL:\t" + deletePath + "\nPRE: \t" + preservePath)
					if not justPrint:
						os.remove(deletePath)
					
					deletedEntries = deletedEntries + 1
				else:
					# no, but no we have
					map[entry] = []
					map[entry].append(path)
					map[entry].append(statinfo.st_size)
			else:
				log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "Not a file nor directory: " + path)
		
		return deletedEntries
	# make method static
	deleteSameNamedFiles = staticmethod(deleteSameNamedFiles)	
	
	##  Delete all files/directories that match to strings
	#   @param directory
	#   @param deleteEntries
	#   @param justPrint
	def deleteEntriesThatMatch(directory, deleteEntries, justPrint):
		
		shouldDeletedFiles = 0
		shouldDeletedDirs = 0

		deletedFiles = 0
		deletedDirs = 0
		
		# go through every entry
		for root, dirs, files in os.walk(unicode(directory), topdown=False):

			for dir in dirs:
				path = os.path.join(root, dir)
		
				for deleteEntry in deleteEntries:
					if dir == deleteEntry:
						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "delete this directory:\t" + path)
						shouldDeletedDirs += 1
						if not justPrint:
							try:
								shutil.rmtree(path)
								deletedDirs += 1
							except:
								log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Exception: "+ str(sys.exc_info()[1]))							

			for file in files:
				path = os.path.join(root, file)
			
				for deleteEntry in deleteEntries:
					if file == deleteEntry:
						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "delete this file:\t" + path)
						shouldDeletedFiles += 1
						if not justPrint:
							try:
								os.remove(path)
								deletedFiles += 1
							except:
								log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Exception: "+ str(sys.exc_info()[1]))
	
		return (shouldDeletedFiles, shouldDeletedDirs, deletedFiles, deletedDirs)
	# make method static
	deleteEntriesThatMatch = staticmethod(deleteEntriesThatMatch)
	
	##  Delete all files/directories that contain certain strings
	#   @param directory
	#   @param deleteEntries
	#   @param justPrint
	def deleteEntriesWithString(directory, deleteEntries, justPrint):
		
		shouldDeletedFiles = 0
		shouldDeletedDirs = 0

		deletedFiles = 0
		deletedDirs = 0

		# go through every entry
		for root, dirs, files in os.walk(unicode(directory), topdown=False):

			for dir in dirs:
				path = os.path.join(root, dir)
				
				for deleteEntry in deleteEntries:
					if string.find(dir, deleteEntry) > -1:

						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "delete this directory:\t" + path)
						shouldDeletedDirs += 1
						if not justPrint:
							try:
								shutil.rmtree(path)
								deletedDirs += 1
							except:
								log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Exception: "+ str(sys.exc_info()[1]))

			for file in files:
				path = os.path.join(root, file)
			
				for deleteEntry in deleteEntries:
					if string.find(file, deleteEntry) > -1:
						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "delete this file:\t" + path)
						shouldDeletedFiles += 1
						if not justPrint:
							try:
								os.remove(path)
								deletedFiles += 1
							except:
								log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Exception: "+ str(sys.exc_info()[1]))
		
		return (shouldDeletedFiles, shouldDeletedDirs, deletedFiles, deletedDirs)
	# make method static
	deleteEntriesWithString = staticmethod(deleteEntriesWithString)	
	
	##  Delete all files/directories that start with certain string
	#   @param directory
	#   @param deleteEntries
	#   @param justPrint
	def deleteEntriesWithStart(directory, deleteEntries, justPrint):
	
		shouldDeletedFiles = 0
		shouldDeletedDirs = 0

		deletedFiles = 0
		deletedDirs = 0
		
		# go through every entry
		for root, dirs, files in os.walk(unicode(directory), topdown=False):

			for dir in dirs:
				path = os.path.join(root, dir)
		
				for deleteEntry in deleteEntries:
					if deleteEntry == dir[ : len(deleteEntry)]:

						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "delete this directory:\t" + path)
						shouldDeletedDirs += 1
						if not justPrint:
							try:
								shutil.rmtree(path)
								deletedDirs += 1
							except:
								log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Exception: "+ str(sys.exc_info()[1]))

			for file in files:
				path = os.path.join(root, file)
			
				for deleteEntry in deleteEntries:
					if deleteEntry == file[ : len(deleteEntry)]:
						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "delete this file:\t" + path)
						shouldDeletedFiles += 1
						if not justPrint:
							try:
								os.remove(path)
								deletedFiles += 1
							except:
								log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "Exception: "+ str(sys.exc_info()[1]))
		
		return (shouldDeletedFiles, shouldDeletedDirs, deletedFiles, deletedDirs)
	# make method static
	deleteEntriesWithStart = staticmethod(deleteEntriesWithStart)
	
	
	## Make subdirectories for every starting letter found and move files there
	#   @param directory
	def moveFilesToAlphabeticalSubDirectories(directory, justPrint = False):
		
		# our current directory is where we change back eventually
		original_directory = os.getcwd()
	
		# change to wanted directory
		os.chdir(directory)
	
		# take list of filenames
		filenames = os.listdir(directory)
	
		# existing alphabetical sub directories
		directories = []
	
		# iterate over all file entries
		for file in filenames:
		
			sub_directory = file[0]
			# does sub directory with found alphabet already exist?
			if sub_directory not in directories:
				# no, create directory
				fileUtility.makeDir(sub_directory, justPrint)
				# store our entry
				directories.append(sub_directory)
			
			# move the file in directory
			utility.move(file, sub_directory)
				
		# change working directory back where we were
		os.chdir(original_directory)
	# make method static
	moveFilesToAlphabeticalSubDirectories = staticmethod(moveFilesToAlphabeticalSubDirectories)
		
	##  Uncompress all files from given directory to target directory,
	#   while only uncompressing files of different names
	#   with certain conditions (check the code for these conditions)
	#   @param source_directory
	#   @param target_directory
	#   @param file_count_limit
	def uncompressUnsimilarFilesToDirectory(source_directory, target_directory, file_count_limit, justPrint):
		# command for uncompression program (including path if needed)
		uncompress = "7z.exe e "
		
		# our current directory is where we change back eventually
		original_directory = os.getcwd()

		# change to source directory
		os.chdir(source_directory)
		
		# our list of entries
		files_handled = []
		
		# take list of all filenames
		file_names = os.listdir(source_directory)

		# handled count
		file_count = 0
		
		# handled count
		handled_count = 0
		
		# intialize markers
		previous_name = ""
		previous_key = ""
				
		# go through all entries
		for current_name in file_names:
			
			# accumulate
			file_count += 1
			
			# take the file name stripped out of version
			splitted_name = current_name.partition("(")
			
			# init our version found flag
			version_found = 1
			
			# do we have a '()' version info?
			if splitted_name[1] == "" and splitted_name[2] == "":
				
				# do we have a '[]' version info?
				splitted_name = current_name.partition("[")
				
				if splitted_name[1] == "" and splitted_name[2] == "":
					version_found = 0					
			
			# our key for this game
			current_key = splitted_name[0]
			
			# check handling for previous file if it was not handled and the title changed
			if previous_key != current_key and previous_key != "":
				# handle the previous, do the uncompression
				utility.execute(uncompress + "\"" + previous_name + "\" -o" + target_directory, justPrint)
				# mark as handled
				files_handled.append(previous_key)
				# clear markers
				previous_game = ""
				previous_key = ""
				# accumulate
				handled_count += 1
				
			# if we have already similar file handled, skip
			if current_key in files_handled:
				continue	
			
			# find a version of file, if there is one, of form *(x)*, with reg exp
			match = re.search('\(.\)', current_name)
			
			# take the version string, if available
			if match == None:
				# we don't have version string, but we may have *[x]* id
				match = re.search('\[*\]', current_name)
				# ... so do we?
				if match == None:					
					version_found = 0
				else:
					# just mark this as E, so we can proceed with the first occurence
					version_group = "(E)"
			else:
				version_group = match.group(0)
				
			# if there is only one version, or this is the euro version, stick to this
			if version_found == 0 or version_group == "(E)":
				# do the uncompression
				utility.execute(uncompress + "\"" + current_name + "\" -o" + target_directory, justPrint)
				# mark as handled
				files_handled.append(current_key)
				# clear markers
				previous_name = ""
				previous_key = ""
				# accumulate
				handled_count += 1
			else:
				# this is some other version, (x) with x!=E, mark it
				previous_name = current_name
				previous_key = current_key
			
			# if we hit a file count limit, we exit the loop
			if file_count_limit > 0 and handled_count >= file_count_limit:
				break
				
		# change back to original working directory
		os.chdir(original_directory)
		
		# construct an result list, with handled count and all file count
		results = []
		results.append(handled_count)
		results.append(file_count)
		return results		
	# make method static
	uncompressUnsimilarFilesToDirectory = staticmethod(uncompressUnsimilarFilesToDirectory)	
	
	##  Uncompress all files from given directory to target directory,
	#  @param source_directory
	#  @param target_directory
	#  @param file_count_limit
	def uncompressAllFilesToDirectory(source_directory, target_directory, file_count_limit, justPrint):
		# command for uncompression program (including path if needed)
		uncompress = "7z.exe e "
		
		# our current directory is where we change back eventually
		original_directory = os.getcwd()

		# change to source directory
		os.chdir(source_directory)
		
		# our list of entries
		files_handled = []
		
		# take list of all filenames
		file_names = os.listdir(source_directory)

		# handled count
		file_count = 0
		
		# handled count
		handled_count = 0
		
		# go through all entries
		for current_name in file_names:
			
			# accumulate
			file_count += 1
			
			# do the uncompression
			utility.execute(uncompress + "\"" + current_name + "\" -o" + target_directory, justPrint)
			# mark as handled
			files_handled.append(current_name)
			# accumulate
			handled_count += 1
			
			# if we hit a file count limit, we exit the loop
			if file_count_limit > 0 and handled_count >= file_count_limit:
				break
				
		# change back to original working directory
		os.chdir(original_directory)
		
		# construct an result list, with handled count and all file count
		results = []
		results.append(handled_count)
		results.append(file_count)
		return results
		
	# make method static
	uncompressAllFilesToDirectory = staticmethod(uncompressAllFilesToDirectory)	
	
	##  Goes through all directories, and uncompresses found archives.
	def unzipAllFilesRecursively(path, deleteSource, justPrint):
		
		foundCount = 0
		executedCount = 0
		deletedCount = 0
		
		unzipCommand = "\"C:\\Program Files\\Utils\\7-Zip\\7z.exe\" x"
		for root, dirs, files in os.walk(path, topdown=False):
			for filename in files:
				if fileUtility.isArchiveFile(filename):
					
					foundCount += 1
					fullPath = os.path.join(root, filename)
					fullPath = os.path.abspath(fullPath)
					
					log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "unzip: \"" + fullPath + "\"")
					
					# make a temp dir for the archive ...
					tempDir = os.path.splitext(fullPath)[0] + "_TEMP"
					fileUtility.makeDir(tempDir, justPrint)
					# ... do the unzipping to the temp directory
					# NOTE! With 7z.exe switches, there must not be a SPACE BETWEEN switch and arg!
					fullUnzipCommand = unzipCommand + " -o\"" + tempDir + "\" \"" + fullPath + "\""

					log.log(fileUtility.LOG_NAME, log.LVL_DETAIL, "issuing cmd: \"" + fullUnzipCommand + "\"")
					
					utility.execute(fullUnzipCommand, justPrint)
					
					tempDirEntries = []
					if os.path.exists(tempDir):
						tempDirEntries = os.listdir(tempDir)
						
					# ... if only one file or directory under the temp ...
					if len(tempDirEntries) == 1:
						# move "all", meaning the one and only, stuff from the directory one level down ...
						toMoveFullPath = os.path.join(tempDir, tempDirEntries[0])
						toMoveFullPath = os.path.abspath(toMoveFullPath)
						fileUtility.move(toMoveFullPath, root, justPrint)
						
						# ... and then delete the temp
					entryCount = len(tempDirEntries)
					if entryCount < 2:
						fileUtility.deleteAllEmptyDirectories(tempDir, True, justPrint)
					elif entryCount > 1:
						# this is the case where we have lots of files without proper root - folder, so leave the TEMP, but rename it ...
						newDirName = tempDir[ 0 : len(tempDir) - 5 ]
						os.chmod(tempDir, stat.S_IWRITE)
						os.rename(tempDir, newDirName)

					if not justPrint:
						executedCount += 1
					
						if deleteSource:
							deletedCount += 1
							os.chmod(fullPath, stat.S_IWRITE)
							os.remove(fullPath)
					
		
		return foundCount, executedCount, deletedCount
	# make method static
	unzipAllFilesRecursively = staticmethod(unzipAllFilesRecursively)
	
	
	##  Goes through all directories, and compresses found files if they're ONLY of the specified type
	#  @param directory
	#  @param fileTypes
	#  @param archiveExtension
	#  @param deleteSource
	#  @param justPrint
	def zipAllSpecifiedFilesRecursively(directory, fileTypes, archiveExtension, deleteSource, justPrint):
		
		# our current directory is where we change back eventually
		original_directory = os.getcwd()

		# change to source directory
		os.chdir(directory)
		
		foundFiles = 0
		foundDirectories = 0

		fileExtension = ""
		ofSameExtension = 1
		
		# take all files in directory
		filenames = os.listdir(directory)

		# go through every entry
		for entry in filenames:
			path = os.path.join(directory, entry)

			# is this a directory?
			if os.path.isdir(path):
				
				# recursive call to this same function
				fileUtility.zipAllSpecifiedFilesRecursively(path, fileTypes, archiveExtension, deleteSource, justPrint)
				foundDirectories = foundDirectories + 1
		
			# a file then?
			elif os.path.isfile(path):

				foundFiles = foundFiles + 1
				thisExtension = os.path.splitext(path)[1]
				thisExtension = thisExtension.replace(".", "")
				thisExtension = thisExtension.lower()
				
				# first one that we are dealing?
				if len(fileExtension) == 0:
					fileExtension = thisExtension
				
				# is this different type than the last one?
				if fileExtension != thisExtension:
					ofSameExtension = 0
				
				fileExtension = thisExtension
			
		# check if we support the file type, if we had only one
		if ofSameExtension == 1:
			# init this to false, so we can switch it if we found match
			ofSameExtension = 0
			for i in range(len(fileTypes)):
				# is this a match? if so, then mark it
				if fileTypes[i] == fileExtension:
					ofSameExtension = 1
		
		lowestDirectory = os.path.split(directory)[1]
		
		#print "in dir:",lowestDirectory,"found dirs:",foundDirectories,", found files:",foundFiles,", of same extension:",ofSameExtension
					
		# ok, actual processing, takes place only if we're on the lowest hierarchy level (meaning no sub-directories)
		# also, we require all the files to be of the same type (and on the support-list)
		if foundDirectories == 0 and foundFiles > 0 and ofSameExtension == 1:
			
			archiveName = "\"" + lowestDirectory + "." + archiveExtension + "\""
			# our compression command
			zipCommand = "7z.exe a -tzip " + archiveName + " *." + fileExtension
			
			if justPrint == False:
				utility.execute(zipCommand, justPrint)
				# move the  formed archive one directory down
				utility.execute("move " + archiveName + " ../", justPrint)
				
				if deleteSource == True:
					# delete the source files
					utility.execute("erase *." + fileExtension, justPrint)
					# move one directory down and delete the empty directory
					os.chdir("../")
					os.rmdir(lowestDirectory)
			else:
				print (zipCommand)
		
		# change back to original working directory
		os.chdir(original_directory)
				
	# make method static
	zipAllSpecifiedFilesRecursively = staticmethod(zipAllSpecifiedFilesRecursively)
	
	##  Goes through all directories, and takes all filenames with full path from there recursively.
	#   @param directory
	def getFilePathsFromDirectoryRecursive(directory, foundFilenames, maxDepth=-1, currentDepth = 0):
		
		currentDepth += 1
		
		if maxDepth > 0 and currentDepth > maxDepth:
			return
		
		# our current directory is where we change back eventually
		original_directory = os.getcwd()

		# change to source directory
		os.chdir(directory)
				
		# take all files in directory
		filenames = os.listdir(directory)
		
		# go through every entry
		for entry in filenames:
			path = os.path.join(directory, entry)

			# is this a directory?
			if os.path.isdir(path):
				# recursive call to this same function
				fileUtility.getFilePathsFromDirectoryRecursive(path, foundFilenames, maxDepth, currentDepth)
		
			# a file then?
			elif os.path.isfile(path):
				foundFilenames.append(path)
				
		# change to original directory
		os.chdir(original_directory)
	# make method static
	getFilePathsFromDirectoryRecursive = staticmethod(getFilePathsFromDirectoryRecursive)	
		
	##  Goes through all directories, and does something if there is a given count of files in them
	#   @param directory
	#   @param wantedFileCount
	#   @param foundFilenames
	#   @param foundDirectories
	def goThroughDirectoriesWithFileCount(directory, wantedFileCount, wantedDirCount, \
		foundFilenames, foundDirectories):
		
		foundFilesCount = 0
		foundDirsCount = 0
		
		# take all files in directory
		filenames = os.listdir(directory)			

		files = []
		
		dirsWithMatchedCount = 0
		# go through every entry
		for entry in filenames:
			path = os.path.join(directory, entry)

			# is this a directory?
			if os.path.isdir(path):										
				foundDirsCount += 1
				# recursive call to this same function
				dirsWithMatchedCount += fileUtility.goThroughDirectoriesWithFileCount(path, \
					wantedFileCount, wantedDirCount,\
					foundFilenames, foundDirectories)
		
			# a file then?
			elif os.path.isfile(path):
				files.append(path)
				foundFilesCount += 1
		
		if (foundFilesCount == wantedFileCount or wantedFileCount == -1) and\
			(foundDirsCount == wantedDirCount or wantedDirCount == -1):
			
			foundDirectories.append(directory)
				
			for fileEntry in files:
				foundFilenames.append(fileEntry)
			dirsWithMatchedCount += 1			
		
		return dirsWithMatchedCount
	# make method static
	goThroughDirectoriesWithFileCount = staticmethod(goThroughDirectoriesWithFileCount)	
	
	##  Reads file entries from a file (one per line) and returns array containing them.
	#   @param filename The full path to the input file.
	def readStringsFromFile(filename):
		entries = []
		f = open(filename, 'r')
		for line in f:
			entries.append(line.strip('\n').lstrip().rstrip())
		f.close()
		return entries
		# make method static
	readStringsFromFile = staticmethod(readStringsFromFile)	

	##  Writes all the filenames (strings) given in input array to the file. 
	#   @param filenames All the filenames (strings) to be written (one per line).
	#   @param file Full path to the file where strings will be written.
	def writeStringsToFile(entries, filename):		
		f = open(filename, 'w')
		for entry in entries:
			f.write(entry + "\n")
		f.close()
		# make method static
	writeStringsToFile = staticmethod(writeStringsToFile)	
	
	##  Goes through directory, starting from the bottom-most under the path, and deletes all directories and files.
	#   @param path The path under we perform the action
	def deleteAllDirectories(path):
		for root, dirs, files in os.walk(path, topdown=False):
			for name in files:
				os.remove(os.path.join(root, name))
			for name in dirs:
				os.rmdir(os.path.join(root, name))	
	# make method static
	deleteAllDirectories = staticmethod(deleteAllDirectories)	

	##  Goes through directory, starting from the top most, and replacing all strings with given replacement
	#   @param path The path under we perform the action
	def replaceStringFromAllDirectories(path, source, target, justPrint):
		foundCount = 0
		for root, dirs, files in os.walk(path, topdown=False):
			for name in dirs:
				if name.find(source) > -1:
					foundCount += 1
					oldName = os.path.join(root, name)
					newName = os.path.join(root, name.replace(source, target))

					log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "rename \"" + oldName + "\" -> \"" + newName + "\"")
				
					if not justPrint:
						os.rename(oldName, newName)
		return foundCount
	# make method static
	replaceStringFromAllDirectories = staticmethod(replaceStringFromAllDirectories)	
	
	##  Goes through directory and returns if it is empty
	#   @param path The path under we perform the action
	def isEmpty(path):			
		numberOfFiles = 0
		numberOfDirs = 0
		for root, dirs, files in os.walk(path, topdown=False):
			numberOfDirs += len(dirs)
			numberOfFiles += len(files)
		if numberOfFiles == 0 and numberOfDirs == 0:
			return True
		else:
			return False
	# make method static
	isEmpty = staticmethod(isEmpty)	
	
	##  Goes through directory, and returns counts for files and dirs
	#   @param path The path under we perform the action
	def getFileAndDirectoryCount(path):			
		
		#print "getFileAndDirectoryCount of " + path
		
		dirCount = 0
		fileCount = 0
		for root, dirs, files in os.walk(path, topdown=False):
			dirCount += len(dirs)
			fileCount += len(files)
		
		return ( fileCount, dirCount )
	# make method static
	getFileAndDirectoryCount = staticmethod(getFileAndDirectoryCount)	

	##  Goes through directory, moving all found files to target directory.
	#   @param path The path under we perform the action
	#   @param targetDirectory
	def moveAllFilesAndCollapseDirectories(path, targetDirectory, justPrint):
		movedFiles = []

		fileUtility.makeDir(targetDirectory, justPrint)
		print (path)
		for root, dirs, files in os.walk(path, topdown=False):
			for name in files:
				childFullPath = os.path.join(root, name)
				fileUtility.move(childFullPath, targetDirectory, justPrint)
				movedFiles.append(childFullPath)
		
		return movedFiles
	# make method static
	moveAllFilesAndCollapseDirectories = staticmethod(moveAllFilesAndCollapseDirectories)	


	##  Goes through directory, moving all found files to target directory, preserving directory structure.
	#   @param path The path under we perform the action
	#   @param targetDirectory
	def moveAllFilesAndPreserveDirectoryStructure(path, target, justPrint):
		movedFiles = []
	
		for root, dirs, files in os.walk(path, topdown=False):
			for name in files:
				childFullPath = os.path.join(root, name)
				
				targetDir = root.replace(path, target)

				fileUtility.makeDir(targetDir, justPrint)

				fileUtility.move(childFullPath, targetDir, justPrint, True)
				movedFiles.append(childFullPath)
		
		fileUtility.deleteAllEmptyDirectories(path, False, justPrint)

		return movedFiles
	# make method static
	moveAllFilesAndPreserveDirectoryStructure = staticmethod(moveAllFilesAndPreserveDirectoryStructure)	

	##  Goes through directory, and moves all files/directories one level down.
	#   @param path The path under we perform the action
	#   @param targetDirectory
	def moveAllOneDirectoryDown(path, targetDirectory, level, justPrint):
		movedFiles = []

		movedCount = 0
		
		fileUtility.makeDir(targetDirectory, justPrint)
		
		level = level - 1
		
		for root, dirs, files in os.walk(path, topdown=True):
			
			for dir in dirs:
				childFullPath = os.path.join(root, dir)
				
				#print "[level: " + str(level) + "] checking: " + childFullPath
				
				if level == 0:
					fileUtility.move(childFullPath, targetDirectory, justPrint)
					movedCount = movedCount + 1
				else:
					targetDirectory = root
					movedCount = movedCount + fileUtility.moveAllOneDirectoryDown(childFullPath, targetDirectory, level, justPrint)
					
		return movedCount
	# make method static
	moveAllOneDirectoryDown = staticmethod(moveAllOneDirectoryDown)
	
	##  Goes through directory, starting from the bottom-most under the path, and deletes all empty directories.
	# NOTE! If you want to only remove non-empty directories, you can use os.removedirs(path)
	#   @param path The path under we perform the action
	#   @param deleteAlsoRoot Do we delete also the root if all empty
	def deleteAllEmptyDirectories(path, deleteAlsoRoot, justPrint):
		
		if not os.path.exists(path):
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR, path + "\" does not exist!")
			return
		
		deletedDirectories = []
		preservedDirectories = []
	
		for root, dirs, files in os.walk(path, topdown=False):
			for name in dirs:
				childFullPath = os.path.join(root, name)
				numberOfFiles = 0
				numberOfDirs = 0
				
				for childRoot, childDirs, childFiles in os.walk(childFullPath, topdown=False):
					numberOfDirs += len(childDirs)
					numberOfFiles += len(childFiles)
					
				if numberOfFiles == 0 and numberOfDirs == 0:
					deletedDirectories.append(childFullPath)
	
					log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "dir \"" + childFullPath + "\" empty, deleting ..: ")
					if not justPrint:
						os.rmdir(childFullPath)
				else:
					preservedDirectories.append(childFullPath)
		if deleteAlsoRoot:
			if fileUtility.isEmpty(path):
				deletedDirectories.append(path)

				log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "root dir \"" + path + "\" empty, deleting ..: ")
				if not justPrint:
					os.rmdir(path)	
			else:
				preservedDirectories.append(path)		
		return ( deletedDirectories, preservedDirectories )
	# make method static
	deleteAllEmptyDirectories = staticmethod(deleteAllEmptyDirectories)	

	##  Goes through directory, and if it starts with article, renames it with trailing comma and article.
	#   @param path The path under we perform the action
	def renameArticleDirectories(path):
	
		renamedDirectories = []
		preservedDirectories = []
	
		for root, dirs, files in os.walk(path, topdown=False):
			for name in dirs:
				# try to get article
				articleResult = stringUtilities.parseArticle(name)		
				# if we have article, take it away from entry name
				if articleResult[0] != None and len(articleResult[0]) > 0:			
					renamedDirectories.append(name)
					
					article = articleResult[0]
					dirWithoutArticle = articleResult[1]
					oldName = os.path.join(path, name)
					newName = os.path.join(path, dirWithoutArticle + ", " + article)

					os.rename(oldName, newName)
				else:
					preservedDirectories.append(name)
					
		return renamedDirectories, preservedDirectories
	# make method static
	renameArticleDirectories = staticmethod(renameArticleDirectories)	
	
	comicPostFixes =\
	[	
		"cbr",
		"cbz",
		"rar",
		"zip",
		"pdf",
		"jpg",
		"gif",
		"png"
	]
	##  Checks by examining file postfix if file is comic
	#   @param path The path under we perform the action
	def isComicFile(path):
		postFix = stringUtilities.parseFilePostFix(path)
		if postFix in fileUtility.comicPostFixes:
			return True
		return False
	# make method static
	isComicFile = staticmethod(isComicFile)	
	
	videoPostFixes =\
	[	
		"avi",
		"mov",
		"mp4",
		"m4v",
		"iso",
		"mpg",
		"divx",
		"mkv",
		"ogm",
		"m2ts",
		"ts",
		"asf",
		"3gp"
	]
	##  Checks by examining file postfix if file is video file
	#   @param path The path under we perform the action
	def isVideoFile(path):
		postFix = stringUtilities.parseFilePostFix(path)
		if postFix in fileUtility.videoPostFixes:
			return True
		return False
	# make method static
	isVideoFile = staticmethod(isVideoFile)	
	
	subtitlePostFixes =\
	[	
		"sub",
		"srt",
		"vob",
		"idx",
		"smi"
	]
	##  Checks by examining file postfix if file is subtitle
	#   @param path The path under we perform the action
	def isSubtitleFile(path):
		postFix = stringUtilities.parseFilePostFix(path)
		if postFix in fileUtility.subtitlePostFixes:
			return True
		return False
	# make method static
	isSubtitleFile = staticmethod(isSubtitleFile)	
	
	g_bookPostFixes =\
	[	
		"pdf",
		"ps",
		"lit",
		"djvu",
		"djv",
		"chm",
		"html",
		"htm",
		"txt",
		"doc",
		"cbr",
		"cbz",
		"wri",
		"rtf",
		"mht",
		"ppt",
		"epub"
	]
	##  Checks by examining file postfix if file is ebook
	#   @param path The path under we perform the action
	def isBookFile(path):
		postFix = stringUtilities.parseFilePostFix(path)
		if postFix in fileUtility.g_bookPostFixes:
			return True
		return False
	# make method static
	isBookFile = staticmethod(isBookFile)
	
	g_archivePostFixes =\
	[	
		"zip",
		"7z",
		"rar",
		"gz",
		"tar"
	]
	##  Checks by examining file postfix if file is archive
	#   @param path The path under we perform the action
	def isArchiveFile(path):
		postFix = stringUtilities.parseFilePostFix(path)
		if postFix in fileUtility.g_archivePostFixes:
			return True
		return False
	# make method static
	isArchiveFile = staticmethod(isArchiveFile)
	
	def getText(nodelist):
		rc = ""
		for node in nodelist:
			if node.nodeType == node.TEXT_NODE:
				rc = rc + node.data
		return rc
	getText = staticmethod(getText)	

	def getFirstValueOfNode(node, name):
		nodes = node.getElementsByTagName(name)
		if len(nodes) > 0:
			return fileUtility.getText(nodes[0].childNodes)
		return None
	getFirstValueOfNode = staticmethod(getFirstValueOfNode)	

	def goThroughFileAndRemoveLinesContaining(path, strings):
		file = open(path, 'r')
		if file:
			matchesFound = 0
			newLines = []
			for line in file:
				found = False
				for string in strings:
					if line.find(string) >= 0:
						found = True
						break
				
				if found:
					matchesFound += 1
				else:
					newLines.append(line)
			
			file.close()
			if matchesFound > 0:
				# delete the old
				os.remove(path)
				# create with the stripped lines ...
				file = open(path,"w")
				file.writelines(newLines)
				file.close()
	goThroughFileAndRemoveLinesContaining = staticmethod(goThroughFileAndRemoveLinesContaining)	
	
	## Returns the disc matching serial, if one is attached.
	def getDisc(discSerial):
		allDrives = fileUtility.getDiscInfo()
		for letter, drive in allDrives.items():
			if drive["serial"] == discSerial:
				return drive
		return None
	# make method static
	getDisc = staticmethod(getDisc)	

	# Cached value of current working directory disc serial.
	g_currentDiscID = None

	## Returns the id (serial) of the hard disc of current working directory.
	def getCurrentDiscID():
		
		# if constructed already during this program run, just return previous instance ...
		if fileUtility.g_currentDiscID != None:
			return fileUtility.g_currentDiscID
		
		drives = fileUtility.getDiscInfo()
		cwd = os.getcwd()
		drive = os.path.splitdrive(cwd)[0] 
		fileUtility.g_currentDiscID = drives[drive]["serial"]
		return fileUtility.g_currentDiscID
	# make method static
	getCurrentDiscID = staticmethod(getCurrentDiscID)
	
	# Disc-info map for program instance.
	g_drives = None
	
	##  Returns some basic info about the hard disks currently attached to system.
	# NOTE! Windows specific, and obviously, needs the custom application "DriveLetterExtractor.exe"
	def getDiscInfo(forceRebuild = False, justPrint = False):
		
		if not sys.platform.startswith('win'):
			return None

		if forceRebuild:
			fileUtility.g_drives = None
		
		# if constructed already during this program run, just return previous instance ...
		if fileUtility.g_drives != None:
			return fileUtility.g_drives
		
		# our result vector
		result = dict()

		# NOTE! We take for granted that the called exe lies in FIXED location regarding this python code file
		appDir = os.path.dirname(os.path.realpath(__file__))
		# ... go down two directory levels
		appDir = os.path.split(os.path.split(appDir)[0])[0]
		# ... go up to specified location
		appDir = os.path.join(appDir, "wemarchive\\bin")
		appDir = os.path.normpath(appDir)
		
		# file where the temp tool info will be put
		outputFilename = "drive_info_results.xml"
		outputFilename = os.path.abspath(outputFilename)
		# app args
		exeName = "DriveLetterExtractor.exe"
		command = appDir + "\\" + exeName
		command = os.path.abspath(command)
		
		# execute the command
		utility.execute(command, justPrint)
		
		# this is needed for preventing file-concurrency problems ... DriveLetterExtractor needs time to let go of outputFilename
		time.sleep(1)
		
		# read the generated xml-output
		doc = None
		try:
			doc = xml.dom.minidom.parse(outputFilename)
		except: # catch *all* exceptions
			e = sys.exc_info()[1]
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "Note! Malformed XML")
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "\"" + outputFilename + "\": " + str(e))
		
		if doc == None:
			return None
		
		drives = doc.getElementsByTagName("DriveEntry")
		for drive in drives:
			serial = fileUtility.getFirstValueOfNode(drive, "Serial")
			letter = fileUtility.getFirstValueOfNode(drive, "Letter")
			name = fileUtility.getFirstValueOfNode(drive, "Name")
			space = fileUtility.getFirstValueOfNode(drive, "Space")
			freeSpace = fileUtility.getFirstValueOfNode(drive, "FreeSpace")

			driveResult = dict()
			driveResult["serial"] = serial
			driveResult["letter"] = letter
			driveResult["name"] = name
			driveResult["space"] = space
			driveResult["freeSpace"] = freeSpace
			
			result[letter] = driveResult
		
		fileUtility.g_drives = result
		
		# delete temp file
		os.remove(outputFilename)
		
		return result
	# make method static
	getDiscInfo = staticmethod(getDiscInfo)

	## Helper to fetch disc serial out of existing path.
	def getDiscSerialForPath(path):
		drives = fileUtility.getDiscInfo()
		
		entryLetter = os.path.splitdrive(path)[0].upper()
		
		if not drives == None and entryLetter in drives:
			drive = drives[entryLetter]
			return drive["serial"]
		return None
	# make method static
	getDiscSerialForPath = staticmethod(getDiscSerialForPath)
	
	##  Returns some basic info about the video file, if found. Uses MediaCoder's tool, MediaInfo.
	## Returns (if found): width, height, fps, duration (in minutes)
	#   @param path The full path to the input file.
	def getVideoInfo(path, exitIfMalformed, justPrint = False):
		
		log.log(fileUtility.LOG_NAME, 2, "getVideoInfo() for: " + path)
		
		# our result vector
		result = dict()
		
		appDir =  "C:\\Program Files (x86)\\MediaCoder\\tools"
		exeName = "MediaInfo.exe"
		command = appDir + "\\" + exeName
		command = "\"" + os.path.abspath(command) + "\""
		
		# file where the temp tool info will be put
		outputFilename = "video_info_results.xml"
		outputFilename = os.path.abspath(outputFilename)
		
		#args = "\"" + path + "\" --Inform=Video,Text" + " --Output=XML" + " --LogFile=" + outputFilename
		args = []
		args.append("\"" + path + "\"")
		args.append("--Inform=Video,Text")
		args.append("--Output=XML")
		args.append("--LogFile=\"" + outputFilename + "\"")
		
		#dumbFilename = os.path.abspath("dumb_filename.txt")
		#args.append("> \"" + dumbFilename + "\"")
		dumbFilename = ""
		
		# execute the command
		utility.execute(command, justPrint, args)
		
		# try to ensure we have of right format ...
		linesContaining = [ "Subtitles_Pan&Scan_", "Writing_library", "Comment" ]
		fileUtility.goThroughFileAndRemoveLinesContaining(outputFilename, linesContaining)
		
		if not os.path.exists(outputFilename):
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "Outputfile \"" + outputFilename + "\" does not exist!")
			if exitIfMalformed:
				utility.errorExit("Exiting ... fix the problem and try again!")
		
		# read the generated xml-output
		doc = None
		try:
			doc = xml.dom.minidom.parse(outputFilename)
		except: # catch *all* exceptions
			e = sys.exc_info()[1]
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "Note! Malformed XML with \"" + path + "\"")
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR, "\"" + outputFilename + "\": " + str(e))
			if exitIfMalformed:
				utility.errorExit("Exiting ... fix the problem and try again!")
		
		if doc == None:
			return None
		
		tracks = doc.getElementsByTagName("track")
		languages = []
		allMinutes = 0
		selectedWidth = None
		selectedHeight = None
		selectedFps = None
		
		for track in tracks:
			if track.attributes["type"] != None:
			
				trackType = track.attributes["type"].value
				if trackType == "Video":
					width = fileUtility.getFirstValueOfNode(track, "Width")
					height = fileUtility.getFirstValueOfNode(track, "Height")
					duration = fileUtility.getFirstValueOfNode(track, "Duration")
					minimumFps = fileUtility.getFirstValueOfNode(track, "Minimum_frame_rate")
					fps = fileUtility.getFirstValueOfNode(track, "Frame_rate")

					if width != None:
						width = width.replace("pixels", "").strip()
						width = width.replace(" ", "")
						if len(width) > 0:
							w = int(width)
							if selectedWidth == None or w > selectedWidth:
								selectedWidth = w

					if height != None:
						height = height.replace("pixels", "").strip()
						height = height.replace(" ", "")
						if len(height) > 0:
							h = int(height)
							if selectedHeight== None or h > selectedHeight:
								selectedHeight = h

					if minimumFps != None:
						minimumFps = minimumFps.replace("fps", "").strip()
						minimumFps = minimumFps.replace(" ", "")
						if len(minimumFps) > 0:
							f = float(minimumFps)
							if selectedFps == None or f > selectedFps:
								selectedFps = f

					if selectedFps == None:
						fps = fps.replace("fps", "").strip()
						fps = fps.replace(" ", "")
						if len(fps) > 0:
							f = float(fps)
							if selectedFps == None or f > selectedFps:
								selectedFps = f

					if duration != None:
						minutes = stringUtilities.parseToMinutes(duration)
						if minutes > 0:
							allMinutes += minutes
				
				elif trackType == "Text":
					lang = fileUtility.getFirstValueOfNode(track, "Language")
					if lang != None and len(lang) > 0 and not lang in languages:
						languages.append(lang)

		if selectedWidth != None:
			result["video_width"] = selectedWidth	
		if selectedHeight!= None:
			result["video_height"] = selectedHeight
		if selectedFps != None:
			result["video_fps"] = selectedFps
		result["duration_minutes"] = allMinutes		
		if len(languages) > 0:
			result["subtitles"] = languages
		else:
			result["subtitles"] = None
		
		# delete temp-file
		os.remove(outputFilename)
		# ... remove dumb
		if len(dumbFilename) > 0:
			os.remove(dumbFilename)
		
		return result
	# make method static
	getVideoInfo = staticmethod(getVideoInfo)	
	
	##  Returns the size of given files
	#   @param path The files list
	def getSizeOfFiles(files):
		size = 0
		for filePath in files:
			size += os.path.getsize(filePath)
		return size
	# make method static
	getSizeOfFiles = staticmethod(getSizeOfFiles)	
	
	##  Returns the size of directory (all of it's files).
	#   @param path The full path to the input file.
	def getSizeOfDir(directory):	
		
		dirSize = 0
		for (path, dirs, files) in os.walk(unicode(directory)):
			for file in files:
				filename = os.path.normpath(os.path.join(path, file))
				dirSize += os.path.getsize(filename)
		return dirSize
	# make method static
	getSizeOfDir = staticmethod(getSizeOfDir)
	
	##  Copy file from source to target
	#   @param source Full source path for the file that is copied.
	#   @param target Full target path where the file is copied.
	def copy(source, target, justPrint):
		log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS , "copying: \"" + source + "\" to \"" + target + "\"")
		if not justPrint:
			shutil.copy(source, target)
	# make method static
	copy = staticmethod(copy)
	
	##  Make directory if it does not already exist
	#   @param dir The directory to be created. 
	def makeDir(dir, justPrint):
		if os.path.exists(dir):
			log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "already exists: \"" + dir + "\"")
			return
			
		#log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "creating dir: \"" + dir + "\"")
		if not justPrint:
			os.makedirs(dir)
	# make method static
	makeDir = staticmethod(makeDir)
	
	##  Move file from source to target
	#   @param source Full source path of the file to be moved.
	#   @param target Where the file is moved.
	def move(source, target, justPrint, deleteSourceIfTargetExits = False):
		#log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS , "called move: \"" + source + "\" to \"" + target + "\"")
		
		if not justPrint:
			#os.chmod(source, stat.S_IWRITE)
			os.chmod(source, 0o777)

			if os.path.exists(target):
				#os.chmod(target, stat.S_IWRITE)
				os.chmod(target, 0o777)

		# if source and target are directories ...
		if os.path.isdir(source) and os.path.isdir(target):
			# ... and the top-level directory-names are same, just recurse
			if os.path.split(source)[1] == os.path.split(target)[1]:
				for root, dirs, files in os.walk(source, topdown=False):
					for filename in files:
						fileUtility.move(os.path.join(root, filename), target, justPrint)
				
				return
		
		elif source == target:
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR , "src (\"" + source + "\") and trg (\"" + target + "\") the same! NOT moving.")
			return
		else:
			if os.path.isfile(source) and os.path.isdir(target):
				targetFile = os.path.join(target, os.path.split(source)[1])
				
				if os.path.exists(targetFile):
					if deleteSourceIfTargetExits:
						log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS, "target exists, deleting src (\"" + source + "\"")
						
						if not justPrint:
							os.remove(source)
					else:
						log.log(fileUtility.LOG_NAME, log.LVL_ERROR , "src (\"" + source + "\") exists already in trg-folder (\"" + target + "\") the same! NOT moving.")
					
					return
		
		if justPrint:
			log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS , "would move: \"" + source + "\" to \"" + target + "\"")
		else:
			shutil.move(source, target)




	# make method static
	move = staticmethod(move)
	
	g_multiplierGB = 1 / (1024.0*1024.0*1024.0)
	##  Convert the given bytes to more nice Gigabyte representation for pretty printing.
	def inGB(bytes):
		inGB = fileUtility.g_multiplierGB * long(bytes)
		return "%0.2f GB" % (inGB)
	# make method static
	inGB = staticmethod(inGB)
	
	##  Move file with wild cards to other disc, preserving directory structure.
	## NOTE! Uses "glob.glob", so these letters are reserved and have regexp-functionality: [, ], *, ?
	## -> We can have * and ? in matchPattern, but NO [ or ] !!!
	#   @param source Full source path of the file to be moved.
	#   @param target Where the file is moved.
	def moveToOtherDisc(matchPattern, targetDisc, justPrint):
		global g_threadBreak
	
		#matchPattern = os.path.normpath(matchPattern)
		targetDisc = os.path.normpath(targetDisc)
		
		dir = os.path.dirname(matchPattern)
		withoutDisc = os.path.splitdrive(dir)[1]
		targetDir = os.path.join(targetDisc, withoutDisc)
		fileUtility.makeDir(targetDir, justPrint)

		globSource = matchPattern
		globSource = stringUtilities.replaceWith(globSource, "[", False, "?")
		globSource = stringUtilities.replaceWith(globSource, "]", False, "?")

		filesToMove = glob.glob(globSource)
		
		# check if there is enough space, with some margin ...
		marginBytes = 1024 * 1024
		fileSizes = long(fileUtility.getSizeOfFiles(filesToMove))
		# NOTE! need to forcefully update the  disc-info,, so we don't get cached results instead  ...
		spaceOnTarget = long(fileUtility.getDiscInfo(True)[targetDisc]["freeSpace"])

		log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS ,\
			"files to move: " + str(len(filesToMove)) + " (" + fileUtility.inGB(fileSizes)  +\
			"), space left in target: " + fileUtility.inGB(spaceOnTarget))

		if spaceOnTarget <= fileSizes + marginBytes:
			log.log(fileUtility.LOG_NAME, log.LVL_ERROR ,\
				"no space for \"" + matchPattern + "\" -> \"" + targetDisc + "\" " +\
				"(need " + fileUtility.inGB(fileSizes - spaceOnTarget) + " more)")
			return False, fileSizes

		for sourceFile in filesToMove:		
			targetFile = os.path.join(targetDir, os.path.split(sourceFile)[1])
			
			# is this a directory? if so, recurse ...
			if os.path.isdir(sourceFile):
				
				dirOk, dirSizes = fileUtility.moveToOtherDisc(os.path.join(sourceFile, "*"), targetDisc, justPrint)
				fileSizes += dirSizes
				
			elif os.path.isfile(sourceFile):
				if justPrint:
					log.log(fileUtility.LOG_NAME, log.LVL_PROGRESS , "move to: \"" + targetFile + "\"")
				else:
					g_threadBreak = False
					threadID = None
					try:
						fileSizeBytes = os.path.getsize(sourceFile)
						threadID = thread.start_new_thread(fileCopyLogger, (targetFile, fileSizeBytes))
					except Exception as errtxt:
						log.log(fileUtility.LOG_NAME, log.LVL_ERROR , str(errtxt))

					# do this in chunkcs, so we can log the file size during process ...
					src = open(sourceFile, 'rb')
					trg = open(targetFile, 'wb')
					try:
						with open(targetFile, 'wb') as trg:
							shutil.copyfileobj(src, trg, 1024)
							g_threadBreak = True
					finally:
						src.close()					
					# wait a bit for thread to terminate
					time.sleep(1)
			else:
				utility.errorExit("Not a file or directory: \"" + sourceFile + "\"!")
			
			# delete the source, as this should be a MOVE command ...
			if not justPrint:
				if os.path.isdir(sourceFile):
					os.rmdir(sourceFile)
				elif os.path.isfile(sourceFile):
					os.remove(sourceFile)
				else:
					utility.errorExit("Not a file or directory: \"" + sourceFile + "\"!")
				
		# success!
		return True, fileSizes
	# make method static
	moveToOtherDisc = staticmethod(moveToOtherDisc)
	
	
	