##  @package utility
#   Collection of static utilities for file handling.  

import os
import sys
import re
import datetime
import time
import shutil
import string
import locale
from stat import *

import utility

from stringUtilities import stringUtilities
from fileUtility import fileUtility
from database import database
from log import log

##  Class containing all the functions.
class categorize:

	LOG_NAME = __name__

	# log-levels:
	# 0 - only errors
	# 1 - + progress info
	# 2 - + basic program action notifications (deleting, making directories, etc.)
	# 3 - NOT USED CURRENTLY
	# 4 - + very much info about what happens (adding entries and what entries etc.)
	
	MIN_ACCEPTED_YEAR = 1901
	MAX_ACCEPTED_YEAR = 2014
	MAX_ACCEPTED_ISSUE = 1500
	
	g_acceptedModes =\
	[
		"comic", "tv", "movie", "subtitle", "ebook", "multi"
	]
	
	g_forbiddenWords =\
	{
		"comic" : [" ctc ", " c2c " ],
		"tv" : ["hdtv", "x264", "xvid-at", "xvid", "divx", "ac3", "amc", "hddvd", "hidef",\
					"-dimension", "orenji", "notv", "aaf", "2hd", "axxo", "sitv", "-hdq", "topaz", "tpz-", "immerse",\
					"bluetv","-mint", ".hr.","caph","remastered", "omicron", "dgas", \
					"mp3", "mkv", "-ctu", ".ctu", "fov", "dsr", "-th", "-ch", "pdtv", "-lol", ".lol", "dot", "0tv", "fxg",\
					"fxm", "dvbrip", "vmtk", "dvdrip", "dvd", "ctrlhd", ".rerip.", "~tm", ".hr.", "-red", "[Eclipse]",\
					"720p", "1080p", "-hl-", "melite", "-waf", "-tmc", "- tcm", "- tmc", "-sinners",\
					"-nph", ".sfm", "bluray", "-hv", "-vf", "-gou", "- episode"],
		"movie" : [ ],
		"ebook" : ["ebook"],
		"multi" : ["ebook"]
	}
	g_forbiddenWordsIgnoreCase =\
	[
		"tv", "movie"
	]
	
	def isFileTypeRight(fullPath, mode):
		if categorize.g_acceptedModes.count(mode) == 0:
			utility.errorExit("Error! Mode \"" + mode + "\" not supported!")
		
		if mode == "comic":
			return fileUtility.isComicFile(fullPath)
		elif mode == "subtitle":
			return fileUtility.isSubtitleFile(fullPath)
		elif mode == "movie" or mode == "tv":
			return fileUtility.isVideoFile(fullPath)
		elif mode == "ebook":
			return fileUtility.isBookFile(fullPath)
		# as default, return true
		return True
	# make method static
	isFileTypeRight = staticmethod(isFileTypeRight)	
		
	##  Goes through directory, and returns counts for files and dirs
	#   @param path The path under we perform the action
	def getFileTypeCount(path, mode):			
		
		fileCount = 0
		for root, dirs, files in os.walk(path, topdown=False):
			for file in files:
				if categorize.isFileTypeRight(os.path.join(root, file), mode):
					fileCount += 1
		return fileCount
	# make method static
	getFileTypeCount = staticmethod(getFileTypeCount)	
	
	g_alreadyPrintedShouldAddCategory = []
	
	def getOrCreateCategory(table, key, value, database, useYear, justPrint):

		search = dict()
		
		if useYear:
			# prune possible year away from the value ...
			yearResult = stringUtilities.parsePublishYear(value, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
			pubYear = yearResult[0]
			if not pubYear == None:
				search["pub_year"] = pubYear
				value = stringUtilities.parseAwayEmptyParenthesis(yearResult[1])
		
		value = str(value.replace("  ", " ").strip())
		search[key] = value
	
		addedCount = 0
		category = None
		
		alreadyLogged = value in categorize.g_alreadyPrintedShouldAddCategory
		if not alreadyLogged:
			categorize.g_alreadyPrintedShouldAddCategory.append(value)
		
		# check if category exists, if not, add it
		categoryCount = database.getCountForWhere(table, search)
		if categoryCount < 1:
			
			if not alreadyLogged:
				log.log(categorize.LOG_NAME, log.LVL_PROGRESS, "Should add category: \"" + value + "\"")
			
			# form a category entry
			catAddResult = database.addEntries(table, search, False, justPrint)
			if catAddResult != None:
				addedCount += 1
			
		elif categoryCount > 1:
			if not alreadyLogged:
				log.log(categorize.LOG_NAME, log.LVL_ERROR, "Category count for " + value + " == " + str(cagetoryCount) + " in DB!!!")
		else:
			if not alreadyLogged:
				log.log(categorize.LOG_NAME, log.LVL_DETAIL, "category: \"" + value + "\" already in DB")
		
		# take the category id from the category table that will be given to the entry in different table ...	
		category = database.getFirstFromTableWhere(table, search) 
		
		return category, categoryCount, addedCount
	# make method static
	getOrCreateCategory = staticmethod(getOrCreateCategory)

	def addEntry(\
		newEntry, database, entryTable, \
		columns, duplicateCheckColumns, modifyColumns,\
		sourceDir, targetDir, deleteDuplicates,\
		justPrint):
		
		keysAndValues = dict()
		checkKeysAndValues = dict()
		
		if columns == None:
			# just use the entry keys as they are ...
			columns = list()
			for key, val in newEntry.items():
				columns.append(key)
		
		for col in columns:
			if col in newEntry:
				keysAndValues[col] = newEntry[col]

		for checkCol in duplicateCheckColumns:
			if checkCol in keysAndValues:
				checkKeysAndValues[checkCol] = keysAndValues[checkCol]
		
		# check if entry exists, if not, add it directly
		addNewEntry = False
		addedEntries = 0
		modifyExistingEntry = False
		filePathDuplicateFound = False
		deletedCount = 0
		fileToRemove = ""
		keepDatabaseEntry = True

		alreadyInCount = database.getCountForWhere(entryTable, checkKeysAndValues)
		modifiedEntries = 0

		newEntryPath = categorize.getFullPathNoIndex(newEntry)
		
		# it's an error if there are more than one in database, as it should have been pruned already!
		if alreadyInCount > 1:
			log.log(categorize.LOG_NAME, log.LVL_ERROR,\
				"ERROR! There are " + str(alreadyInCount) + " entries in database with search criteria:")
			for checkKey, checkValue in checkKeysAndValues.items():
				log.log(categorize.LOG_NAME, log.LVL_ERROR, "\t" + str(checkKey) + ": " + str(checkValue))
		# in this case we have one match in database for this entry ... make a selection between these two		
		elif alreadyInCount == 0:
			# mark the addition, done later below
			addNewEntry = True
		else: #  alreadyInCount == 1
			
			alreadyInEntry = database.getFirstFromTableWhere(entryTable, checkKeysAndValues) 
			entryColumnNameToIndex = database.getColumnFields(entryTable)

			# we're interested in file/path name and the filesize
			# 1. if there's "noads" in the entry, it gets selected
			# 2. if 1 is true for both, or  not true for both, then the bigger filesize wins
			index = database.getColumnFields(entryTable)
			databaseEntryPath = categorize.getFullPath(alreadyInEntry, index)
			
			# if the filepaths are the same, this propably means we have just executed the script twice for same material
			# -> this means we should not delete or add anything
			if newEntryPath.lower() == databaseEntryPath.lower():
				filePathDuplicateFound = True
				log.log(categorize.LOG_NAME, log.LVL_PROGRESS, "NOTE! duplicate (fullpath) in DB, skipping \"" + databaseEntryPath + "\"")
				log.log(categorize.LOG_NAME, log.LVL_PROGRESS, categorize.getAsString(newEntry))
			else:
				sizeDatabaseEntry = alreadyInEntry[entryColumnNameToIndex["size"]]
				sizeNewEntry = newEntry["size"]
				
				# as a fail-safe, we check here that we still have the file that the DB says it has,
				if not os.path.exists(databaseEntryPath):
					log.log(categorize.LOG_NAME, log.LVL_ERROR, "NOTE! Don't have file (even if it is in Database): " + databaseEntryPath)
					keepDatabaseEntry = False
				else:
					noAdsInDatabaseEntry = databaseEntryPath.find("noads") != -1
					noAdsInNewEntry = newEntryPath.find("noads") != -1
					
					keepDatabaseEntry = True
					if noAdsInDatabaseEntry == True:
						if noAdsInNewEntry == True:
							keepDatabaseEntry =  sizeNewEntry > sizeDatabaseEntry
					else:
						if noAdsInNewEntry == True:
							keepDatabaseEntry = False
						else:
							keepDatabaseEntry = sizeDatabaseEntry >= sizeNewEntry
	

				if keepDatabaseEntry == True:
					# we preserve the entry found in database, and delete the new entry
					fileToRemove = newEntryPath
				else:
					# we want to keep the new entry, so we delete the old database entry (file),
					# and modify the database entry (we modify instead of delete/add as there might be some
					# already modified, interesting data such as ratings, notes, etc.)
					modifyExistingEntry = True
					# we delete the entry from database, and preserve/add the new one
					fileToRemove = databaseEntryPath
					
					# take the values we want to change from the new entry
					newKeysAndValues = dict()			
					for col in modifyColumns:
						if col in keysAndValues:
							newKeysAndValues[col] = keysAndValues[col]
					# swap
					keysAndValues = newKeysAndValues
						
				if len(fileToRemove) > 0:
					if keepDatabaseEntry == True:
						log.log(categorize.LOG_NAME, 2, "selected EXISTING over new:")
					else:
						log.log(categorize.LOG_NAME, 2, "selected NEW over existing:")				
					log.log(categorize.LOG_NAME, 2, "\tDB  [" + str(sizeDatabaseEntry) + "] - \"" + databaseEntryPath + "\"")
					log.log(categorize.LOG_NAME, 2, "\tNew [" + str(sizeNewEntry) + "] - \"" + newEntryPath + "\"")

					if deleteDuplicates and not justPrint:
						os.remove(fileToRemove)
						deletedCount += 1
					
		if addNewEntry or modifyExistingEntry:
			newCategoryDirectory = os.path.normpath(os.path.join(targetDir, keysAndValues["path"]))
			
			# if the target directory does not exists, we need to create it, of course
			if not os.path.exists(newCategoryDirectory):
				log.log(categorize.LOG_NAME, log.LVL_DETAIL, "mkDir: \"" + newCategoryDirectory + "\"")
				fileUtility.makeDir(newCategoryDirectory, justPrint)
			
			# manipulate filesystem
			if sourceDir != targetDir:
				# modify the entry as we move it 
				keysAndValues["root_path"] = targetDir

				log.log(categorize.LOG_NAME, log.LVL_DEBUG, "move: \"" + newEntryPath + "\" to: \"" + newCategoryDirectory + "\"")
				
				if not justPrint:
					utility.move(newEntryPath, newCategoryDirectory) 
			
			# manipulate database
			if modifyExistingEntry:
					# update the existing database entry
				modResult = database.updateValuesWhere(entryTable, newKeysAndValues, checkKeysAndValues, False, justPrint)
				if not justPrint and modResult != None:
					modifiedEntries += 1
			elif addNewEntry:
				#log.log(categorize.LOG_NAME, 2, "add as NEW: " + categorize.getAsString(newEntry))
				log.log(categorize.LOG_NAME, log.LVL_PROGRESS, "NEW: \"" + newEntryPath + "\"")
				
				# add the new entry to SQL Database
				addResult = database.addEntries(entryTable, keysAndValues, True, justPrint)
				if not justPrint and addResult != None:
					addedEntries += 1
		
		return alreadyInCount, filePathDuplicateFound, addNewEntry,\
			addedEntries, modifyExistingEntry, modifiedEntries,\
			fileToRemove != "", deletedCount, keepDatabaseEntry
			
	# make method static
	addEntry = staticmethod(addEntry)		
	
	def categorizeAndPutToDatabase(\
					sourceDirectory, targetDirectory,\
					categoryTable, categoryColumnNameInEntryTable, entryTable, enableCategories,\
					mode, deleteDuplicates,\
					columns, duplicateCheckColumns, modifyColumns,\
					justPrint):
	
		# here we put all our results
		results = dict()
		# do the stuff!!
		rootCategory = []
		categorize.directoryCategorizerWithRoot(results, rootCategory, sourceDirectory, "", enableCategories, mode)

		fileUtility.makeDir(targetDirectory, justPrint)

		db = database()
		db.connect("memarc")
		
		categoryColumnNameToIndex = []
		categoriesEnabled = len(categoryTable) > 0 and len(categoryColumnNameInEntryTable) > 0
		
		categoryColumnNameToIndex = None
		if categoriesEnabled:
			categoryColumnNameToIndex = db.getColumnFields(categoryTable)
		useYearInCategory = (categoryColumnNameToIndex) != None and ("pub_year" in categoryColumnNameToIndex)
		
		entryColumnNameToIndex = db.getColumnFields(entryTable)

		addedCategories = 0		
		addedEntries = 0
		modifiedEntries = 0
		deletedEntries = 0

		shouldAddedCategories = 0
		shouldAddedEntries = 0
		shouldModifiedEntries = 0
		shouldDeletedEntriesDB = 0
		shouldDeletedEntriesNew = 0
		
		duplicatesFound = 0
		filePathDuplicatesFound = 0
		databaseDuplicatesFound = 0
		
		# just count the results first, for printing out progression
		totalCategoryCount = len(results.items())
		totalItemCount = 0
		for categoryKey, categoryValue in results.items():
			totalItemCount += len(categoryValue)
		
		log.log(categorize.LOG_NAME, log.LVL_PROGRESS, "total results in \"" + sourceDirectory + "\"" +\
			": " + str(totalItemCount) + " [categories: " + str(totalCategoryCount) + "]")

		handledItemCount = 0
		progressPrintDelta = 100
		nextProgressPrintCount = progressPrintDelta
			
		# go throug every result category
		for categoryKey, categoryValue in results.items():
			
			categoryId = -1
			
			if categoriesEnabled:
				## CATEGORY (meaning, folders)
				category, alreadyInCat, addedCat = \
					categorize. getOrCreateCategory(categoryTable, "name", categoryKey, db, useYearInCategory, justPrint)
				addedCategories += addedCat
				if addedCat == 0:
					shouldAddedCategories += 1
				
				if category == None or  not "id" in categoryColumnNameToIndex:
					# could not retrieve category id!  (and not just printing ...)
					if not justPrint:
						log.log(categorize.LOG_NAME, log.LVL_ERROR, "No category, either could not create in table or fetching it failed.")
						return False
				else:
					categoryName = category[categoryColumnNameToIndex["name"]] 
					categoryId = category[categoryColumnNameToIndex["id"]]	
					log.log(categorize.LOG_NAME, 4, "Category \"" + categoryName + "\" with id = " + str(categoryId) + " found.")
			
			## ENTRIES INSIDE THE CATEGORY (meaning, files)
			
			# now, go through all entries in category
			for categoryEntry in categoryValue:		

				if categoryId > -1:
					categoryEntry[categoryColumnNameInEntryTable] = categoryId
			
				alreadyInCount, filePathDuplicate, add, added, modify, modified, delete, deleted, keptDB =\
					categorize.addEntry(\
						categoryEntry, db, entryTable, columns, duplicateCheckColumns,\
						modifyColumns, sourceDirectory, targetDirectory, deleteDuplicates, justPrint)
				
				if alreadyInCount > 0:
					duplicatesFound += 1
				if alreadyInCount > 1:
					databaseDuplicatesFound += 1
				if filePathDuplicate:
					filePathDuplicatesFound += 1
				if add:
					shouldAddedEntries += 1
				addedEntries += added
				if modify:
					shouldModifiedEntries += 1
				modifiedEntries += modified
				if delete:
					if keptDB:
						shouldDeletedEntriesNew += 1
					else:
						shouldDeletedEntriesDB += 1
						
				deletedEntries += deleted
				
				handledItemCount += 1
				
				# do we want to print?
				if handledItemCount >= nextProgressPrintCount:			
					log.log(categorize.LOG_NAME, log.LVL_PROGRESS, "processed against DB"+\
						": " + str(handledItemCount) + " of " + str(totalItemCount))
					nextProgressPrintCount += progressPrintDelta
				
		deletedPreservedString = ""
		
		if not justPrint and deleteDuplicates:
			# remove source, if empty
			deletedPreserved = fileUtility.deleteAllEmptyDirectories(sourceDirectory, False, justPrint)
			deletedPreservedString = "Directories deleted: " + str(len(deletedPreserved[0])) +\
				", preserved: " + str(len(deletedPreserved[1])) + "\n"
			# warn if did not succeed entirely
			if os.path.exists(sourceDirectory):
				deletedPreservedString += "Warning! Could not delete source directory: " + sourceDirectory + ", it is not empty!"
			else:
				deletedPreservedString += "Deleted also empty source directory: " + sourceDirectory
				
		lines = []		
		lines.append("Added (/should) categories: " + str(addedCategories)  + " (" + str(shouldAddedCategories) + ")")
		lines.append("Added (/should) entries: " + str(addedEntries)  + " (" + str(shouldAddedEntries) + ")")
		lines.append("Modified (/should) entries: " + str(modifiedEntries)  + " (" + str(shouldModifiedEntries) + ")")
		lines.append("Deleted (/should NEW, DB) entries: " + str(deletedEntries)  + " (" +\
			str(shouldDeletedEntriesNew) + " / " + str(shouldDeletedEntriesDB) + ")")
		lines.append("Duplicates found: " + str(duplicatesFound))
		lines.append("Filepath duplicates found: " + str(filePathDuplicatesFound))
		if databaseDuplicatesFound > 0:
			lines.append("Duplicates in DB (should not be! malformed DB!): " + str(databaseDuplicatesFound))
		if len(deletedPreservedString) > 0: 
			lines.append(deletedPreservedString)
		log.printOutput(lines, categorize.LOG_NAME, 1)
		
		return True
	# make method static
	categorizeAndPutToDatabase = staticmethod(categorizeAndPutToDatabase)		
	
	def getFullPath(entry, index):
		result = ""
		if "disc_id" in index and index["disc_id"] < len(entry) and\
			entry[index["disc_id"]] != None and len(entry[index["disc_id"]]) > 0:
			
			discID = entry[index["disc_id"]]
			disks = fileUtility.getDiscInfo()
			for k, v in disks.items():
				if v["serial"] == discID:
					result = str(v["letter"]) + os.sep
					break
		if "root_path" in index:
			result = os.path.join(result, str(entry[index["root_path"]]))
		if "path" in index:
			result = os.path.join(result, str(entry[index["path"]]))
		if "filename" in index:
			result = os.path.join(result, str(entry[index["filename"]]))
		return os.path.normpath(result) 
	# make method static
	getFullPath = staticmethod(getFullPath)

	def getFullPathNoIndex(entry):
		result = ""

		if "disc_id" in entry and entry["disc_id"] != None and len(entry["disc_id"]) > 0:
			discID = entry["disc_id"]
			disks = fileUtility.getDiscInfo()
			for k, v in disks.items():
				if v["serial"] == discID:
					result = str(v["letter"] + os.sep)
					break
		elif not sys.platform.startswith('win'):
			result = os.sep

		if "root_path" in entry and entry["root_path"] != None:
			result = os.path.join(result, str(entry["root_path"]))
		if "path" in entry and entry["path"] != None:
			result = os.path.join(result, str(entry["path"]))
		if "filename" in entry and entry["filename"] != None:
			result = os.path.join(result, str(entry["filename"]))
		return os.path.normpath(result) 
	# make method static
	getFullPathNoIndex = staticmethod(getFullPathNoIndex)		
	
	def getAsString(entry, printFields = None):
		resultString = ""
		if "size" in entry and "path" in entry and "filename" in entry:
			resultString = "[size: " + str(entry["size"] ) + "] - \"" + entry["path"] + "\\" + entry["filename"] + "\"\n"
		
		keys = entry.keys()
		keys.sort()
		if printFields != None and len(printFields) > 0:
			keys = printFields
			
		for key in keys:
			if key in entry:
				resultString += "\t" + key + ": " + str(entry[key]) + "\n"
		return resultString
	# make method static
	getAsString = staticmethod(getAsString)	

	##  Function that gets all the entries with the same given fields INSIDE A CATEGORY. 
	#   @param categoryResults The constructed categories with "directoryCategorizerWithRoot", indexed with some category (so, parameter is a dict with table fields as indices).
	#   @param sameCheckedFields
	#   @param allowedDifferences
	#   @return A dictionary that as keys has the a table with "sameCheckedFields"'s values that categoryResults contain as different combinations, and values are a table with
	#               all those entries associated.
	def getEntriesWithSameCategoryValues(categoryResults, sameCheckedFields, allowedDifferences):
		# results, a list of  dict-lists
		results = []
		
		totalCount = len(categoryResults)
		handledCount = 0
		afterHandledDelta = 1000
		afterHandledPrintProgress = afterHandledDelta
		
		for entry in categoryResults:
			# already a result with this entry's values?
			foundInAll = False
			# go through every already constructed result list
			for dictList in results:
				# is this result list the right one?
				for resultEntry in dictList:					
					foundInCurrent = True
					# check for every wanted field
					for fieldName, differences in map(None, sameCheckedFields, allowedDifferences):
						
						fieldInEntry = fieldName in entry
						fieldInResult = fieldName in resultEntry						
						
						if fieldInEntry and fieldInResult:
							
							compareItem = entry[fieldName]
							resultItem = resultEntry[fieldName]
							
							# we want to ignore some strings in the value?
							if len(differences) > 0:
								for dif in differences:
									resultItem = resultItem.replace(dif, "")
									compareItem = compareItem.replace(dif, "")
								# make sure we did not leave some duplicate white spaces ...
								resultItem = stringUtilities.parseMultiSpacesAway(resultItem)
								compareItem = stringUtilities.parseMultiSpacesAway(compareItem)
							
							if resultItem != compareItem:
								# ... no, mark that
								foundInCurrent = False
								break	
						elif not fieldInResult or not fieldInEntry:
							foundInCurrent = False
							break	
							
					# was this right one?
					if foundInCurrent == True:
						# yes! we add to this result and exit
						foundInAll = True
						dictList.append(entry)
						break
				# exit, if we found
				if foundInAll == True:
					break
			# if did not found on previous results, add this
			if foundInAll == False:
				newResult = []
				newResult.append(entry)
				results.append(newResult)
			
			# print progress?
			handledCount += 1
			if handledCount >= afterHandledPrintProgress:
				# print!
				log.log(categorize.LOG_NAME, 2, "processed"+\
					": " + str(handledCount) + " of " + str(totalCount))
				# init next round
				afterHandledPrintProgress += afterHandledDelta
			
		return results		
	# make method static
	getEntriesWithSameCategoryValues = staticmethod(getEntriesWithSameCategoryValues)	
	
	##  Function to parse string, and form entries (for database, for example) out of it, with some rules.
	# These entries will be formed:
	# 1. filename	- the file name without path
	# 2. full_path	- full path for file, with drive letter and whole path including filename
	# 3. path	     - the same as full_path but witouht filename
	# 3. size			- the size of the file
	# 4. file_type	- the file postfix, after the dot, usually with three letters
	# 5. pub_year	- publishing date of the file (stripped from the file name), OPTIONAL
	# 6. issue		- issue number of the file (stripped from the file name), OPTIONAL
	# 7. article		- article for the file name (stripped from the file name), OPTIONAL
	# 8. title			- title of the file (stripped from the the file name)
	#
	#   @param entry
	#   @param fullpath
	def handleFilenameEntry(entry, fullpath, rootPath, mode, fileCountInFolder, isMultiDirectory):

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry entry \"" + entry + "\",  fullPath: \"" + fullpath + "\"")

		if categorize.g_acceptedModes.count(mode) == 0:
			utility.errorExit("Error! Mode \"" + mode + "\" not supported!")
			
		# result dictionary
		result = dict()

		fullpath = os.path.abspath(fullpath)
		
		disc_id = fileUtility.getDiscSerialForPath(fullpath)
		if disc_id != None:
			result["disc_id"] = disc_id
		
		# store these for reference
		result["filename"] = entry

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to get size ...")
		
		try:
			result["size"] = os.path.getsize(unicode(fullpath))
		except:
			result["size"] = 0
		
		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to get drive letter ...")

		result["root_path"] = stringUtilities.parseDriveLetter(rootPath)[1]
		
		strippedPath = os.path.splitdrive(os.path.dirname(fullpath))[1]
		strippedPath = strippedPath.replace(result["root_path"], "")
		strippedPath = stringUtilities.parseAwayLeading(strippedPath, "\\")
		strippedPath = stringUtilities.parseAwayLeading(strippedPath, "/")
		result["path"] = strippedPath

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to get file type ...")
		
		# take the file type (postfix)
		fileType = stringUtilities.parseFilePostFix(entry)
		# strip file type away from entry name
		entry = stringUtilities.parseFileTypeAway(entry)
		result["file_type"] = fileType
		
		isVideo = False

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to get pub year ...")
		
		# try to get pub year, give priority to parenthesis
		yearResult = stringUtilities.parsePublishYear(entry, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
		pubYear = yearResult[0]
		if not pubYear == None:
			result["pub_year"] = pubYear
			entry = yearResult[1]

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to clean from forbidden words ...")
			
		cleanedFromForbidden = entry
		if mode in categorize.g_forbiddenWords.keys():
			cleanedFromForbidden = stringUtilities.replaceWith(cleanedFromForbidden,\
				categorize.g_forbiddenWords[mode],\
				mode in categorize.g_forbiddenWordsIgnoreCase,\
				" ").strip()
		
		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to do mode-spesific stuff (mode: " + mode + ")")
		
		if mode == "comic":
			categorize.handleFilenameEntryComic(cleanedFromForbidden, result, mode)
		elif mode == "tv":
			categorize.handleFilenameEntryTv(cleanedFromForbidden, result, mode, fileCountInFolder)
			isVideo = True
		elif mode == "movie":
			categorize.handleFilenameEntryMovie(cleanedFromForbidden, result, mode)
			# from "path" we deduct also tags, (genre)
			genre = result["path"].replace("\\", " | ")
			result["genre"] = genre
			isVideo = True
		else:
			titleAndArticle = categorize.cleanTitleName(entry, mode)
			result["title"] = titleAndArticle[0]
			if titleAndArticle[1] != None:
				result["article"] = titleAndArticle[1]
			
			if isMultiDirectory:
				# make sure no trailing spaces ...
				entryNoWhiteSpace = entry.strip()
				# ... this is our special format, we want to deal if ends whit brackets
				if entryNoWhiteSpace[len(entryNoWhiteSpace)-1] == "]":
					closed = stringUtilities.findStringsEnclosedBy(entryNoWhiteSpace, "[", "]")
					# add back ...
					result["title"] = result["title"] + " [" + closed[len(closed)-1] + "]"
		
		if isVideo:
			
			log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntry trying to do extra video processing ...")
			
			subtitles = []

			videoInfo = fileUtility.getVideoInfo(fullpath, True)
			if videoInfo != None:
				for vkey, vvalue in videoInfo.items():
					if vkey == "subtitles":
						if vvalue != None:
							for sub in vvalue:
								subtitles.append(sub)
					else:
						result[vkey] = vvalue
			
			# also, search for a subtitle file of the same name
			# NOTE! We consider only Finnish or English subtitles here, and with a certain rigid naming-convention ...
			# NOTE! The order of the entries in this table is significant
			acceptedLanguagePrefixes =\
			{
				" [Fin]" : "Fin",
				"" : "Fin",
				" [Eng]" : "Eng"
			}
			foundSubtitle = None
			subtitleFileBase = stringUtilities.parseFileTypeAway(fullpath)
			# check if the subtitle-notion is in the file itself
			for key, lang in acceptedLanguagePrefixes.items():
				if len(key) > 0 and result["filename"] .find(key) > -1:
					foundSubtitle = lang
				else:
					for subtitleFormat in fileUtility.subtitlePostFixes:
						subtitleFilePath = subtitleFileBase + key + "." + subtitleFormat
						if os.path.exists(subtitleFilePath):
							foundSubtitle = lang
							break
				if foundSubtitle != None:
					break
			if foundSubtitle != None:
				subtitles.append(foundSubtitle)
			
			foundFinnish = False
			foundEnglish = False
			# check for accepted ones ...
			for sub in subtitles:
				if len(sub) < 3:
					continue
				shortSub = sub[0 : 3].lower()
				if shortSub == "fin" or shortSub == "suo":
					foundFinnish = True
				elif shortSub == "eng":
					foundEnglish = True
			
			if foundFinnish:
				result["subtitle"] = "Fin"
			elif foundEnglish:
				result["subtitle"] = "Eng"
		
		return result		
	# make method static
	handleFilenameEntry = staticmethod(handleFilenameEntry)	

	notAllowedStarters = [ " ", "-", ".", "|", "_" ]
	
	## Take all "futile"/"wrong" stuff out of title name, for example strange symbols/characters.
	def cleanTitleName(entry, mode, replaceOddCharacters = True):
	
		while len(entry) > 0 and entry[0] in categorize.notAllowedStarters:
			entry = entry[1 : ].strip()

		if len(entry) == 0:
			return entry, None
			
		entry = stringUtilities.parseAwayAlphabetsAfter(entry, "__")
		# conditionally replace some characters/strings with white space
		if replaceOddCharacters:
			entry = entry.replace("_", " ").strip()
			entry = entry.replace(".", " ").strip()	
			entry = entry.replace("~", " ").strip()

		#if mode in categorize.g_forbiddenWords.keys():
		#	entry = stringUtilities.replaceWith(entry,\
		#		categorize.g_forbiddenWords[mode],\
		#		mode in categorize.g_forbiddenWordsIgnoreCase,\
		#		" ").strip()

		# take everything inside parenthesis away
		entry = stringUtilities.parseAwayParenthesis(entry)
		# also make sure we don't have some malformations ...
		entry = stringUtilities.parseAwayNonClosingParenthesis(entry)
		# parse "strange" numbers away
		entry = stringUtilities.parseAwayLonelyNumbers(entry, 10000, 100000000)
						
		# take all tokens that  are of form, for example "- -" or "--" or "-    -" away
		entry = stringUtilities.parseAwayEnclosedBy(entry, "-", "-", 1)
		entry = stringUtilities.parseAwayLeading(entry, "-")
		entry = stringUtilities.parseAwayTrailing(entry, "-")
		
		# try to get article
		articleResult = stringUtilities.parseArticle(entry)
		# if we have article, take it away from entry name
		if articleResult[0] != None:
			entry = articleResult[1]

		# do some extra processing for each remaining word
		titleWords = entry.split(" ")
		newTitleWords = []
		for word in titleWords:
			# if this represents a page count, e.g. "23P", we skip it
			if not stringUtilities.isNumberWithPostfix(word, "p", True):
				# make sure starts with a capital letter
				newTitleWords.append(word)
		newTitle = " ".join(newTitleWords)
		newTitle = stringUtilities.makeStartingLettersUppercase(newTitle, 3)
		
		# parse away multiple spaces
		newTitle = stringUtilities.parseMultiSpacesAway(newTitle)
		# and finalize
		newTitle = newTitle.strip()
		
		return newTitle, articleResult[0]
	# make method static
	cleanTitleName = staticmethod(cleanTitleName)	

	## Handle the entry knowing that it is a movie.
	def handleFilenameEntryMovie(entry, result, mode):
		
		titleAndArticle = categorize.cleanTitleName(entry, mode, False)
		result["title"] = titleAndArticle[0]
		if titleAndArticle[1] != None:			
			result["article"] = titleAndArticle[1]
			
	# make method static
	handleFilenameEntryMovie = staticmethod(handleFilenameEntryMovie)	
	
	## Handle the entry knowing that it is a tv show/video.
	def handleFilenameEntryTv(entry, result, mode, fileCountInFolder):		
		
		maxAcceptedEpisodeNumber = max( [fileCountInFolder * 2, 99 ] )
		
		episodeNumber = stringUtilities.parseEpisodeMaster(entry, maxAcceptedEpisodeNumber)
		
		# did we find anything?
		if episodeNumber[0] > -1:
			entry = episodeNumber[1]
			result["episode"] = episodeNumber[0]
			
			# make also sure that we don't have any ghost issue numbers
			entry = stringUtilities.parseAwayNumbersWith(entry, " ", "_")
			entry = stringUtilities.parseAwayLeadingNumberWithPostfix(entry, " -") 
			entry = stringUtilities.parseAwayLeadingNumberWithPostfix(entry, ".") 
		
		titleAndArticle = categorize.cleanTitleName(entry, mode)
		result["title"] = titleAndArticle[0]
		if titleAndArticle[1] != None:
			result["article"] = titleAndArticle[1]
	
	# make method static
	handleFilenameEntryTv = staticmethod(handleFilenameEntryTv)	
	
	## Handle the entry knowing that it is a comic file.
	def handleFilenameEntryComic(entry, result, mode):

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntryComic trying to parse issue ... (\"" + entry + "\"")

		# issue number
		issueNumber = stringUtilities.parseIssueNumberMaster(entry, categorize.MAX_ACCEPTED_ISSUE)

		log.log(categorize.LOG_NAME, log.LVL_DETAIL , "handleFilenameEntryComic issue: " + str(issueNumber))
		
		# did we find anything?
		if issueNumber[0] > -1:
			entry = issueNumber[1]
			result["issue"] = issueNumber[0]
			
			# make also sure that we don't have any ghost issue numbers
			entry = stringUtilities.parseAwayNumbersWith(entry, " ", "_")
			entry = stringUtilities.parseAwayLeadingNumberWithPostfix(entry, " -") 
			entry = stringUtilities.parseAwayLeadingNumberWithPostfix(entry, ".") 
			
		titleAndArticle = categorize.cleanTitleName(entry, mode)
		result["title"] = titleAndArticle[0]
		if titleAndArticle[1] != None:
			result["article"] = titleAndArticle[1]
			
	# make method static
	handleFilenameEntryComic = staticmethod(handleFilenameEntryComic)
		

	# some static helper numbers for printing progress during operations
	underRootDirsCount = 0 # how many directories in total there are in root what we are processing
	underRootFilesCount = 0 # how many files in total there are in root what we are processing
	handledDirsCount = 0
	handledFilesCount = 0
	afterHandledFilesPrintProgress = 0 # what is the next handled count after we print the progress
	afterHandledDelta = 100 # the step for the progression printing, increment to afterHandledFilesPrintProgress after print
	
	##   Function to parse directorys entries, storing the root as separate entry
	#
	#    @param results
	#    @param category
	#    @param rootPath
	#    @param entry
	#    @param enableCategories
	#    @param acceptedPostFixes
	def directoryCategorizerWithRoot(results, category, rootPath, entry, enableCategories, mode):
		
		if categorize.g_acceptedModes.count(mode) == 0:
			utility.errorExit("Error! Mode \"" + mode + "\" not supported!")
		
		rootPath = os.path.normpath(rootPath)
		
		# init some printing helpers for progress ...
		fileAndDirCount = fileUtility.getFileAndDirectoryCount(rootPath)
		categorize.underRootFilesCount = fileAndDirCount[0]
		categorize.underRootDirsCount = fileAndDirCount[1]
		categorize.handledFilesCount = 0
		categorize.handledDirsCount = 0
		categorize.afterHandledFilesPrintProgress = categorize.afterHandledDelta
		
		log.log(categorize.LOG_NAME, log.LVL_PROGRESS, \
			"under root \"" + rootPath + "\"" +\
			": files - " + str(categorize.underRootFilesCount) +\
			", dirs - " + str(categorize.underRootDirsCount))
		
		# call the actual routine ...
		categorize.directoryCategorizer(results, category, rootPath, entry, enableCategories, rootPath, mode, 0)
				
	# make method static
	directoryCategorizerWithRoot = staticmethod(directoryCategorizerWithRoot)	
	
	
	def handleAsEntry(entry, fullpath, rootPath, mode, category, isMultiDirectory, fileCountInFolder):
		
		stripCategoryOutOfTitle = True
		
		if isMultiDirectory:
			stripCategoryOutOfTitle = False
		else:
			# check if entry preconditions through "mode"
			postFix = stringUtilities.parseFilePostFix(fullpath)
			acceptedPostFixes = []
			if mode == "comic":
				acceptedPostFixes = fileUtility.comicPostFixes
			elif mode == "tv":
				acceptedPostFixes = fileUtility.videoPostFixes
			elif mode == "ebook":
				acceptedPostFixes = fileUtility.g_bookPostFixes
				stripCategoryOutOfTitle = False
			elif mode == "movie":
				acceptedPostFixes = fileUtility.videoPostFixes
				stripCategoryOutOfTitle = False
		
			if len(acceptedPostFixes) > 0 and not postFix in acceptedPostFixes:
				# this is not a valid file type, or we do not want to handle this!
				return None
	
		# don't accept Mac OSX temp files ...
		if len(entry) > 1 and entry[ : 2] == "._":
			return None
		
		# handle as file, add to database with category
		result = categorize.handleFilenameEntry(entry, fullpath, rootPath, mode, fileCountInFolder, isMultiDirectory)
			
		if stripCategoryOutOfTitle:
			cleaned = categorize.stripCategoryOutOfTitle(result["title"], category, mode)
			if cleaned[0] > 0:
				result["title"] = cleaned[1]
				if cleaned[2] != None:
					result["article"] = cleaned[2]
		
		# if title is empty, do something about it ...
		if  len(category) > 0 and (not "title" in result or len(result["title"]) == 0):
			# NOTE! What index to use? First or last category? Or something else?
			useIndex = len(category) - 1
			title = category[useIndex]
			title = stringUtilities.parseAwayParenthesis(title)
			result["title"] = title
		
		# prune the category a bit, and fetch pub year from that if not found already
		if "pub_year" not in result:
			categoryPubYear = -1
			for cat in category:
				for yearString in stringUtilities.parseParenthesis(cat):
					yearResult = stringUtilities.parseYear(yearString, 0, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
					if yearResult[0] > -1:
						categoryPubYear = yearResult[0]
						break
				# if we did not found from parenthesis, check the whole name
				if categoryPubYear == -1:
					yearResult = stringUtilities.parseYear(cat, 0, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
					if yearResult[0] > -1:
						categoryPubYear = yearResult[0]
				if categoryPubYear > 0:
					result["pub_year"] = categoryPubYear
					break
		
		if isMultiDirectory:
			result["size"] = fileUtility.getSizeOfDir(fullpath)
		
		return result
	
	# make method static
	handleAsEntry = staticmethod(handleAsEntry)	
	
	## Checks if we should handle the given directory as entry, without going to the invidual files insde the directory.
	def shouldHandleDirectoryAsEntry(fullPath, rootPath):

		directoryType = None
		# FIRST, check our special formatting
		endings = [ "[Mul]", "[Source]", "[Video]" ]
		
		for end in endings:
			index = fullPath.find(end)
			wasMultiFile = index > -1
			if wasMultiFile:
				fullPath = fullPath[0 : index + len(end)]
				subRoot, subTail = os.path.split(fullPath)
				rootPath = subRoot
				break
		
		# SECOND, check for DVD structure
		if not wasMultiFile: 
			wasMultiFile = fullPath.find("VIDEO_TS") > -1
			if wasMultiFile:
				fullPath = fullPath[0 : fullPath.find("VIDEO_TS") - 1]
				# take the root-directory as entry name ...
				subRoot, subTail = os.path.split(fullPath)
				rootPath = subRoot
				directoryType = "dvd"
		
		return wasMultiFile, fullPath, rootPath, directoryType
		
	# make method static
	shouldHandleDirectoryAsEntry = staticmethod(shouldHandleDirectoryAsEntry)	
	
	##   Function to parse directorys entries. 
	#
	#    @param results
	#    @param category
	#    @param path
	#    @param entry
	#    @param enableCategories
	#    @param rootPath
	#    @param mode
	def directoryCategorizer(results, category, path, entry, enableCategories, rootPath, mode, filesInFolder):
		
		if categorize.g_acceptedModes.count(mode) == 0:
			utility.errorExit("Error! Mode \"" + mode + "\" not supported!")

		fullpath = path
		if len(entry) > 0:
			fullpath = os.path.join(fullpath, entry)
		fullpath = os.path.abspath(fullpath)
		
		handleAsEntry = False
		wasMultiFile = False
		wasFile = False
		
		if os.path.isdir(fullpath):
		
			wasMultiFile, fullpath, rootPath, directoryType = categorize.shouldHandleDirectoryAsEntry(fullpath, rootPath)
		
			if not wasMultiFile:
				# take all entries in directory
				filenames = os.listdir(fullpath)
				filesInFolder = len(filenames)
				
				entry = stringUtilities.makeStartingLettersUppercase(entry, 3)
				article = stringUtilities.parseArticle(entry)
				if article[0] != None:
					entry = article[1] + ", " + article[0]
					
				# the entry name will be appended to the category, if categories enabled
				newCategories = []
				if enableCategories:
					for cat in category:
						newCategories.append(cat)
					if len(entry) > 0:
						newCategories.append(entry)
				
				# go through all entries in dir, and call the categorizer for each
				for name in filenames:
					categorize.directoryCategorizer(results, newCategories,\
						fullpath, name, enableCategories, rootPath, mode, filesInFolder)
					
				categorize.handledDirsCount += 1
		elif os.path.isfile(fullpath):
			wasFile = True
			categorize.handledFilesCount += 1
		else:
			log.log(categorize.LOG_NAME, log.LVL_ERROR, "Not a file nor directory: " + fullpath)			
		
		if wasFile or wasMultiFile:
			result = categorize.handleAsEntry(entry, fullpath, rootPath, mode, category, wasMultiFile, filesInFolder)
			
			if not result == None:
				categoryString = " | ".join(category)
				if categoryString not in results:
					results[categoryString] = []
				results[categoryString].append(result)		
		
		# print progress?
		if categorize.handledFilesCount  >= categorize.afterHandledFilesPrintProgress:
			# print!
			log.log(categorize.LOG_NAME, 2, "categorized"+\
				" files: " + str(categorize.handledFilesCount) + " of " + str(categorize.underRootFilesCount) +\
				" [dirs: " + str(categorize.handledDirsCount) + " of " + str(categorize.underRootDirsCount) + "]")				
			# init next round
			categorize.afterHandledFilesPrintProgress += categorize.afterHandledDelta
	
	# make method static
	directoryCategorizer = staticmethod(directoryCategorizer)	

	##  Function to take category definitions out of title.
	#    @param entry
	#    @param category
	#    @param mode
	def stripCategoryOutOfTitle(title, category, mode):
		# if there is some part of the category in title, we prune that away so we get nice, short title names
		totalReplaces = 0
		catCount = len(category)
		article = None
		if catCount > 1 or (catCount == 1 and title != category[0]):
			
			# do this in reverse order 
			for cat in reversed(category):
				replaceCount = 0
				# ... make sure there is no parenthesis (if there is, it's propably because of publishing year)
				cat = stringUtilities.parseAwayParenthesis(cat)
				
				# require the category be long enough ...
				if len(cat) < 2:
					continue
				
				if title.find(cat) == 0:
					title = title.replace(cat, "")
					replaceCount += 1
				# also check if the category contains trailing (processed, that is) article
				fixedCategory = stringUtilities.parseReversedArticle(cat)
				if len(fixedCategory[0]) > 0 and title.find(fixedCategory[1]) == 0:
					title = title.replace(fixedCategory[1], "")
					replaceCount += 1
				
				if replaceCount > 0:
					cleaned = categorize.cleanTitleName(title, mode)
					title = cleaned[0]
					article = cleaned[1]
					totalReplaces += replaceCount

		return totalReplaces, title, article
	# make method static
	stripCategoryOutOfTitle = staticmethod(stripCategoryOutOfTitle)	
	
	##  Function to parse author entry,
	#   and form database entries out of them (adds them to mysql table)
	#    @param author
	#    @param entry
	def handleFilenameWithAuthor(author, entry, fullpath):
		
		# store these for reference
		result["filename"] = entry
		
		# result dictionary
		result = dict()
		
		# make entry as title
		entry = entry.title()
		
		# take author away from entry if found
		entry = entry.strip(author.title()).strip()
		while \
			entry[0] == "-" or \
			entry[0] == "." or \
			entry[0] == "_":
			entry = entry[1 : len(entry)].strip()
		
		# try to get pub year, give priority to parenthesis
		pubYear = -1
		for yearString in stringUtilities.parseParenthesis(entry):
			pubYear = stringUtilities.parseYear(yearString, 1, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
		# if we did not found from parenthesis, check the whole name
		if pubYear == -1:
			pubYear = stringUtilities.parseYear(entry, 0, categorize.MIN_ACCEPTED_YEAR, categorize.MAX_ACCEPTED_YEAR)
		
		# take the file type (postfix)
		fileType = stringUtilities.parseFilePostFix(entry)
		# strip file type away from entry name
		entry = stringUtilities.parseFileTypeAway(entry)
		
		# take everything in parenthesis away
		entry = stringUtilities.parseAwayParenthesis(entry)
		
		# replace all dots (".") with white space
		entry = entry.replace(".", " ").strip()
			
		# try to get article
		article = stringUtilities.parseArticle(entry)
			
		# if we have article, take it away from entry name
		if article[0] != None:
			entry = article[1]
			result["article"] = article[0]
		
		if pubYear > 0:
			result["pub_year"] = pubYear
		
		result["title"] = entry
		result["file_type"] = fileType
			
		return result
		
	# make method static
	handleFilenameWithAuthor = staticmethod(handleFilenameWithAuthor)	


	##  Function to parse author based directories
	#   and form database entries out of them (adds them to mysql table)
	#    @param category
	#    @param path
	#    @param entry
	#    @param author
	def authorCategorizer(results, path, entry, author):
		fullpath = path
		if len(entry) > 0:
			fullpath = fullpath + "/" + entry
		
		if os.path.isdir(fullpath):
							
			# take all entries in directory
			filenames = os.listdir(fullpath)

			# we have the author already?
			if len(author) == 0:
				author = entry.title()
			# already have the author, so the entry name will be 
			# appended to the category
			elif len(category) > 0 and len(entry) > 0:
				category = category + "/" + entry
			elif len(entry) > 0:
				category = entry

			# make category lower case
			category = category.lower()
			
			# go through all entries in dir, and call the categorizer for each
			for name in filenames:
				categorize.authorCategorizer(results, fullpath, name, author)
			
		elif os.path.isfile(fullpath):
		
			# handle as file, add to database					
			result = categorize.handleFilenameWithAuthor(author, entry, fullpath)		
			
			if category not in results:
				results[category] = []
			
			results[category].append(result)
			
		else:
			log.log(categorize.LOG_NAME, log.LVL_ERROR, "ERROR! Not a file nor directory: " + fullpath)
	# make method static
	authorCategorizer = staticmethod(authorCategorizer)	
	
	
	
