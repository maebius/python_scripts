##  @package utility
#    Miscallenous collection of utility functions.

import os
import sys
import getopt
import re
import datetime
import time
import shutil
import string
from io import StringIO
import subprocess

LOG_NAME = __name__

from stringUtilities import stringUtilities

def getMultipleOfStringOfLongest(lines, entry):
	longestLine = 0
	for line in lines:
		subLines = line.split("\n")
		for subLine in subLines:
			longestLine = max(longestLine, len(subLine))
	
	result = ""
	index = 0
	while index < longestLine:
		result += entry
		index += 1
	return result

def errorExit(msg):
	lines = []
	lines.append("ERROR! - " + msg)
	lines.append("Location: " + getFrameSignature(2))
	lines.append("Exiting execution with sys.exit(2)) ...")
	limiter = getMultipleOfStringOfLongest(lines, "-")
	
	print (limiter)
	for line in lines:
		print (line)
	print (limiter)
		
	sys.exit(2)

## Gets the frames (from wanted hierarchy) class and function names as a string.
def getFrameSignature(number):
	resultString = ""
	fileName = sys._getframe(number).f_code.co_filename
	functionName = sys._getframe(number).f_code.co_name
	#lineNumber = sys._getframe(number).f_code.co_firstlineno
	lineNumber = sys._getframe(number).f_lineno
	resultString += fileName
	if functionName != "<module>":
		resultString += "::" + functionName
	resultString += " [L" + str(lineNumber) + "]"
	return resultString

## Parses the command line args to a dict
def formatOptionsAsString(acceptedArgs, acceptedOptions, requiredOptions):
	resultString = ""
	
	if len(acceptedArgs) > 0:
		resultString  += "\tAccepted arguments:\n"
		for accepted in acceptedArgs:
			resultString  += "\t\t" + accepted + "\n"
	if len(acceptedOptions) > 0:
		resultString  += "\tAccepted options:\n"
		for accepted in acceptedOptions:
			resultString  += "\t\t" + accepted + "\n"
	if len(requiredOptions) > 0:
		resultString  += "\tRequired options:\n"
		for req in requiredOptions:
			resultString  += "\t\t" + req + "\n"
	return resultString

## Parses the command line args to a dict
def parseOptions(acceptedArgs, acceptedOptions, requiredOptions, argsResult, optionsResult):
	
	# just re-format the input so we have nicer keys (without the equality mark with options, tha tis)
	formattedAcceptedOptions = []
	for entry in acceptedOptions:
		formattedAcceptedOptions.append(entry.replace("=", ""))
	
	resultString = ""
	resultOk = True
	
	# parse the system args against wanted options
	try:
		opts, args = getopt.getopt(sys.argv[1:], acceptedArgs, acceptedOptions)
	except getopt.GetoptError as err:
		resultString += getFrameSignature(2) + " - " + str(err) # will print something like "option -a not recognized"
		resultString += formatOptionsAsString(acceptedArgs, formattedAcceptedOptions, requiredOptions)
		resultOk = False

	if resultOk:
		# take the args
		for arg in args:
			argsResult.append(arg)
		# take the options into own dict()
		for o, a in opts:
			strippedO = o.replace("--", "")
			if not strippedO in formattedAcceptedOptions:
				resultString += getFrameSignature(2)  + " - unaccepted option: \"" + strippedO + "\""
				resultString += formatOptionsAsString(acceptedArgs, formattedAcceptedOptions, requiredOptions)
				resultOk = False
			else:
				optionsResult[strippedO] = a

	if resultOk:
		# additional checking for required options
		for required in requiredOptions:
			if not required in optionsResult.keys():
				resultString += getFrameSignature(2)  + " - required option missing: \"" + required + "\""
				resultString += formatOptionsAsString(acceptedArgs, formattedAcceptedOptions, requiredOptions)
				resultOk = False

	if not resultOk:
		print (resultString)
		sys.exit(2)
		return False

	return True

##  Command to print out some basic stuff concerning the program
def printProgramInfo(options):
	preFix = "\t*"

	callTime = getCurrentTime()
	originalDirectory = os.getcwd()
	callerFileName = sys._getframe(1).f_code.co_filename
	callerFunctionName = sys._getframe(1).f_code.co_name

	resultString = ""
	
	resultString += preFix + "**************************************************************************************\n"
	resultString += preFix + " Program file: " + callerFileName + "\n"
	if len(callerFunctionName) > 0:
		resultString += preFix + " function: " + callerFunctionName + "\n"
	resultString += preFix + " Sys args: " + str(len(sys.argv) - 1)
	if len(sys.argv) > 1:
		resultString += " (\"" + " ".join(sys.argv[1:]) + "\")\n"
	resultString += preFix + " Program options: " + str(len(options.items())) + "\n"
	optionNumber = 1
	for o, a in options.items():
		resultString += preFix + "\t#" + str(optionNumber) + ": " + o
		if len(a) > 0:
			resultString += " = " + a
		resultString += "\n"
		optionNumber += 1
		
	resultString += preFix + " Called in dir: " + originalDirectory + "\n"
	resultString += preFix + " Call time: " + repr(callTime.ctime()) + "\n"
	resultString += preFix + "**************************************************************************************\n"
	resultString += ""
	
	print (resultString)
	
##  Command to print out processing time, calculated out of start and end times
#   @param start_time The start time used for printing.
#   @param stop_time The stop time used for printing.
def printProcessingTime(start_time, stop_time):
	preFix = "\t*"

	callerFileName = sys._getframe(1).f_code.co_filename

	resultString = ""

	resultString +=  "\n"
	resultString +=  preFix + "**************************************************************************************\n"
	resultString +=  preFix + " Finished! (calling: " + callerFileName + ")\n"
	resultString +=  preFix + " ---------\n"
	resultString +=  preFix + "\n"
	resultString +=  preFix + " Start time: " + repr(start_time.ctime()) + "\n"
	resultString +=  preFix + " Stop  time: " + repr(stop_time.ctime()) + "\n"
	resultString +=  preFix + "\n"
	resultString +=  preFix + " Processing took: " +  str(stop_time - start_time) + "\n"
	resultString +=  preFix + "**************************************************************************************\n"
	
	print (resultString)

## Check that string 'value' represents a number
def isNumber(value):
	if isInteger(value):
		return True
	if isDecimal(value):
		return True
	return False

## Check that string 'value' represents an integer
def isInteger(value):
	return str(value).isdigit()

## Check that string 'value' represents a decimal
def isDecimal(value):
	try:
		stripped = str(float(source))
		return True
	except:
		# 'value' does not represent a decimal
		return False

##  Execute command on shell and print it before
#   @param command The command that is to be executed in console.
def execute(command, justPrint, args = [], usePipe = False):
	fullCommand = command
	if args != None:
		fullCommand += " " + " ".join(args)
		
	if not justPrint:
		if usePipe:
			subprocess.call(fullCommand, stdout=subprocess.PIPE)
		else:
			subprocess.call(fullCommand, stdout=0)
	
## Return the deltatime struct as whole seconds since reference.
def getSecondsSince(startTime):
	delta = getCurrentTime() - startTime
	return delta.days * 86400 + delta.seconds

##  Return current time.
def getCurrentTime():
	return datetime.datetime.now()

##  Return current time, more simpler format.
def getSimpleTime():
	now = datetime.datetime.now()
	return now.strftime("%H:%M:%S")

##  Exit the application and change working dir back if number exceeds threshold,
##  If not, return the number incremented by one
#   @param i The number that is incremented by one if not equal or greater than the comparison value.
#   @param max The maximum where we compare i.
#   @param path To this path we change after condition is satisfied.
def exitIfGreatEnough(i, max, path):
	if (i >= max):
		os.chdir(path)
		exit(0)	
	else:
		return i + 1